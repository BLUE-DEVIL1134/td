import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:td/api/objects.dart' as TdObjects;
import 'package:td/helpers/service.dart';

void main() {
  runApp(
    MaterialApp(
      home: ChangeNotifierProvider(
        child: MyApp(),
        create: (BuildContext context) => TelegramService(),
      ),
    ),
  );
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String tdlibVersion = "unknown";

  @override
  void initState() {
    super.initState();
    initClient();
  }

  Future<void> initClient() async {
    await Provider.of<TelegramService>(context, listen: false)
        .initClient(Backend.PLATFORM_CHANNELS);
    Provider.of<TelegramService>(context, listen: false)
        .currentClientEvents
        .stream
        .listen((obj) {
      if (obj is TdObjects.UpdateOption) {
        setState(() =>
            tdlibVersion = (obj.value as TdObjects.OptionValueString).value!);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('TDLib version: $tdlibVersion'),
      ),
    );
  }
}
