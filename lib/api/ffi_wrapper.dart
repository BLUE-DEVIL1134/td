import 'dart:async';
import 'dart:convert';
import 'dart:isolate';

import 'package:td/api/td_object.dart';
import 'package:td/api/convertor.dart';
import 'package:td/api/td_function.dart';
import 'package:td/api/raw_json_client.dart';
import 'package:td/helpers/isolate_client_manager.dart';

Future<void> _receiveUpdates(RawJsonBindings client, SendPort port) async {
  final event = await Future(() => client.receive(timeout: 1.0));
  if (event != null) {
    var eventParsed = json.decode(event);
    eventParsed['clientId'] = client.clientAddr;
    port.send(eventParsed.toString());
  }

  Future(() => _receiveUpdates(client, port));
}

void _clientIsolate(SendPort port) async {
  var isolateReceivePort = ReceivePort();
  RawJsonBindings jsonClient = RawJsonBindings();
  jsonClient.initClient();
  port.send({
    'port': isolateReceivePort.sendPort,
    'clientId': jsonClient.clientAddr.address // int
  });

  isolateReceivePort.listen((message) async {
    if (message is Map<String, dynamic>) {
      SendPort? tmpSendPort;
      if (message.containsKey('port'))
        tmpSendPort = message['port'] as SendPort;

      switch (message['_requestType'] as String) {
        case 'execute':
          var tdlibRequest = message;
          tdlibRequest.remove('_requestType');
          tdlibRequest.remove('port');

          final result = jsonClient.execute(tdlibRequest);
          tmpSendPort!.send(result);
          break;
        case 'send':
          var tdlibRequest = message;
          tdlibRequest.remove('_requestType');
          jsonClient.send(tdlibRequest);
          break;
        case 'destroy':
          jsonClient.destroy();
          tmpSendPort!.send(true);
      }
    }
  });

  _receiveUpdates(jsonClient, port);
}

class TdlibFFIWrapper {
  Map<int, Isolate> _isolates = {};
  Map<int, SendPort> _sendPorts = {};
  late ReceivePort _receivePort;
  IsolateClientManager? _manager;
  StreamController<String> updates = StreamController<String>();

  Future<int> initClient(
      {IsolateClientManager? manager, bool killClients = true}) async {
    Completer<int> _completer = new Completer<int>();
    _receivePort = ReceivePort();
    _manager = manager;

    if (_manager != null && killClients == true) _manager!.checkClients();
    var tmpIsolate = await Isolate.spawn(_clientIsolate, _receivePort.sendPort);
    _receivePort.listen((message) {
      if (message is Map) {
        if (message.containsKey('port')) {
          int clientId = message['clientId'] as int;

          _manager?.saveClient(clientId);
          _sendPorts[clientId] = message['port'];
          _isolates[clientId] = tmpIsolate;
          _completer.complete(clientId);
        } else {
          updates.add(json.encode(message));
        }
      }
    });

    return _completer.future;
  }

  Future<TdObject> execute(int clientId, TdFunction request) async {
    var tmpReceivePort = ReceivePort();
    var jsonRequest = request.toJson();

    jsonRequest['port'] = tmpReceivePort.sendPort;
    jsonRequest['_requestType'] = 'execute';
    _sendPorts[clientId]!.send(jsonRequest);
    return convertToTdObject(await tmpReceivePort.first);
  }

  Future<void> send(int clientId, TdFunction request) async {
    var jsonRequest = request.toJson();

    jsonRequest['_requestType'] = 'send';
    _sendPorts[clientId]!.send(jsonRequest);
  }

  Future<void> close(int clientId) async {
    var tmpReceivePort = ReceivePort();
    _sendPorts[clientId]!
        .send({'_requestType': 'destroy', 'port': tmpReceivePort});

    if ((await tmpReceivePort.first) == true) {
      _isolates[clientId]!.kill();
      _isolates.remove(clientId);
    }
  }
}
