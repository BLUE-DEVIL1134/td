import 'package:td/api/td_function.dart';
import 'package:td/api/objects.dart';

class GetAuthorizationState extends TdFunction {
  /// Returns the current authorization state; this is an offline request. For informational purposes only. Use updateAuthorizationState instead to maintain the current authorization state. Can be called before initialization
  GetAuthorizationState(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetAuthorizationState.fromJson(Map<String, dynamic> json) {
    return GetAuthorizationState(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getAuthorizationState';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetTdlibParameters extends TdFunction {
  /// Sets the parameters for TDLib initialization. Works only when the current authorization state is authorizationStateWaitTdlibParameters
  SetTdlibParameters(
      {required this.parameters,
      this.extra});

  /// [parameters] Parameters for TDLib initialization
  TdlibParameters parameters;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetTdlibParameters.fromJson(Map<String, dynamic> json) {
    return SetTdlibParameters(
      parameters: TdlibParameters.fromJson(json['parameters'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "parameters": this.parameters.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setTdlibParameters';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CheckDatabaseEncryptionKey extends TdFunction {
  /// Checks the database encryption key for correctness. Works only when the current authorization state is authorizationStateWaitEncryptionKey
  CheckDatabaseEncryptionKey(
      {required this.encryptionKey,
      this.extra});

  /// [encryptionKey] Encryption key to check or set up
  String encryptionKey;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CheckDatabaseEncryptionKey.fromJson(Map<String, dynamic> json) {
    return CheckDatabaseEncryptionKey(
      encryptionKey: json['encryption_key'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "encryption_key": this.encryptionKey,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'checkDatabaseEncryptionKey';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetAuthenticationPhoneNumber extends TdFunction {
  /// Sets the phone number of the user and sends an authentication code to the user. Works only when the current authorization state is authorizationStateWaitPhoneNumber,. or if there is no pending authentication query and the current authorization state is authorizationStateWaitCode, authorizationStateWaitRegistration, or authorizationStateWaitPassword
  SetAuthenticationPhoneNumber(
      {required this.phoneNumber,
      required this.settings,
      this.extra});

  /// [phoneNumber] The phone number of the user, in international format
  String phoneNumber;

  /// [settings] Settings for the authentication of the user's phone number; pass null to use default settings
  PhoneNumberAuthenticationSettings settings;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetAuthenticationPhoneNumber.fromJson(Map<String, dynamic> json) {
    return SetAuthenticationPhoneNumber(
      phoneNumber: json['phone_number'] ?? "",
      settings: PhoneNumberAuthenticationSettings.fromJson(json['settings'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "phone_number": this.phoneNumber,
      "settings": this.settings.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setAuthenticationPhoneNumber';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ResendAuthenticationCode extends TdFunction {
  /// Re-sends an authentication code to the user. Works only when the current authorization state is authorizationStateWaitCode, the next_code_type of the result is not null and the server-specified timeout has passed
  ResendAuthenticationCode(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ResendAuthenticationCode.fromJson(Map<String, dynamic> json) {
    return ResendAuthenticationCode(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'resendAuthenticationCode';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CheckAuthenticationCode extends TdFunction {
  /// Checks the authentication code. Works only when the current authorization state is authorizationStateWaitCode
  CheckAuthenticationCode(
      {required this.code,
      this.extra});

  /// [code] The verification code received via SMS, Telegram message, phone call, or flash call
  String code;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CheckAuthenticationCode.fromJson(Map<String, dynamic> json) {
    return CheckAuthenticationCode(
      code: json['code'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "code": this.code,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'checkAuthenticationCode';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class RequestQrCodeAuthentication extends TdFunction {
  /// Requests QR code authentication by scanning a QR code on another logged in device. Works only when the current authorization state is authorizationStateWaitPhoneNumber,. or if there is no pending authentication query and the current authorization state is authorizationStateWaitCode, authorizationStateWaitRegistration, or authorizationStateWaitPassword
  RequestQrCodeAuthentication(
      {required this.otherUserIds,
      this.extra});

  /// [otherUserIds] List of user identifiers of other users currently using the application
  List<int> otherUserIds;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory RequestQrCodeAuthentication.fromJson(Map<String, dynamic> json) {
    return RequestQrCodeAuthentication(
      otherUserIds: List<int>.from((json['other_user_ids'] ?? []).map((item) => item ?? 0).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "other_user_ids": this.otherUserIds.map((i) => i).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'requestQrCodeAuthentication';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class RegisterUser extends TdFunction {
  /// Finishes user registration. Works only when the current authorization state is authorizationStateWaitRegistration
  RegisterUser(
      {required this.firstName,
      required this.lastName,
      this.extra});

  /// [firstName] The first name of the user; 1-64 characters
  String firstName;

  /// [lastName] The last name of the user; 0-64 characters
  String lastName;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory RegisterUser.fromJson(Map<String, dynamic> json) {
    return RegisterUser(
      firstName: json['first_name'] ?? "",
      lastName: json['last_name'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "first_name": this.firstName,
      "last_name": this.lastName,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'registerUser';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CheckAuthenticationPassword extends TdFunction {
  /// Checks the authentication password for correctness. Works only when the current authorization state is authorizationStateWaitPassword
  CheckAuthenticationPassword(
      {required this.password,
      this.extra});

  /// [password] The password to check
  String password;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CheckAuthenticationPassword.fromJson(Map<String, dynamic> json) {
    return CheckAuthenticationPassword(
      password: json['password'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "password": this.password,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'checkAuthenticationPassword';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class RequestAuthenticationPasswordRecovery extends TdFunction {
  /// Requests to send a password recovery code to an email address that was previously set up. Works only when the current authorization state is authorizationStateWaitPassword
  RequestAuthenticationPasswordRecovery(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory RequestAuthenticationPasswordRecovery.fromJson(Map<String, dynamic> json) {
    return RequestAuthenticationPasswordRecovery(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'requestAuthenticationPasswordRecovery';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CheckAuthenticationPasswordRecoveryCode extends TdFunction {
  /// Checks whether a password recovery code sent to an email address is valid. Works only when the current authorization state is authorizationStateWaitPassword
  CheckAuthenticationPasswordRecoveryCode(
      {required this.recoveryCode,
      this.extra});

  /// [recoveryCode] Recovery code to check
  String recoveryCode;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CheckAuthenticationPasswordRecoveryCode.fromJson(Map<String, dynamic> json) {
    return CheckAuthenticationPasswordRecoveryCode(
      recoveryCode: json['recovery_code'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "recovery_code": this.recoveryCode,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'checkAuthenticationPasswordRecoveryCode';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class RecoverAuthenticationPassword extends TdFunction {
  /// Recovers the password with a password recovery code sent to an email address that was previously set up. Works only when the current authorization state is authorizationStateWaitPassword
  RecoverAuthenticationPassword(
      {required this.recoveryCode,
      required this.newPassword,
      required this.newHint,
      this.extra});

  /// [recoveryCode] Recovery code to check
  String recoveryCode;

  /// [newPassword] New password of the user; may be empty to remove the password
  String newPassword;

  /// [newHint] New password hint; may be empty
  String newHint;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory RecoverAuthenticationPassword.fromJson(Map<String, dynamic> json) {
    return RecoverAuthenticationPassword(
      recoveryCode: json['recovery_code'] ?? "",
      newPassword: json['new_password'] ?? "",
      newHint: json['new_hint'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "recovery_code": this.recoveryCode,
      "new_password": this.newPassword,
      "new_hint": this.newHint,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'recoverAuthenticationPassword';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CheckAuthenticationBotToken extends TdFunction {
  /// Checks the authentication token of a bot; to log in as a bot. Works only when the current authorization state is authorizationStateWaitPhoneNumber. Can be used instead of setAuthenticationPhoneNumber and checkAuthenticationCode to log in
  CheckAuthenticationBotToken(
      {required this.token,
      this.extra});

  /// [token] The bot token
  String token;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CheckAuthenticationBotToken.fromJson(Map<String, dynamic> json) {
    return CheckAuthenticationBotToken(
      token: json['token'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "token": this.token,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'checkAuthenticationBotToken';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class LogOut extends TdFunction {
  /// Closes the TDLib instance after a proper logout. Requires an available network connection. All local data will be destroyed. After the logout completes, updateAuthorizationState with authorizationStateClosed will be sent
  LogOut(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory LogOut.fromJson(Map<String, dynamic> json) {
    return LogOut(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'logOut';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class Close extends TdFunction {
  /// Closes the TDLib instance. All databases will be flushed to disk and properly closed. After the close completes, updateAuthorizationState with authorizationStateClosed will be sent. Can be called before initialization
  Close(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory Close.fromJson(Map<String, dynamic> json) {
    return Close(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'close';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class Destroy extends TdFunction {
  /// Closes the TDLib instance, destroying all local data without a proper logout. The current user session will remain in the list of all active sessions. All local data will be destroyed. After the destruction completes updateAuthorizationState with authorizationStateClosed will be sent. Can be called before authorization
  Destroy(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory Destroy.fromJson(Map<String, dynamic> json) {
    return Destroy(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'destroy';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ConfirmQrCodeAuthentication extends TdFunction {
  /// Confirms QR code authentication on another device. Returns created session on success
  ConfirmQrCodeAuthentication(
      {required this.link,
      this.extra});

  /// [link] A link from a QR code. The link must be scanned by the in-app camera
  String link;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ConfirmQrCodeAuthentication.fromJson(Map<String, dynamic> json) {
    return ConfirmQrCodeAuthentication(
      link: json['link'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "link": this.link,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'confirmQrCodeAuthentication';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetCurrentState extends TdFunction {
  /// Returns all updates needed to restore current TDLib state, i.e. all actual UpdateAuthorizationState/UpdateUser/UpdateNewChat and others. This is especially useful if TDLib is run in a separate process. Can be called before initialization
  GetCurrentState(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetCurrentState.fromJson(Map<String, dynamic> json) {
    return GetCurrentState(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getCurrentState';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetDatabaseEncryptionKey extends TdFunction {
  /// Changes the database encryption key. Usually the encryption key is never changed and is stored in some OS keychain
  SetDatabaseEncryptionKey(
      {required this.newEncryptionKey,
      this.extra});

  /// [newEncryptionKey] New encryption key
  String newEncryptionKey;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetDatabaseEncryptionKey.fromJson(Map<String, dynamic> json) {
    return SetDatabaseEncryptionKey(
      newEncryptionKey: json['new_encryption_key'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "new_encryption_key": this.newEncryptionKey,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setDatabaseEncryptionKey';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetPasswordState extends TdFunction {
  /// Returns the current state of 2-step verification
  GetPasswordState(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetPasswordState.fromJson(Map<String, dynamic> json) {
    return GetPasswordState(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getPasswordState';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetPassword extends TdFunction {
  /// Changes the password for the current user. If a new recovery email address is specified, then the change will not be applied until the new recovery email address is confirmed
  SetPassword(
      {required this.oldPassword,
      required this.newPassword,
      required this.newHint,
      required this.setRecoveryEmailAddress,
      required this.newRecoveryEmailAddress,
      this.extra});

  /// [oldPassword] Previous password of the user
  String oldPassword;

  /// [newPassword] New password of the user; may be empty to remove the password
  String newPassword;

  /// [newHint] New password hint; may be empty
  String newHint;

  /// [setRecoveryEmailAddress] Pass true if the recovery email address must be changed
  bool setRecoveryEmailAddress;

  /// [newRecoveryEmailAddress] New recovery email address; may be empty
  String newRecoveryEmailAddress;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetPassword.fromJson(Map<String, dynamic> json) {
    return SetPassword(
      oldPassword: json['old_password'] ?? "",
      newPassword: json['new_password'] ?? "",
      newHint: json['new_hint'] ?? "",
      setRecoveryEmailAddress: json['set_recovery_email_address'] ?? false,
      newRecoveryEmailAddress: json['new_recovery_email_address'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "old_password": this.oldPassword,
      "new_password": this.newPassword,
      "new_hint": this.newHint,
      "set_recovery_email_address": this.setRecoveryEmailAddress,
      "new_recovery_email_address": this.newRecoveryEmailAddress,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setPassword';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetRecoveryEmailAddress extends TdFunction {
  /// Returns a 2-step verification recovery email address that was previously set up. This method can be used to verify a password provided by the user
  GetRecoveryEmailAddress(
      {required this.password,
      this.extra});

  /// [password] The password for the current user
  String password;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetRecoveryEmailAddress.fromJson(Map<String, dynamic> json) {
    return GetRecoveryEmailAddress(
      password: json['password'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "password": this.password,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getRecoveryEmailAddress';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetRecoveryEmailAddress extends TdFunction {
  /// Changes the 2-step verification recovery email address of the user. If a new recovery email address is specified, then the change will not be applied until the new recovery email address is confirmed.. If new_recovery_email_address is the same as the email address that is currently set up, this call succeeds immediately and aborts all other requests waiting for an email confirmation
  SetRecoveryEmailAddress(
      {required this.password,
      required this.newRecoveryEmailAddress,
      this.extra});

  /// [password] Password of the current user
  String password;

  /// [newRecoveryEmailAddress] New recovery email address
  String newRecoveryEmailAddress;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetRecoveryEmailAddress.fromJson(Map<String, dynamic> json) {
    return SetRecoveryEmailAddress(
      password: json['password'] ?? "",
      newRecoveryEmailAddress: json['new_recovery_email_address'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "password": this.password,
      "new_recovery_email_address": this.newRecoveryEmailAddress,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setRecoveryEmailAddress';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CheckRecoveryEmailAddressCode extends TdFunction {
  /// Checks the 2-step verification recovery email address verification code
  CheckRecoveryEmailAddressCode(
      {required this.code,
      this.extra});

  /// [code] Verification code
  String code;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CheckRecoveryEmailAddressCode.fromJson(Map<String, dynamic> json) {
    return CheckRecoveryEmailAddressCode(
      code: json['code'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "code": this.code,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'checkRecoveryEmailAddressCode';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ResendRecoveryEmailAddressCode extends TdFunction {
  /// Resends the 2-step verification recovery email address verification code
  ResendRecoveryEmailAddressCode(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ResendRecoveryEmailAddressCode.fromJson(Map<String, dynamic> json) {
    return ResendRecoveryEmailAddressCode(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'resendRecoveryEmailAddressCode';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class RequestPasswordRecovery extends TdFunction {
  /// Requests to send a 2-step verification password recovery code to an email address that was previously set up
  RequestPasswordRecovery(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory RequestPasswordRecovery.fromJson(Map<String, dynamic> json) {
    return RequestPasswordRecovery(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'requestPasswordRecovery';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CheckPasswordRecoveryCode extends TdFunction {
  /// Checks whether a 2-step verification password recovery code sent to an email address is valid
  CheckPasswordRecoveryCode(
      {required this.recoveryCode,
      this.extra});

  /// [recoveryCode] Recovery code to check
  String recoveryCode;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CheckPasswordRecoveryCode.fromJson(Map<String, dynamic> json) {
    return CheckPasswordRecoveryCode(
      recoveryCode: json['recovery_code'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "recovery_code": this.recoveryCode,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'checkPasswordRecoveryCode';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class RecoverPassword extends TdFunction {
  /// Recovers the 2-step verification password using a recovery code sent to an email address that was previously set up
  RecoverPassword(
      {required this.recoveryCode,
      required this.newPassword,
      required this.newHint,
      this.extra});

  /// [recoveryCode] Recovery code to check
  String recoveryCode;

  /// [newPassword] New password of the user; may be empty to remove the password
  String newPassword;

  /// [newHint] New password hint; may be empty
  String newHint;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory RecoverPassword.fromJson(Map<String, dynamic> json) {
    return RecoverPassword(
      recoveryCode: json['recovery_code'] ?? "",
      newPassword: json['new_password'] ?? "",
      newHint: json['new_hint'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "recovery_code": this.recoveryCode,
      "new_password": this.newPassword,
      "new_hint": this.newHint,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'recoverPassword';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ResetPassword extends TdFunction {
  /// Removes 2-step verification password without previous password and access to recovery email address. The password can't be reset immediately and the request needs to be repeated after the specified time
  ResetPassword(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ResetPassword.fromJson(Map<String, dynamic> json) {
    return ResetPassword(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'resetPassword';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CancelPasswordReset extends TdFunction {
  /// Cancels reset of 2-step verification password. The method can be called if passwordState.pending_reset_date
  CancelPasswordReset(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CancelPasswordReset.fromJson(Map<String, dynamic> json) {
    return CancelPasswordReset(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'cancelPasswordReset';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CreateTemporaryPassword extends TdFunction {
  /// Creates a new temporary password for processing payments
  CreateTemporaryPassword(
      {required this.password,
      required this.validFor,
      this.extra});

  /// [password] Persistent user password
  String password;

  /// [validFor] Time during which the temporary password will be valid, in seconds; must be between 60 and 86400
  int validFor;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CreateTemporaryPassword.fromJson(Map<String, dynamic> json) {
    return CreateTemporaryPassword(
      password: json['password'] ?? "",
      validFor: json['valid_for'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "password": this.password,
      "valid_for": this.validFor,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'createTemporaryPassword';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetTemporaryPasswordState extends TdFunction {
  /// Returns information about the current temporary password
  GetTemporaryPasswordState(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetTemporaryPasswordState.fromJson(Map<String, dynamic> json) {
    return GetTemporaryPasswordState(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getTemporaryPasswordState';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetMe extends TdFunction {
  /// Returns the current user
  GetMe(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetMe.fromJson(Map<String, dynamic> json) {
    return GetMe(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getMe';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetUser extends TdFunction {
  /// Returns information about a user by their identifier. This is an offline request if the current user is not a bot
  GetUser(
      {required this.userId,
      this.extra});

  /// [userId] User identifier
  int userId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetUser.fromJson(Map<String, dynamic> json) {
    return GetUser(
      userId: json['user_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "user_id": this.userId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getUser';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetUserFullInfo extends TdFunction {
  /// Returns full information about a user by their identifier
  GetUserFullInfo(
      {required this.userId,
      this.extra});

  /// [userId] User identifier
  int userId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetUserFullInfo.fromJson(Map<String, dynamic> json) {
    return GetUserFullInfo(
      userId: json['user_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "user_id": this.userId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getUserFullInfo';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetBasicGroup extends TdFunction {
  /// Returns information about a basic group by its identifier. This is an offline request if the current user is not a bot
  GetBasicGroup(
      {required this.basicGroupId,
      this.extra});

  /// [basicGroupId] Basic group identifier
  int basicGroupId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetBasicGroup.fromJson(Map<String, dynamic> json) {
    return GetBasicGroup(
      basicGroupId: json['basic_group_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "basic_group_id": this.basicGroupId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getBasicGroup';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetBasicGroupFullInfo extends TdFunction {
  /// Returns full information about a basic group by its identifier
  GetBasicGroupFullInfo(
      {required this.basicGroupId,
      this.extra});

  /// [basicGroupId] Basic group identifier
  int basicGroupId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetBasicGroupFullInfo.fromJson(Map<String, dynamic> json) {
    return GetBasicGroupFullInfo(
      basicGroupId: json['basic_group_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "basic_group_id": this.basicGroupId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getBasicGroupFullInfo';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetSupergroup extends TdFunction {
  /// Returns information about a supergroup or a channel by its identifier. This is an offline request if the current user is not a bot
  GetSupergroup(
      {required this.supergroupId,
      this.extra});

  /// [supergroupId] Supergroup or channel identifier
  int supergroupId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetSupergroup.fromJson(Map<String, dynamic> json) {
    return GetSupergroup(
      supergroupId: json['supergroup_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "supergroup_id": this.supergroupId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getSupergroup';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetSupergroupFullInfo extends TdFunction {
  /// Returns full information about a supergroup or a channel by its identifier, cached for up to 1 minute
  GetSupergroupFullInfo(
      {required this.supergroupId,
      this.extra});

  /// [supergroupId] Supergroup or channel identifier
  int supergroupId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetSupergroupFullInfo.fromJson(Map<String, dynamic> json) {
    return GetSupergroupFullInfo(
      supergroupId: json['supergroup_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "supergroup_id": this.supergroupId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getSupergroupFullInfo';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetSecretChat extends TdFunction {
  /// Returns information about a secret chat by its identifier. This is an offline request
  GetSecretChat(
      {required this.secretChatId,
      this.extra});

  /// [secretChatId] Secret chat identifier
  int secretChatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetSecretChat.fromJson(Map<String, dynamic> json) {
    return GetSecretChat(
      secretChatId: json['secret_chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "secret_chat_id": this.secretChatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getSecretChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChat extends TdFunction {
  /// Returns information about a chat by its identifier, this is an offline request if the current user is not a bot
  GetChat(
      {required this.chatId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChat.fromJson(Map<String, dynamic> json) {
    return GetChat(
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetMessage extends TdFunction {
  /// Returns information about a message
  GetMessage(
      {required this.chatId,
      required this.messageId,
      this.extra});

  /// [chatId] Identifier of the chat the message belongs to
  int chatId;

  /// [messageId] Identifier of the message to get
  int messageId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetMessage.fromJson(Map<String, dynamic> json) {
    return GetMessage(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getMessage';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetMessageLocally extends TdFunction {
  /// Returns information about a message, if it is available locally without sending network request. This is an offline request
  GetMessageLocally(
      {required this.chatId,
      required this.messageId,
      this.extra});

  /// [chatId] Identifier of the chat the message belongs to
  int chatId;

  /// [messageId] Identifier of the message to get
  int messageId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetMessageLocally.fromJson(Map<String, dynamic> json) {
    return GetMessageLocally(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getMessageLocally';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetRepliedMessage extends TdFunction {
  /// Returns information about a message that is replied by a given message. Also returns the pinned message, the game message, and the invoice message for messages of the types messagePinMessage, messageGameScore, and messagePaymentSuccessful respectively
  GetRepliedMessage(
      {required this.chatId,
      required this.messageId,
      this.extra});

  /// [chatId] Identifier of the chat the message belongs to
  int chatId;

  /// [messageId] Identifier of the reply message
  int messageId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetRepliedMessage.fromJson(Map<String, dynamic> json) {
    return GetRepliedMessage(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getRepliedMessage';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChatPinnedMessage extends TdFunction {
  /// Returns information about a newest pinned message in the chat
  GetChatPinnedMessage(
      {required this.chatId,
      this.extra});

  /// [chatId] Identifier of the chat the message belongs to
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChatPinnedMessage.fromJson(Map<String, dynamic> json) {
    return GetChatPinnedMessage(
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChatPinnedMessage';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetCallbackQueryMessage extends TdFunction {
  /// Returns information about a message with the callback button that originated a callback query; for bots only
  GetCallbackQueryMessage(
      {required this.chatId,
      required this.messageId,
      required this.callbackQueryId,
      this.extra});

  /// [chatId] Identifier of the chat the message belongs to
  int chatId;

  /// [messageId] Message identifier
  int messageId;

  /// [callbackQueryId] Identifier of the callback query
  int callbackQueryId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetCallbackQueryMessage.fromJson(Map<String, dynamic> json) {
    return GetCallbackQueryMessage(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      callbackQueryId: int.tryParse(json['callback_query_id'].toString()) ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "callback_query_id": this.callbackQueryId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getCallbackQueryMessage';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetMessages extends TdFunction {
  /// Returns information about messages. If a message is not found, returns null on the corresponding position of the result
  GetMessages(
      {required this.chatId,
      required this.messageIds,
      this.extra});

  /// [chatId] Identifier of the chat the messages belong to
  int chatId;

  /// [messageIds] Identifiers of the messages to get
  List<int> messageIds;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetMessages.fromJson(Map<String, dynamic> json) {
    return GetMessages(
      chatId: json['chat_id'] ?? 0,
      messageIds: List<int>.from((json['message_ids'] ?? []).map((item) => item ?? 0).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_ids": this.messageIds.map((i) => i).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getMessages';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetMessageThread extends TdFunction {
  /// Returns information about a message thread. Can be used only if message.can_get_message_thread == true
  GetMessageThread(
      {required this.chatId,
      required this.messageId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [messageId] Identifier of the message
  int messageId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetMessageThread.fromJson(Map<String, dynamic> json) {
    return GetMessageThread(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getMessageThread';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetMessageViewers extends TdFunction {
  /// Returns viewers of a recent outgoing message in a basic group or a supergroup chat. For video notes and voice notes only users, opened content of the message, are returned. The method can be called if message.can_get_viewers == true
  GetMessageViewers(
      {required this.chatId,
      required this.messageId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [messageId] Identifier of the message
  int messageId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetMessageViewers.fromJson(Map<String, dynamic> json) {
    return GetMessageViewers(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getMessageViewers';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetFile extends TdFunction {
  /// Returns information about a file; this is an offline request
  GetFile(
      {required this.fileId,
      this.extra});

  /// [fileId] Identifier of the file to get
  int fileId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetFile.fromJson(Map<String, dynamic> json) {
    return GetFile(
      fileId: json['file_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "file_id": this.fileId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getFile';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetRemoteFile extends TdFunction {
  /// Returns information about a file by its remote getRemoteFile; this is an offline request. Can be used to register a URL as a file for further uploading, or sending as a message. Even the request succeeds, the file can be used only if it is still accessible to the user.. For example, if the file is from a message, then the message must be not deleted and accessible to the user. If the file database is disabled, then the corresponding object with the file must be preloaded by the application
  GetRemoteFile(
      {required this.remoteFileId,
      required this.fileType,
      this.extra});

  /// [remoteFileId] Remote identifier of the file to get
  String remoteFileId;

  /// [fileType] File type; pass null if unknown
  FileType fileType;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetRemoteFile.fromJson(Map<String, dynamic> json) {
    return GetRemoteFile(
      remoteFileId: json['remote_file_id'] ?? "",
      fileType: FileType.fromJson(json['file_type'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "remote_file_id": this.remoteFileId,
      "file_type": this.fileType.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getRemoteFile';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class LoadChats extends TdFunction {
  /// Loads more chats from a chat list. The loaded chats and their positions in the chat list will be sent through updates. Chats are sorted by the pair (chat.position.order, chat.id) in descending order. Returns a 404 error if all chats has been loaded
  LoadChats(
      {required this.chatList,
      required this.limit,
      this.extra});

  /// [chatList] The chat list in which to load chats; pass null to load chats from the main chat list
  ChatList chatList;

  /// [limit] The maximum number of chats to be loaded. For optimal performance, the number of loaded chats is chosen by TDLib and can be smaller than the specified limit, even if the end of the list is not reached
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory LoadChats.fromJson(Map<String, dynamic> json) {
    return LoadChats(
      chatList: ChatList.fromJson(json['chat_list'] ?? <String, dynamic>{}),
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_list": this.chatList.toJson(),
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'loadChats';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChats extends TdFunction {
  /// Returns an ordered list of chats from the beginning of a chat list. For informational purposes only. Use loadChats and updates processing instead to maintain chat lists in a consistent state
  GetChats(
      {required this.chatList,
      required this.limit,
      this.extra});

  /// [chatList] The chat list in which to return chats; pass null to get chats from the main chat list
  ChatList chatList;

  /// [limit] The maximum number of chats to be returned
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChats.fromJson(Map<String, dynamic> json) {
    return GetChats(
      chatList: ChatList.fromJson(json['chat_list'] ?? <String, dynamic>{}),
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_list": this.chatList.toJson(),
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChats';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SearchPublicChat extends TdFunction {
  /// Searches a public chat by its username. Currently only private chats, supergroups and channels can be public. Returns the chat if found; otherwise an error is returned
  SearchPublicChat(
      {required this.username,
      this.extra});

  /// [username] Username to be resolved
  String username;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SearchPublicChat.fromJson(Map<String, dynamic> json) {
    return SearchPublicChat(
      username: json['username'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "username": this.username,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'searchPublicChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SearchPublicChats extends TdFunction {
  /// Searches public chats by looking for specified query in their username and title. Currently only private chats, supergroups and channels can be public. Returns a meaningful number of results.. Excludes private chats with contacts and chats from the chat list from the results
  SearchPublicChats(
      {required this.query,
      this.extra});

  /// [query] Query to search for
  String query;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SearchPublicChats.fromJson(Map<String, dynamic> json) {
    return SearchPublicChats(
      query: json['query'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "query": this.query,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'searchPublicChats';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SearchChats extends TdFunction {
  /// Searches for the specified query in the title and username of already known chats, this is an offline request. Returns chats in the order seen in the main chat list
  SearchChats(
      {required this.query,
      required this.limit,
      this.extra});

  /// [query] Query to search for. If the query is empty, returns up to 50 recently found chats
  String query;

  /// [limit] The maximum number of chats to be returned
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SearchChats.fromJson(Map<String, dynamic> json) {
    return SearchChats(
      query: json['query'] ?? "",
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "query": this.query,
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'searchChats';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SearchChatsOnServer extends TdFunction {
  /// Searches for the specified query in the title and username of already known chats via request to the server. Returns chats in the order seen in the main chat list
  SearchChatsOnServer(
      {required this.query,
      required this.limit,
      this.extra});

  /// [query] Query to search for
  String query;

  /// [limit] The maximum number of chats to be returned
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SearchChatsOnServer.fromJson(Map<String, dynamic> json) {
    return SearchChatsOnServer(
      query: json['query'] ?? "",
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "query": this.query,
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'searchChatsOnServer';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SearchChatsNearby extends TdFunction {
  /// Returns a list of users and location-based supergroups nearby. The list of users nearby will be updated for 60 seconds after the request by the updates updateUsersNearby. The request must be sent again every 25 seconds with adjusted location to not miss new chats
  SearchChatsNearby(
      {required this.location,
      this.extra});

  /// [location] Current user location
  Location location;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SearchChatsNearby.fromJson(Map<String, dynamic> json) {
    return SearchChatsNearby(
      location: Location.fromJson(json['location'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "location": this.location.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'searchChatsNearby';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetTopChats extends TdFunction {
  /// Returns a list of frequently used chats. Supported only if the chat info database is enabled
  GetTopChats(
      {required this.category,
      required this.limit,
      this.extra});

  /// [category] Category of chats to be returned
  TopChatCategory category;

  /// [limit] The maximum number of chats to be returned; up to 30
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetTopChats.fromJson(Map<String, dynamic> json) {
    return GetTopChats(
      category: TopChatCategory.fromJson(json['category'] ?? <String, dynamic>{}),
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "category": this.category.toJson(),
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getTopChats';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class RemoveTopChat extends TdFunction {
  /// Removes a chat from the list of frequently used chats. Supported only if the chat info database is enabled
  RemoveTopChat(
      {required this.category,
      required this.chatId,
      this.extra});

  /// [category] Category of frequently used chats
  TopChatCategory category;

  /// [chatId] Chat identifier
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory RemoveTopChat.fromJson(Map<String, dynamic> json) {
    return RemoveTopChat(
      category: TopChatCategory.fromJson(json['category'] ?? <String, dynamic>{}),
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "category": this.category.toJson(),
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'removeTopChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class AddRecentlyFoundChat extends TdFunction {
  /// Adds a chat to the list of recently found chats. The chat is added to the beginning of the list. If the chat is already in the list, it will be removed from the list first
  AddRecentlyFoundChat(
      {required this.chatId,
      this.extra});

  /// [chatId] Identifier of the chat to add
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory AddRecentlyFoundChat.fromJson(Map<String, dynamic> json) {
    return AddRecentlyFoundChat(
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'addRecentlyFoundChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class RemoveRecentlyFoundChat extends TdFunction {
  /// Removes a chat from the list of recently found chats
  RemoveRecentlyFoundChat(
      {required this.chatId,
      this.extra});

  /// [chatId] Identifier of the chat to be removed
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory RemoveRecentlyFoundChat.fromJson(Map<String, dynamic> json) {
    return RemoveRecentlyFoundChat(
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'removeRecentlyFoundChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ClearRecentlyFoundChats extends TdFunction {
  /// Clears the list of recently found chats
  ClearRecentlyFoundChats(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ClearRecentlyFoundChats.fromJson(Map<String, dynamic> json) {
    return ClearRecentlyFoundChats(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'clearRecentlyFoundChats';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetRecentlyOpenedChats extends TdFunction {
  /// Returns recently opened chats, this is an offline request. Returns chats in the order of last opening
  GetRecentlyOpenedChats(
      {required this.limit,
      this.extra});

  /// [limit] The maximum number of chats to be returned
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetRecentlyOpenedChats.fromJson(Map<String, dynamic> json) {
    return GetRecentlyOpenedChats(
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getRecentlyOpenedChats';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CheckChatUsername extends TdFunction {
  /// Checks whether a username can be set for a chat
  CheckChatUsername(
      {required this.chatId,
      required this.username,
      this.extra});

  /// [chatId] Chat identifier; must be identifier of a supergroup chat, or a channel chat, or a private chat with self, or zero if the chat is being created
  int chatId;

  /// [username] Username to be checked
  String username;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CheckChatUsername.fromJson(Map<String, dynamic> json) {
    return CheckChatUsername(
      chatId: json['chat_id'] ?? 0,
      username: json['username'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "username": this.username,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'checkChatUsername';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetCreatedPublicChats extends TdFunction {
  /// Returns a list of public chats of the specified type, owned by the user
  GetCreatedPublicChats(
      {required this.type,
      this.extra});

  /// [type] Type of the public chats to return
  PublicChatType type;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetCreatedPublicChats.fromJson(Map<String, dynamic> json) {
    return GetCreatedPublicChats(
      type: PublicChatType.fromJson(json['type'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "type": this.type.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getCreatedPublicChats';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CheckCreatedPublicChatsLimit extends TdFunction {
  /// Checks whether the maximum number of owned public chats has been reached. Returns corresponding error if the limit was reached
  CheckCreatedPublicChatsLimit(
      {required this.type,
      this.extra});

  /// [type] Type of the public chats, for which to check the limit
  PublicChatType type;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CheckCreatedPublicChatsLimit.fromJson(Map<String, dynamic> json) {
    return CheckCreatedPublicChatsLimit(
      type: PublicChatType.fromJson(json['type'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "type": this.type.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'checkCreatedPublicChatsLimit';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetSuitableDiscussionChats extends TdFunction {
  /// Returns a list of basic group and supergroup chats, which can be used as a discussion group for a channel. Returned basic group chats must be first upgraded to supergroups before they can be set as a discussion group. To set a returned supergroup as a discussion group, access to its old messages must be enabled using toggleSupergroupIsAllHistoryAvailable first
  GetSuitableDiscussionChats(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetSuitableDiscussionChats.fromJson(Map<String, dynamic> json) {
    return GetSuitableDiscussionChats(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getSuitableDiscussionChats';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetInactiveSupergroupChats extends TdFunction {
  /// Returns a list of recently inactive supergroups and channels. Can be used when user reaches limit on the number of joined supergroups and channels and receives CHANNELS_TOO_MUCH error
  GetInactiveSupergroupChats(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetInactiveSupergroupChats.fromJson(Map<String, dynamic> json) {
    return GetInactiveSupergroupChats(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getInactiveSupergroupChats';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetGroupsInCommon extends TdFunction {
  /// Returns a list of common group chats with a given user. Chats are sorted by their type and creation date
  GetGroupsInCommon(
      {required this.userId,
      required this.offsetChatId,
      required this.limit,
      this.extra});

  /// [userId] User identifier
  int userId;

  /// [offsetChatId] Chat identifier starting from which to return chats; use 0 for the first request
  int offsetChatId;

  /// [limit] The maximum number of chats to be returned; up to 100
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetGroupsInCommon.fromJson(Map<String, dynamic> json) {
    return GetGroupsInCommon(
      userId: json['user_id'] ?? 0,
      offsetChatId: json['offset_chat_id'] ?? 0,
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "user_id": this.userId,
      "offset_chat_id": this.offsetChatId,
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getGroupsInCommon';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChatHistory extends TdFunction {
  /// Returns messages in a chat. The messages are returned in a reverse chronological order (i.e., in order of decreasing message_id).. For optimal performance, the number of returned messages is chosen by TDLib. This is an offline request if only_local is true
  GetChatHistory(
      {required this.chatId,
      required this.fromMessageId,
      required this.offset,
      required this.limit,
      required this.onlyLocal,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [fromMessageId] Identifier of the message starting from which history must be fetched; use 0 to get results from the last message
  int fromMessageId;

  /// [offset] Specify 0 to get results from exactly the from_message_id or a negative offset up to 99 to get additionally some newer messages
  int offset;

  /// [limit] The maximum number of messages to be returned; must be positive and can't be greater than 100. If the offset is negative, the limit must be greater than or equal to -offset. For optimal performance, the number of returned messages is chosen by TDLib and can be smaller than the specified limit
  int limit;

  /// [onlyLocal] If true, returns only messages that are available locally without sending network requests
  bool onlyLocal;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChatHistory.fromJson(Map<String, dynamic> json) {
    return GetChatHistory(
      chatId: json['chat_id'] ?? 0,
      fromMessageId: json['from_message_id'] ?? 0,
      offset: json['offset'] ?? 0,
      limit: json['limit'] ?? 0,
      onlyLocal: json['only_local'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "from_message_id": this.fromMessageId,
      "offset": this.offset,
      "limit": this.limit,
      "only_local": this.onlyLocal,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChatHistory';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetMessageThreadHistory extends TdFunction {
  /// Returns messages in a message thread of a message. Can be used only if message.can_get_message_thread == true. Message thread of a channel message is in the channel's linked supergroup.. The messages are returned in a reverse chronological order (i.e., in order of decreasing message_id). For optimal performance, the number of returned messages is chosen by TDLib
  GetMessageThreadHistory(
      {required this.chatId,
      required this.messageId,
      required this.fromMessageId,
      required this.offset,
      required this.limit,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [messageId] Message identifier, which thread history needs to be returned
  int messageId;

  /// [fromMessageId] Identifier of the message starting from which history must be fetched; use 0 to get results from the last message
  int fromMessageId;

  /// [offset] Specify 0 to get results from exactly the from_message_id or a negative offset up to 99 to get additionally some newer messages
  int offset;

  /// [limit] The maximum number of messages to be returned; must be positive and can't be greater than 100. If the offset is negative, the limit must be greater than or equal to -offset. For optimal performance, the number of returned messages is chosen by TDLib and can be smaller than the specified limit
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetMessageThreadHistory.fromJson(Map<String, dynamic> json) {
    return GetMessageThreadHistory(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      fromMessageId: json['from_message_id'] ?? 0,
      offset: json['offset'] ?? 0,
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "from_message_id": this.fromMessageId,
      "offset": this.offset,
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getMessageThreadHistory';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DeleteChatHistory extends TdFunction {
  /// Deletes all messages in the chat. Use chat.can_be_deleted_only_for_self and chat.can_be_deleted_for_all_users fields to find whether and how the method can be applied to the chat
  DeleteChatHistory(
      {required this.chatId,
      required this.removeFromChatList,
      required this.revoke,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [removeFromChatList] Pass true if the chat needs to be removed from the chat list
  bool removeFromChatList;

  /// [revoke] Pass true to try to delete chat history for all users
  bool revoke;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DeleteChatHistory.fromJson(Map<String, dynamic> json) {
    return DeleteChatHistory(
      chatId: json['chat_id'] ?? 0,
      removeFromChatList: json['remove_from_chat_list'] ?? false,
      revoke: json['revoke'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "remove_from_chat_list": this.removeFromChatList,
      "revoke": this.revoke,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'deleteChatHistory';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DeleteChat extends TdFunction {
  /// Deletes a chat along with all messages in the corresponding chat for all chat members; requires owner privileges. For group chats this will release the username and remove all members. Chats with more than 1000 members can't be deleted using this method
  DeleteChat(
      {required this.chatId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DeleteChat.fromJson(Map<String, dynamic> json) {
    return DeleteChat(
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'deleteChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SearchChatMessages extends TdFunction {
  /// Searches for messages with given words in the chat. Returns the results in reverse chronological order, i.e. in order of decreasing message_id. Cannot be used in secret chats with a non-empty query. (searchSecretMessages must be used instead), or without an enabled message database. For optimal performance, the number of returned messages is chosen by TDLib and can be smaller than the specified limit
  SearchChatMessages(
      {required this.chatId,
      required this.query,
      required this.sender,
      required this.fromMessageId,
      required this.offset,
      required this.limit,
      required this.filter,
      this.messageThreadId,
      this.extra});

  /// [chatId] Identifier of the chat in which to search messages
  int chatId;

  /// [query] Query to search for
  String query;

  /// [sender] Sender of messages to search for; pass null to search for messages from any sender. Not supported in secret chats
  MessageSender sender;

  /// [fromMessageId] Identifier of the message starting from which history must be fetched; use 0 to get results from the last message
  int fromMessageId;

  /// [offset] Specify 0 to get results from exactly the from_message_id or a negative offset to get the specified message and some newer messages
  int offset;

  /// [limit] The maximum number of messages to be returned; must be positive and can't be greater than 100. If the offset is negative, the limit must be greater than -offset. For optimal performance, the number of returned messages is chosen by TDLib and can be smaller than the specified limit
  int limit;

  /// [filter] Additional filter for messages to search; pass null to search for all messages
  SearchMessagesFilter filter;

  /// [messageThreadId] If not 0, only messages in the specified thread will be returned; supergroups only
  int? messageThreadId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SearchChatMessages.fromJson(Map<String, dynamic> json) {
    return SearchChatMessages(
      chatId: json['chat_id'] ?? 0,
      query: json['query'] ?? "",
      sender: MessageSender.fromJson(json['sender'] ?? <String, dynamic>{}),
      fromMessageId: json['from_message_id'] ?? 0,
      offset: json['offset'] ?? 0,
      limit: json['limit'] ?? 0,
      filter: SearchMessagesFilter.fromJson(json['filter'] ?? <String, dynamic>{}),
      messageThreadId: json['message_thread_id'],
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "query": this.query,
      "sender": this.sender.toJson(),
      "from_message_id": this.fromMessageId,
      "offset": this.offset,
      "limit": this.limit,
      "filter": this.filter.toJson(),
      "message_thread_id": this.messageThreadId == null ? null : this.messageThreadId!,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'searchChatMessages';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SearchMessages extends TdFunction {
  /// Searches for messages in all chats except secret chats. Returns the results in reverse chronological order (i.e., in order of decreasing (date, chat_id, message_id)).. For optimal performance, the number of returned messages is chosen by TDLib and can be smaller than the specified limit
  SearchMessages(
      {required this.chatList,
      required this.query,
      required this.offsetDate,
      this.offsetChatId,
      this.offsetMessageId,
      required this.limit,
      required this.filter,
      this.minDate,
      this.maxDate,
      this.extra});

  /// [chatList] Chat list in which to search messages; pass null to search in all chats regardless of their chat list. Only Main and Archive chat lists are supported
  ChatList chatList;

  /// [query] Query to search for
  String query;

  /// [offsetDate] The date of the message starting from which the results need to be fetched. Use 0 or any date in the future to get results from the last message
  int offsetDate;

  /// [offsetChatId] The chat identifier of the last found message, or 0 for the first request
  int? offsetChatId;

  /// [offsetMessageId] The message identifier of the last found message, or 0 for the first request
  int? offsetMessageId;

  /// [limit] The maximum number of messages to be returned; up to 100. For optimal performance, the number of returned messages is chosen by TDLib and can be smaller than the specified limit
  int limit;

  /// [filter] Additional filter for messages to search; pass null to search for all messages. Filters searchMessagesFilterCall, searchMessagesFilterMissedCall, searchMessagesFilterMention, searchMessagesFilterUnreadMention, searchMessagesFilterFailedToSend and searchMessagesFilterPinned are unsupported in this function
  SearchMessagesFilter filter;

  /// [minDate] If not 0, the minimum date of the messages to return
  int? minDate;

  /// [maxDate] If not 0, the maximum date of the messages to return
  int? maxDate;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SearchMessages.fromJson(Map<String, dynamic> json) {
    return SearchMessages(
      chatList: ChatList.fromJson(json['chat_list'] ?? <String, dynamic>{}),
      query: json['query'] ?? "",
      offsetDate: json['offset_date'] ?? 0,
      offsetChatId: json['offset_chat_id'],
      offsetMessageId: json['offset_message_id'],
      limit: json['limit'] ?? 0,
      filter: SearchMessagesFilter.fromJson(json['filter'] ?? <String, dynamic>{}),
      minDate: json['min_date'],
      maxDate: json['max_date'],
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_list": this.chatList.toJson(),
      "query": this.query,
      "offset_date": this.offsetDate,
      "offset_chat_id": this.offsetChatId == null ? null : this.offsetChatId!,
      "offset_message_id": this.offsetMessageId == null ? null : this.offsetMessageId!,
      "limit": this.limit,
      "filter": this.filter.toJson(),
      "min_date": this.minDate == null ? null : this.minDate!,
      "max_date": this.maxDate == null ? null : this.maxDate!,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'searchMessages';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SearchSecretMessages extends TdFunction {
  /// Searches for messages in secret chats. Returns the results in reverse chronological order. For optimal performance, the number of returned messages is chosen by TDLib
  SearchSecretMessages(
      {required this.chatId,
      required this.query,
      required this.offset,
      required this.limit,
      required this.filter,
      this.extra});

  /// [chatId] Identifier of the chat in which to search. Specify 0 to search in all secret chats
  int chatId;

  /// [query] Query to search for. If empty, searchChatMessages must be used instead
  String query;

  /// [offset] Offset of the first entry to return as received from the previous request; use empty string to get first chunk of results
  String offset;

  /// [limit] The maximum number of messages to be returned; up to 100. For optimal performance, the number of returned messages is chosen by TDLib and can be smaller than the specified limit
  int limit;

  /// [filter] Additional filter for messages to search; pass null to search for all messages
  SearchMessagesFilter filter;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SearchSecretMessages.fromJson(Map<String, dynamic> json) {
    return SearchSecretMessages(
      chatId: json['chat_id'] ?? 0,
      query: json['query'] ?? "",
      offset: json['offset'] ?? "",
      limit: json['limit'] ?? 0,
      filter: SearchMessagesFilter.fromJson(json['filter'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "query": this.query,
      "offset": this.offset,
      "limit": this.limit,
      "filter": this.filter.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'searchSecretMessages';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SearchCallMessages extends TdFunction {
  /// Searches for call messages. Returns the results in reverse chronological order (i. e., in order of decreasing message_id). For optimal performance, the number of returned messages is chosen by TDLib
  SearchCallMessages(
      {required this.fromMessageId,
      required this.limit,
      required this.onlyMissed,
      this.extra});

  /// [fromMessageId] Identifier of the message from which to search; use 0 to get results from the last message
  int fromMessageId;

  /// [limit] The maximum number of messages to be returned; up to 100. For optimal performance, the number of returned messages is chosen by TDLib and can be smaller than the specified limit
  int limit;

  /// [onlyMissed] If true, returns only messages with missed calls
  bool onlyMissed;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SearchCallMessages.fromJson(Map<String, dynamic> json) {
    return SearchCallMessages(
      fromMessageId: json['from_message_id'] ?? 0,
      limit: json['limit'] ?? 0,
      onlyMissed: json['only_missed'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "from_message_id": this.fromMessageId,
      "limit": this.limit,
      "only_missed": this.onlyMissed,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'searchCallMessages';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DeleteAllCallMessages extends TdFunction {
  /// Deletes all call messages
  DeleteAllCallMessages(
      {required this.revoke,
      this.extra});

  /// [revoke] Pass true to delete the messages for all users
  bool revoke;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DeleteAllCallMessages.fromJson(Map<String, dynamic> json) {
    return DeleteAllCallMessages(
      revoke: json['revoke'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "revoke": this.revoke,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'deleteAllCallMessages';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SearchChatRecentLocationMessages extends TdFunction {
  /// Returns information about the recent locations of chat members that were sent to the chat. Returns up to 1 location message per user
  SearchChatRecentLocationMessages(
      {required this.chatId,
      required this.limit,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [limit] The maximum number of messages to be returned
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SearchChatRecentLocationMessages.fromJson(Map<String, dynamic> json) {
    return SearchChatRecentLocationMessages(
      chatId: json['chat_id'] ?? 0,
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'searchChatRecentLocationMessages';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetActiveLiveLocationMessages extends TdFunction {
  /// Returns all active live locations that need to be updated by the application. The list is persistent across application restarts only if the message database is used
  GetActiveLiveLocationMessages(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetActiveLiveLocationMessages.fromJson(Map<String, dynamic> json) {
    return GetActiveLiveLocationMessages(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getActiveLiveLocationMessages';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChatMessageByDate extends TdFunction {
  /// Returns the last message sent in a chat no later than the specified date
  GetChatMessageByDate(
      {required this.chatId,
      required this.date,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [date] Point in time (Unix timestamp) relative to which to search for messages
  int date;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChatMessageByDate.fromJson(Map<String, dynamic> json) {
    return GetChatMessageByDate(
      chatId: json['chat_id'] ?? 0,
      date: json['date'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "date": this.date,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChatMessageByDate';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChatSparseMessagePositions extends TdFunction {
  /// Returns sparse positions of messages of the specified type in the chat to be used for shared media scroll implementation. Returns the results in reverse chronological order (i.e., in order of decreasing message_id).. Cannot be used in secret chats or with searchMessagesFilterFailedToSend filter without an enabled message database
  GetChatSparseMessagePositions(
      {required this.chatId,
      required this.filter,
      required this.fromMessageId,
      required this.limit,
      this.extra});

  /// [chatId] Identifier of the chat in which to return information about message positions
  int chatId;

  /// [filter] Filter for message content. Filters searchMessagesFilterEmpty, searchMessagesFilterCall, searchMessagesFilterMissedCall, searchMessagesFilterMention and searchMessagesFilterUnreadMention are unsupported in this function
  SearchMessagesFilter filter;

  /// [fromMessageId] The message identifier from which to return information about message positions
  int fromMessageId;

  /// [limit] The expected number of message positions to be returned; 50-2000. A smaller number of positions can be returned, if there are not enough appropriate messages
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChatSparseMessagePositions.fromJson(Map<String, dynamic> json) {
    return GetChatSparseMessagePositions(
      chatId: json['chat_id'] ?? 0,
      filter: SearchMessagesFilter.fromJson(json['filter'] ?? <String, dynamic>{}),
      fromMessageId: json['from_message_id'] ?? 0,
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "filter": this.filter.toJson(),
      "from_message_id": this.fromMessageId,
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChatSparseMessagePositions';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChatMessageCalendar extends TdFunction {
  /// Returns information about the next messages of the specified type in the chat splitted by days. Returns the results in reverse chronological order. Can return partial result for the last returned day. Behavior of this method depends on the value of the option "utc_time_offset"
  GetChatMessageCalendar(
      {required this.chatId,
      required this.filter,
      required this.fromMessageId,
      this.extra});

  /// [chatId] Identifier of the chat in which to return information about messages
  int chatId;

  /// [filter] Filter for message content. Filters searchMessagesFilterEmpty, searchMessagesFilterCall, searchMessagesFilterMissedCall, searchMessagesFilterMention and searchMessagesFilterUnreadMention are unsupported in this function
  SearchMessagesFilter filter;

  /// [fromMessageId] The message identifier from which to return information about messages; use 0 to get results from the last message
  int fromMessageId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChatMessageCalendar.fromJson(Map<String, dynamic> json) {
    return GetChatMessageCalendar(
      chatId: json['chat_id'] ?? 0,
      filter: SearchMessagesFilter.fromJson(json['filter'] ?? <String, dynamic>{}),
      fromMessageId: json['from_message_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "filter": this.filter.toJson(),
      "from_message_id": this.fromMessageId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChatMessageCalendar';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChatMessageCount extends TdFunction {
  /// Returns approximate number of messages of the specified type in the chat
  GetChatMessageCount(
      {required this.chatId,
      required this.filter,
      required this.returnLocal,
      this.extra});

  /// [chatId] Identifier of the chat in which to count messages
  int chatId;

  /// [filter] Filter for message content; searchMessagesFilterEmpty is unsupported in this function
  SearchMessagesFilter filter;

  /// [returnLocal] If true, returns count that is available locally without sending network requests, returning -1 if the number of messages is unknown
  bool returnLocal;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChatMessageCount.fromJson(Map<String, dynamic> json) {
    return GetChatMessageCount(
      chatId: json['chat_id'] ?? 0,
      filter: SearchMessagesFilter.fromJson(json['filter'] ?? <String, dynamic>{}),
      returnLocal: json['return_local'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "filter": this.filter.toJson(),
      "return_local": this.returnLocal,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChatMessageCount';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChatScheduledMessages extends TdFunction {
  /// Returns all scheduled messages in a chat. The messages are returned in a reverse chronological order (i.e., in order of decreasing message_id)
  GetChatScheduledMessages(
      {required this.chatId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChatScheduledMessages.fromJson(Map<String, dynamic> json) {
    return GetChatScheduledMessages(
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChatScheduledMessages';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetMessagePublicForwards extends TdFunction {
  /// Returns forwarded copies of a channel message to different public channels. For optimal performance, the number of returned messages is chosen by TDLib
  GetMessagePublicForwards(
      {required this.chatId,
      required this.messageId,
      required this.offset,
      required this.limit,
      this.extra});

  /// [chatId] Chat identifier of the message
  int chatId;

  /// [messageId] Message identifier
  int messageId;

  /// [offset] Offset of the first entry to return as received from the previous request; use empty string to get first chunk of results
  String offset;

  /// [limit] The maximum number of messages to be returned; must be positive and can't be greater than 100. For optimal performance, the number of returned messages is chosen by TDLib and can be smaller than the specified limit
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetMessagePublicForwards.fromJson(Map<String, dynamic> json) {
    return GetMessagePublicForwards(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      offset: json['offset'] ?? "",
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "offset": this.offset,
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getMessagePublicForwards';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChatSponsoredMessages extends TdFunction {
  /// Returns sponsored messages to be shown in a chat; for channel chats only
  GetChatSponsoredMessages(
      {required this.chatId,
      this.extra});

  /// [chatId] Identifier of the chat
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChatSponsoredMessages.fromJson(Map<String, dynamic> json) {
    return GetChatSponsoredMessages(
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChatSponsoredMessages';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ViewSponsoredMessage extends TdFunction {
  /// Informs TDLib that a sponsored message was viewed by the user
  ViewSponsoredMessage(
      {required this.chatId,
      required this.sponsoredMessageId,
      this.extra});

  /// [chatId] Identifier of the chat with the sponsored message
  int chatId;

  /// [sponsoredMessageId] The identifier of the sponsored message being viewed
  int sponsoredMessageId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ViewSponsoredMessage.fromJson(Map<String, dynamic> json) {
    return ViewSponsoredMessage(
      chatId: json['chat_id'] ?? 0,
      sponsoredMessageId: json['sponsored_message_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "sponsored_message_id": this.sponsoredMessageId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'viewSponsoredMessage';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class RemoveNotification extends TdFunction {
  /// Removes an active notification from notification list. Needs to be called only if the notification is removed by the current user
  RemoveNotification(
      {required this.notificationGroupId,
      required this.notificationId,
      this.extra});

  /// [notificationGroupId] Identifier of notification group to which the notification belongs
  int notificationGroupId;

  /// [notificationId] Identifier of removed notification
  int notificationId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory RemoveNotification.fromJson(Map<String, dynamic> json) {
    return RemoveNotification(
      notificationGroupId: json['notification_group_id'] ?? 0,
      notificationId: json['notification_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "notification_group_id": this.notificationGroupId,
      "notification_id": this.notificationId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'removeNotification';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class RemoveNotificationGroup extends TdFunction {
  /// Removes a group of active notifications. Needs to be called only if the notification group is removed by the current user
  RemoveNotificationGroup(
      {required this.notificationGroupId,
      required this.maxNotificationId,
      this.extra});

  /// [notificationGroupId] Notification group identifier
  int notificationGroupId;

  /// [maxNotificationId] The maximum identifier of removed notifications
  int maxNotificationId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory RemoveNotificationGroup.fromJson(Map<String, dynamic> json) {
    return RemoveNotificationGroup(
      notificationGroupId: json['notification_group_id'] ?? 0,
      maxNotificationId: json['max_notification_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "notification_group_id": this.notificationGroupId,
      "max_notification_id": this.maxNotificationId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'removeNotificationGroup';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetMessageLink extends TdFunction {
  /// Returns an HTTPS link to a message in a chat. Available only for already sent messages in supergroups and channels, or if message.can_get_media_timestamp_links and a media timestamp link is generated. This is an offline request
  GetMessageLink(
      {required this.chatId,
      required this.messageId,
      this.mediaTimestamp,
      required this.forAlbum,
      required this.forComment,
      this.extra});

  /// [chatId] Identifier of the chat to which the message belongs
  int chatId;

  /// [messageId] Identifier of the message
  int messageId;

  /// [mediaTimestamp] If not 0, timestamp from which the video/audio/video note/voice note playing must start, in seconds. The media can be in the message content or in its web page preview
  int? mediaTimestamp;

  /// [forAlbum] Pass true to create a link for the whole media album
  bool forAlbum;

  /// [forComment] Pass true to create a link to the message as a channel post comment, or from a message thread
  bool forComment;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetMessageLink.fromJson(Map<String, dynamic> json) {
    return GetMessageLink(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      mediaTimestamp: json['media_timestamp'],
      forAlbum: json['for_album'] ?? false,
      forComment: json['for_comment'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "media_timestamp": this.mediaTimestamp == null ? null : this.mediaTimestamp!,
      "for_album": this.forAlbum,
      "for_comment": this.forComment,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getMessageLink';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetMessageEmbeddingCode extends TdFunction {
  /// Returns an HTML code for embedding the message. Available only for messages in supergroups and channels with a username
  GetMessageEmbeddingCode(
      {required this.chatId,
      required this.messageId,
      required this.forAlbum,
      this.extra});

  /// [chatId] Identifier of the chat to which the message belongs
  int chatId;

  /// [messageId] Identifier of the message
  int messageId;

  /// [forAlbum] Pass true to return an HTML code for embedding of the whole media album
  bool forAlbum;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetMessageEmbeddingCode.fromJson(Map<String, dynamic> json) {
    return GetMessageEmbeddingCode(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      forAlbum: json['for_album'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "for_album": this.forAlbum,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getMessageEmbeddingCode';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetMessageLinkInfo extends TdFunction {
  /// Returns information about a public or private message link. Can be called for any internal link of the type internalLinkTypeMessage
  GetMessageLinkInfo(
      {required this.url,
      this.extra});

  /// [url] The message link
  String url;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetMessageLinkInfo.fromJson(Map<String, dynamic> json) {
    return GetMessageLinkInfo(
      url: json['url'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "url": this.url,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getMessageLinkInfo';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SendMessage extends TdFunction {
  /// Sends a message. Returns the sent message
  SendMessage(
      {required this.chatId,
      this.messageThreadId,
      this.replyToMessageId,
      this.options,
      this.replyMarkup,
      required this.inputMessageContent,
      this.extra});

  /// [chatId] Target chat
  int chatId;

  /// [messageThreadId] If not 0, a message thread identifier in which the message will be sent
  int? messageThreadId;

  /// [replyToMessageId] Identifier of the message to reply to or 0
  int? replyToMessageId;

  /// [options] Options to be used to send the message; pass null to use default options
  MessageSendOptions? options;

  /// [replyMarkup] Markup for replying to the message; pass null if none; for bots only
  ReplyMarkup? replyMarkup;

  /// [inputMessageContent] The content of the message to be sent
  InputMessageContent inputMessageContent;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SendMessage.fromJson(Map<String, dynamic> json) {
    return SendMessage(
      chatId: json['chat_id'] ?? 0,
      messageThreadId: json['message_thread_id'],
      replyToMessageId: json['reply_to_message_id'],
      options: MessageSendOptions.fromJson(json['options'] ?? <String, dynamic>{}),
      replyMarkup: ReplyMarkup.fromJson(json['reply_markup'] ?? <String, dynamic>{}),
      inputMessageContent: InputMessageContent.fromJson(json['input_message_content'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_thread_id": this.messageThreadId == null ? null : this.messageThreadId!,
      "reply_to_message_id": this.replyToMessageId == null ? null : this.replyToMessageId!,
      "options": this.options == null ? null : this.options!.toJson(),
      "reply_markup": this.replyMarkup == null ? null : this.replyMarkup!.toJson(),
      "input_message_content": this.inputMessageContent.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'sendMessage';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SendMessageAlbum extends TdFunction {
  /// Sends 2-10 messages grouped together into an album. Currently only audio, document, photo and video messages can be grouped into an album. Documents and audio files can be only grouped in an album with messages of the same type. Returns sent messages
  SendMessageAlbum(
      {required this.chatId,
      this.messageThreadId,
      this.replyToMessageId,
      this.options,
      required this.inputMessageContents,
      this.extra});

  /// [chatId] Target chat
  int chatId;

  /// [messageThreadId] If not 0, a message thread identifier in which the messages will be sent
  int? messageThreadId;

  /// [replyToMessageId] Identifier of a message to reply to or 0
  int? replyToMessageId;

  /// [options] Options to be used to send the messages; pass null to use default options
  MessageSendOptions? options;

  /// [inputMessageContents] Contents of messages to be sent. At most 10 messages can be added to an album
  List<InputMessageContent> inputMessageContents;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SendMessageAlbum.fromJson(Map<String, dynamic> json) {
    return SendMessageAlbum(
      chatId: json['chat_id'] ?? 0,
      messageThreadId: json['message_thread_id'],
      replyToMessageId: json['reply_to_message_id'],
      options: MessageSendOptions.fromJson(json['options'] ?? <String, dynamic>{}),
      inputMessageContents: List<InputMessageContent>.from((json['input_message_contents'] ?? []).map((item) => InputMessageContent.fromJson(item ?? <String, dynamic>{})).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_thread_id": this.messageThreadId == null ? null : this.messageThreadId!,
      "reply_to_message_id": this.replyToMessageId == null ? null : this.replyToMessageId!,
      "options": this.options == null ? null : this.options!.toJson(),
      "input_message_contents": this.inputMessageContents.map((i) => i.toJson()).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'sendMessageAlbum';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SendBotStartMessage extends TdFunction {
  /// Invites a bot to a chat (if it is not yet a member) and sends it the /start command. Bots can't be invited to a private chat other than the chat with the bot. Bots can't be invited to channels (although they can be added as admins) and secret chats. Returns the sent message
  SendBotStartMessage(
      {required this.botUserId,
      required this.chatId,
      required this.parameter,
      this.extra});

  /// [botUserId] Identifier of the bot
  int botUserId;

  /// [chatId] Identifier of the target chat
  int chatId;

  /// [parameter] A hidden parameter sent to the bot for deep linking purposes (https://core.telegram.org/bots#deep-linking)
  String parameter;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SendBotStartMessage.fromJson(Map<String, dynamic> json) {
    return SendBotStartMessage(
      botUserId: json['bot_user_id'] ?? 0,
      chatId: json['chat_id'] ?? 0,
      parameter: json['parameter'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "bot_user_id": this.botUserId,
      "chat_id": this.chatId,
      "parameter": this.parameter,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'sendBotStartMessage';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SendInlineQueryResultMessage extends TdFunction {
  /// Sends the result of an inline query as a message. Returns the sent message. Always clears a chat draft message
  SendInlineQueryResultMessage(
      {required this.chatId,
      this.messageThreadId,
      this.replyToMessageId,
      this.options,
      required this.queryId,
      required this.resultId,
      required this.hideViaBot,
      this.extra});

  /// [chatId] Target chat
  int chatId;

  /// [messageThreadId] If not 0, a message thread identifier in which the message will be sent
  int? messageThreadId;

  /// [replyToMessageId] Identifier of a message to reply to or 0
  int? replyToMessageId;

  /// [options] Options to be used to send the message; pass null to use default options
  MessageSendOptions? options;

  /// [queryId] Identifier of the inline query
  int queryId;

  /// [resultId] Identifier of the inline result
  String resultId;

  /// [hideViaBot] If true, there will be no mention of a bot, via which the message is sent. Can be used only for bots GetOption("animation_search_bot_username"), GetOption("photo_search_bot_username") and GetOption("venue_search_bot_username")
  bool hideViaBot;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SendInlineQueryResultMessage.fromJson(Map<String, dynamic> json) {
    return SendInlineQueryResultMessage(
      chatId: json['chat_id'] ?? 0,
      messageThreadId: json['message_thread_id'],
      replyToMessageId: json['reply_to_message_id'],
      options: MessageSendOptions.fromJson(json['options'] ?? <String, dynamic>{}),
      queryId: int.tryParse(json['query_id'].toString()) ?? 0,
      resultId: json['result_id'] ?? "",
      hideViaBot: json['hide_via_bot'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_thread_id": this.messageThreadId == null ? null : this.messageThreadId!,
      "reply_to_message_id": this.replyToMessageId == null ? null : this.replyToMessageId!,
      "options": this.options == null ? null : this.options!.toJson(),
      "query_id": this.queryId,
      "result_id": this.resultId,
      "hide_via_bot": this.hideViaBot,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'sendInlineQueryResultMessage';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ForwardMessages extends TdFunction {
  /// Forwards previously sent messages. Returns the forwarded messages in the same order as the message identifiers passed in message_ids. If a message can't be forwarded, null will be returned instead of the message
  ForwardMessages(
      {required this.chatId,
      required this.fromChatId,
      required this.messageIds,
      this.options,
      required this.sendCopy,
      this.removeCaption,
      required this.onlyPreview,
      this.extra});

  /// [chatId] Identifier of the chat to which to forward messages
  int chatId;

  /// [fromChatId] Identifier of the chat from which to forward messages
  int fromChatId;

  /// [messageIds] Identifiers of the messages to forward. Message identifiers must be in a strictly increasing order. At most 100 messages can be forwarded simultaneously
  List<int> messageIds;

  /// [options] Options to be used to send the messages; pass null to use default options
  MessageSendOptions? options;

  /// [sendCopy] If true, content of the messages will be copied without reference to the original sender. Always true if the messages are forwarded to a secret chat or are local
  bool sendCopy;

  /// [removeCaption] If true, media caption of message copies will be removed. Ignored if send_copy is false
  bool? removeCaption;

  /// [onlyPreview] If true, messages will not be forwarded and instead fake messages will be returned
  bool onlyPreview;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ForwardMessages.fromJson(Map<String, dynamic> json) {
    return ForwardMessages(
      chatId: json['chat_id'] ?? 0,
      fromChatId: json['from_chat_id'] ?? 0,
      messageIds: List<int>.from((json['message_ids'] ?? []).map((item) => item ?? 0).toList()),
      options: MessageSendOptions.fromJson(json['options'] ?? <String, dynamic>{}),
      sendCopy: json['send_copy'] ?? false,
      removeCaption: json['remove_caption'],
      onlyPreview: json['only_preview'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "from_chat_id": this.fromChatId,
      "message_ids": this.messageIds.map((i) => i).toList(),
      "options": this.options == null ? null : this.options!.toJson(),
      "send_copy": this.sendCopy,
      "remove_caption": this.removeCaption == null ? null : this.removeCaption!,
      "only_preview": this.onlyPreview,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'forwardMessages';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ResendMessages extends TdFunction {
  /// Resends messages which failed to send. Can be called only for messages for which messageSendingStateFailed.can_retry is true and after specified in messageSendingStateFailed.retry_after time passed.. If a message is re-sent, the corresponding failed to send message is deleted. Returns the sent messages in the same order as the message identifiers passed in message_ids. If a message can't be re-sent, null will be returned instead of the message
  ResendMessages(
      {required this.chatId,
      required this.messageIds,
      this.extra});

  /// [chatId] Identifier of the chat to send messages
  int chatId;

  /// [messageIds] Identifiers of the messages to resend. Message identifiers must be in a strictly increasing order
  List<int> messageIds;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ResendMessages.fromJson(Map<String, dynamic> json) {
    return ResendMessages(
      chatId: json['chat_id'] ?? 0,
      messageIds: List<int>.from((json['message_ids'] ?? []).map((item) => item ?? 0).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_ids": this.messageIds.map((i) => i).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'resendMessages';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SendChatScreenshotTakenNotification extends TdFunction {
  /// Sends a notification about a screenshot taken in a chat. Supported only in private and secret chats
  SendChatScreenshotTakenNotification(
      {required this.chatId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SendChatScreenshotTakenNotification.fromJson(Map<String, dynamic> json) {
    return SendChatScreenshotTakenNotification(
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'sendChatScreenshotTakenNotification';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class AddLocalMessage extends TdFunction {
  /// Adds a local message to a chat. The message is persistent across application restarts only if the message database is used. Returns the added message
  AddLocalMessage(
      {required this.chatId,
      required this.sender,
      this.replyToMessageId,
      required this.disableNotification,
      required this.inputMessageContent,
      this.extra});

  /// [chatId] Target chat
  int chatId;

  /// [sender] The sender of the message
  MessageSender sender;

  /// [replyToMessageId] Identifier of the message to reply to or 0
  int? replyToMessageId;

  /// [disableNotification] Pass true to disable notification for the message
  bool disableNotification;

  /// [inputMessageContent] The content of the message to be added
  InputMessageContent inputMessageContent;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory AddLocalMessage.fromJson(Map<String, dynamic> json) {
    return AddLocalMessage(
      chatId: json['chat_id'] ?? 0,
      sender: MessageSender.fromJson(json['sender'] ?? <String, dynamic>{}),
      replyToMessageId: json['reply_to_message_id'],
      disableNotification: json['disable_notification'] ?? false,
      inputMessageContent: InputMessageContent.fromJson(json['input_message_content'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "sender": this.sender.toJson(),
      "reply_to_message_id": this.replyToMessageId == null ? null : this.replyToMessageId!,
      "disable_notification": this.disableNotification,
      "input_message_content": this.inputMessageContent.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'addLocalMessage';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DeleteMessages extends TdFunction {
  /// Deletes messages
  DeleteMessages(
      {required this.chatId,
      required this.messageIds,
      required this.revoke,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [messageIds] Identifiers of the messages to be deleted
  List<int> messageIds;

  /// [revoke] Pass true to try to delete messages for all chat members. Always true for supergroups, channels and secret chats
  bool revoke;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DeleteMessages.fromJson(Map<String, dynamic> json) {
    return DeleteMessages(
      chatId: json['chat_id'] ?? 0,
      messageIds: List<int>.from((json['message_ids'] ?? []).map((item) => item ?? 0).toList()),
      revoke: json['revoke'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_ids": this.messageIds.map((i) => i).toList(),
      "revoke": this.revoke,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'deleteMessages';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DeleteChatMessagesFromUser extends TdFunction {
  /// Deletes all messages sent by the specified user to a chat. Supported only for supergroups; requires can_delete_messages administrator privileges
  DeleteChatMessagesFromUser(
      {required this.chatId,
      required this.userId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [userId] User identifier
  int userId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DeleteChatMessagesFromUser.fromJson(Map<String, dynamic> json) {
    return DeleteChatMessagesFromUser(
      chatId: json['chat_id'] ?? 0,
      userId: json['user_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "user_id": this.userId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'deleteChatMessagesFromUser';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DeleteChatMessagesByDate extends TdFunction {
  /// Deletes all messages between the specified dates in a chat. Supported only for private chats and basic groups. Messages sent in the last 30 seconds will not be deleted
  DeleteChatMessagesByDate(
      {required this.chatId,
      required this.minDate,
      required this.maxDate,
      required this.revoke,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [minDate] The minimum date of the messages to delete
  int minDate;

  /// [maxDate] The maximum date of the messages to delete
  int maxDate;

  /// [revoke] Pass true to try to delete chat messages for all users; private chats only
  bool revoke;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DeleteChatMessagesByDate.fromJson(Map<String, dynamic> json) {
    return DeleteChatMessagesByDate(
      chatId: json['chat_id'] ?? 0,
      minDate: json['min_date'] ?? 0,
      maxDate: json['max_date'] ?? 0,
      revoke: json['revoke'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "min_date": this.minDate,
      "max_date": this.maxDate,
      "revoke": this.revoke,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'deleteChatMessagesByDate';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class EditMessageText extends TdFunction {
  /// Edits the text of a message (or a text of a game message). Returns the edited message after the edit is completed on the server side
  EditMessageText(
      {required this.chatId,
      required this.messageId,
      this.replyMarkup,
      required this.inputMessageContent,
      this.extra});

  /// [chatId] The chat the message belongs to
  int chatId;

  /// [messageId] Identifier of the message
  int messageId;

  /// [replyMarkup] The new message reply markup; pass null if none; for bots only
  ReplyMarkup? replyMarkup;

  /// [inputMessageContent] New text content of the message. Must be of type inputMessageText
  InputMessageContent inputMessageContent;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory EditMessageText.fromJson(Map<String, dynamic> json) {
    return EditMessageText(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      replyMarkup: ReplyMarkup.fromJson(json['reply_markup'] ?? <String, dynamic>{}),
      inputMessageContent: InputMessageContent.fromJson(json['input_message_content'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "reply_markup": this.replyMarkup == null ? null : this.replyMarkup!.toJson(),
      "input_message_content": this.inputMessageContent.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'editMessageText';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class EditMessageLiveLocation extends TdFunction {
  /// Edits the message content of a live location. Messages can be edited for a limited period of time specified in the live location. Returns the edited message after the edit is completed on the server side
  EditMessageLiveLocation(
      {required this.chatId,
      required this.messageId,
      this.replyMarkup,
      required this.location,
      required this.heading,
      required this.proximityAlertRadius,
      this.extra});

  /// [chatId] The chat the message belongs to
  int chatId;

  /// [messageId] Identifier of the message
  int messageId;

  /// [replyMarkup] The new message reply markup; pass null if none; for bots only
  ReplyMarkup? replyMarkup;

  /// [location] New location content of the message; pass null to stop sharing the live location
  Location location;

  /// [heading] The new direction in which the location moves, in degrees; 1-360. Pass 0 if unknown
  int heading;

  /// [proximityAlertRadius] The new maximum distance for proximity alerts, in meters (0-100000). Pass 0 if the notification is disabled
  int proximityAlertRadius;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory EditMessageLiveLocation.fromJson(Map<String, dynamic> json) {
    return EditMessageLiveLocation(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      replyMarkup: ReplyMarkup.fromJson(json['reply_markup'] ?? <String, dynamic>{}),
      location: Location.fromJson(json['location'] ?? <String, dynamic>{}),
      heading: json['heading'] ?? 0,
      proximityAlertRadius: json['proximity_alert_radius'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "reply_markup": this.replyMarkup == null ? null : this.replyMarkup!.toJson(),
      "location": this.location.toJson(),
      "heading": this.heading,
      "proximity_alert_radius": this.proximityAlertRadius,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'editMessageLiveLocation';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class EditMessageMedia extends TdFunction {
  /// Edits the content of a message with an animation, an audio, a document, a photo or a video, including message caption. If only the caption needs to be edited, use editMessageCaption instead.. The media can't be edited if the message was set to self-destruct or to a self-destructing media. The type of message content in an album can't be changed with exception of replacing a photo with a video or vice versa. Returns the edited message after the edit is completed on the server side
  EditMessageMedia(
      {required this.chatId,
      required this.messageId,
      this.replyMarkup,
      required this.inputMessageContent,
      this.extra});

  /// [chatId] The chat the message belongs to
  int chatId;

  /// [messageId] Identifier of the message
  int messageId;

  /// [replyMarkup] The new message reply markup; pass null if none; for bots only
  ReplyMarkup? replyMarkup;

  /// [inputMessageContent] New content of the message. Must be one of the following types: inputMessageAnimation, inputMessageAudio, inputMessageDocument, inputMessagePhoto or inputMessageVideo
  InputMessageContent inputMessageContent;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory EditMessageMedia.fromJson(Map<String, dynamic> json) {
    return EditMessageMedia(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      replyMarkup: ReplyMarkup.fromJson(json['reply_markup'] ?? <String, dynamic>{}),
      inputMessageContent: InputMessageContent.fromJson(json['input_message_content'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "reply_markup": this.replyMarkup == null ? null : this.replyMarkup!.toJson(),
      "input_message_content": this.inputMessageContent.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'editMessageMedia';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class EditMessageCaption extends TdFunction {
  /// Edits the message content caption. Returns the edited message after the edit is completed on the server side
  EditMessageCaption(
      {required this.chatId,
      required this.messageId,
      this.replyMarkup,
      this.caption,
      this.extra});

  /// [chatId] The chat the message belongs to
  int chatId;

  /// [messageId] Identifier of the message
  int messageId;

  /// [replyMarkup] The new message reply markup; pass null if none; for bots only
  ReplyMarkup? replyMarkup;

  /// [caption] New message content caption; 0-GetOption("message_caption_length_max") characters; pass null to remove caption
  FormattedText? caption;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory EditMessageCaption.fromJson(Map<String, dynamic> json) {
    return EditMessageCaption(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      replyMarkup: ReplyMarkup.fromJson(json['reply_markup'] ?? <String, dynamic>{}),
      caption: FormattedText.fromJson(json['caption'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "reply_markup": this.replyMarkup == null ? null : this.replyMarkup!.toJson(),
      "caption": this.caption == null ? null : this.caption!.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'editMessageCaption';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class EditMessageReplyMarkup extends TdFunction {
  /// Edits the message reply markup; for bots only. Returns the edited message after the edit is completed on the server side
  EditMessageReplyMarkup(
      {required this.chatId,
      required this.messageId,
      required this.replyMarkup,
      this.extra});

  /// [chatId] The chat the message belongs to
  int chatId;

  /// [messageId] Identifier of the message
  int messageId;

  /// [replyMarkup] The new message reply markup; pass null if none
  ReplyMarkup replyMarkup;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory EditMessageReplyMarkup.fromJson(Map<String, dynamic> json) {
    return EditMessageReplyMarkup(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      replyMarkup: ReplyMarkup.fromJson(json['reply_markup'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "reply_markup": this.replyMarkup.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'editMessageReplyMarkup';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class EditInlineMessageText extends TdFunction {
  /// Edits the text of an inline text or game message sent via a bot; for bots only
  EditInlineMessageText(
      {required this.inlineMessageId,
      required this.replyMarkup,
      required this.inputMessageContent,
      this.extra});

  /// [inlineMessageId] Inline message identifier
  String inlineMessageId;

  /// [replyMarkup] The new message reply markup; pass null if none
  ReplyMarkup replyMarkup;

  /// [inputMessageContent] New text content of the message. Must be of type inputMessageText
  InputMessageContent inputMessageContent;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory EditInlineMessageText.fromJson(Map<String, dynamic> json) {
    return EditInlineMessageText(
      inlineMessageId: json['inline_message_id'] ?? "",
      replyMarkup: ReplyMarkup.fromJson(json['reply_markup'] ?? <String, dynamic>{}),
      inputMessageContent: InputMessageContent.fromJson(json['input_message_content'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "inline_message_id": this.inlineMessageId,
      "reply_markup": this.replyMarkup.toJson(),
      "input_message_content": this.inputMessageContent.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'editInlineMessageText';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class EditInlineMessageLiveLocation extends TdFunction {
  /// Edits the content of a live location in an inline message sent via a bot; for bots only
  EditInlineMessageLiveLocation(
      {required this.inlineMessageId,
      required this.replyMarkup,
      required this.location,
      required this.heading,
      required this.proximityAlertRadius,
      this.extra});

  /// [inlineMessageId] Inline message identifier
  String inlineMessageId;

  /// [replyMarkup] The new message reply markup; pass null if none
  ReplyMarkup replyMarkup;

  /// [location] New location content of the message; pass null to stop sharing the live location
  Location location;

  /// [heading] The new direction in which the location moves, in degrees; 1-360. Pass 0 if unknown
  int heading;

  /// [proximityAlertRadius] The new maximum distance for proximity alerts, in meters (0-100000). Pass 0 if the notification is disabled
  int proximityAlertRadius;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory EditInlineMessageLiveLocation.fromJson(Map<String, dynamic> json) {
    return EditInlineMessageLiveLocation(
      inlineMessageId: json['inline_message_id'] ?? "",
      replyMarkup: ReplyMarkup.fromJson(json['reply_markup'] ?? <String, dynamic>{}),
      location: Location.fromJson(json['location'] ?? <String, dynamic>{}),
      heading: json['heading'] ?? 0,
      proximityAlertRadius: json['proximity_alert_radius'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "inline_message_id": this.inlineMessageId,
      "reply_markup": this.replyMarkup.toJson(),
      "location": this.location.toJson(),
      "heading": this.heading,
      "proximity_alert_radius": this.proximityAlertRadius,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'editInlineMessageLiveLocation';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class EditInlineMessageMedia extends TdFunction {
  /// Edits the content of a message with an animation, an audio, a document, a photo or a video in an inline message sent via a bot; for bots only
  EditInlineMessageMedia(
      {required this.inlineMessageId,
      this.replyMarkup,
      required this.inputMessageContent,
      this.extra});

  /// [inlineMessageId] Inline message identifier
  String inlineMessageId;

  /// [replyMarkup] The new message reply markup; pass null if none; for bots only
  ReplyMarkup? replyMarkup;

  /// [inputMessageContent] New content of the message. Must be one of the following types: inputMessageAnimation, inputMessageAudio, inputMessageDocument, inputMessagePhoto or inputMessageVideo
  InputMessageContent inputMessageContent;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory EditInlineMessageMedia.fromJson(Map<String, dynamic> json) {
    return EditInlineMessageMedia(
      inlineMessageId: json['inline_message_id'] ?? "",
      replyMarkup: ReplyMarkup.fromJson(json['reply_markup'] ?? <String, dynamic>{}),
      inputMessageContent: InputMessageContent.fromJson(json['input_message_content'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "inline_message_id": this.inlineMessageId,
      "reply_markup": this.replyMarkup == null ? null : this.replyMarkup!.toJson(),
      "input_message_content": this.inputMessageContent.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'editInlineMessageMedia';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class EditInlineMessageCaption extends TdFunction {
  /// Edits the caption of an inline message sent via a bot; for bots only
  EditInlineMessageCaption(
      {required this.inlineMessageId,
      required this.replyMarkup,
      this.caption,
      this.extra});

  /// [inlineMessageId] Inline message identifier
  String inlineMessageId;

  /// [replyMarkup] The new message reply markup; pass null if none
  ReplyMarkup replyMarkup;

  /// [caption] New message content caption; pass null to remove caption; 0-GetOption("message_caption_length_max") characters
  FormattedText? caption;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory EditInlineMessageCaption.fromJson(Map<String, dynamic> json) {
    return EditInlineMessageCaption(
      inlineMessageId: json['inline_message_id'] ?? "",
      replyMarkup: ReplyMarkup.fromJson(json['reply_markup'] ?? <String, dynamic>{}),
      caption: FormattedText.fromJson(json['caption'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "inline_message_id": this.inlineMessageId,
      "reply_markup": this.replyMarkup.toJson(),
      "caption": this.caption == null ? null : this.caption!.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'editInlineMessageCaption';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class EditInlineMessageReplyMarkup extends TdFunction {
  /// Edits the reply markup of an inline message sent via a bot; for bots only
  EditInlineMessageReplyMarkup(
      {required this.inlineMessageId,
      required this.replyMarkup,
      this.extra});

  /// [inlineMessageId] Inline message identifier
  String inlineMessageId;

  /// [replyMarkup] The new message reply markup; pass null if none
  ReplyMarkup replyMarkup;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory EditInlineMessageReplyMarkup.fromJson(Map<String, dynamic> json) {
    return EditInlineMessageReplyMarkup(
      inlineMessageId: json['inline_message_id'] ?? "",
      replyMarkup: ReplyMarkup.fromJson(json['reply_markup'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "inline_message_id": this.inlineMessageId,
      "reply_markup": this.replyMarkup.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'editInlineMessageReplyMarkup';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class EditMessageSchedulingState extends TdFunction {
  /// Edits the time when a scheduled message will be sent. Scheduling state of all messages in the same album or forwarded together with the message will be also changed
  EditMessageSchedulingState(
      {required this.chatId,
      required this.messageId,
      required this.schedulingState,
      this.extra});

  /// [chatId] The chat the message belongs to
  int chatId;

  /// [messageId] Identifier of the message
  int messageId;

  /// [schedulingState] The new message scheduling state; pass null to send the message immediately
  MessageSchedulingState schedulingState;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory EditMessageSchedulingState.fromJson(Map<String, dynamic> json) {
    return EditMessageSchedulingState(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      schedulingState: MessageSchedulingState.fromJson(json['scheduling_state'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "scheduling_state": this.schedulingState.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'editMessageSchedulingState';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetTextEntities extends TdFunction {
  /// Returns all entities (mentions, hashtags, cashtags, bot commands, bank card numbers, URLs, and email addresses) contained in the text. Can be called synchronously
  GetTextEntities(
      {required this.text,
      this.extra});

  /// [text] The text in which to look for entites
  String text;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetTextEntities.fromJson(Map<String, dynamic> json) {
    return GetTextEntities(
      text: json['text'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "text": this.text,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getTextEntities';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ParseTextEntities extends TdFunction {
  /// Parses Bold, Italic, Underline, Strikethrough, Code, Pre, PreCode, TextUrl and MentionName entities contained in the text. Can be called synchronously
  ParseTextEntities(
      {required this.text,
      required this.parseMode,
      this.extra});

  /// [text] The text to parse
  String text;

  /// [parseMode] Text parse mode
  TextParseMode parseMode;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ParseTextEntities.fromJson(Map<String, dynamic> json) {
    return ParseTextEntities(
      text: json['text'] ?? "",
      parseMode: TextParseMode.fromJson(json['parse_mode'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "text": this.text,
      "parse_mode": this.parseMode.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'parseTextEntities';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ParseMarkdown extends TdFunction {
  /// Parses Markdown entities in a human-friendly format, ignoring markup errors. Can be called synchronously
  ParseMarkdown(
      {required this.text,
      this.extra});

  /// [text] The text to parse. For example, "__italic__
  FormattedText text;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ParseMarkdown.fromJson(Map<String, dynamic> json) {
    return ParseMarkdown(
      text: FormattedText.fromJson(json['text'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "text": this.text.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'parseMarkdown';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetMarkdownText extends TdFunction {
  /// Replaces text entities with Markdown formatting in a human-friendly format. Entities that can't be represented in Markdown unambiguously are kept as is. Can be called synchronously
  GetMarkdownText(
      {required this.text,
      this.extra});

  /// [text] The text
  FormattedText text;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetMarkdownText.fromJson(Map<String, dynamic> json) {
    return GetMarkdownText(
      text: FormattedText.fromJson(json['text'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "text": this.text.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getMarkdownText';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetFileMimeType extends TdFunction {
  /// Returns the MIME type of a file, guessed by its extension. Returns an empty string on failure. Can be called synchronously
  GetFileMimeType(
      {required this.fileName,
      this.extra});

  /// [fileName] The name of the file or path to the file
  String fileName;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetFileMimeType.fromJson(Map<String, dynamic> json) {
    return GetFileMimeType(
      fileName: json['file_name'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "file_name": this.fileName,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getFileMimeType';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetFileExtension extends TdFunction {
  /// Returns the extension of a file, guessed by its MIME type. Returns an empty string on failure. Can be called synchronously
  GetFileExtension(
      {required this.mimeType,
      this.extra});

  /// [mimeType] The MIME type of the file
  String mimeType;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetFileExtension.fromJson(Map<String, dynamic> json) {
    return GetFileExtension(
      mimeType: json['mime_type'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "mime_type": this.mimeType,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getFileExtension';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CleanFileName extends TdFunction {
  /// Removes potentially dangerous characters from the name of a file. The encoding of the file name is supposed to be UTF-8. Returns an empty string on failure. Can be called synchronously
  CleanFileName(
      {required this.fileName,
      this.extra});

  /// [fileName] File name or path to the file
  String fileName;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CleanFileName.fromJson(Map<String, dynamic> json) {
    return CleanFileName(
      fileName: json['file_name'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "file_name": this.fileName,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'cleanFileName';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetLanguagePackString extends TdFunction {
  /// Returns a string stored in the local database from the specified localization target and language pack by its key. Returns a 404 error if the string is not found. Can be called synchronously
  GetLanguagePackString(
      {required this.languagePackDatabasePath,
      required this.localizationTarget,
      required this.languagePackId,
      required this.key,
      this.extra});

  /// [languagePackDatabasePath] Path to the language pack database in which strings are stored
  String languagePackDatabasePath;

  /// [localizationTarget] Localization target to which the language pack belongs
  String localizationTarget;

  /// [languagePackId] Language pack identifier
  String languagePackId;

  /// [key] Language pack key of the string to be returned
  String key;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetLanguagePackString.fromJson(Map<String, dynamic> json) {
    return GetLanguagePackString(
      languagePackDatabasePath: json['language_pack_database_path'] ?? "",
      localizationTarget: json['localization_target'] ?? "",
      languagePackId: json['language_pack_id'] ?? "",
      key: json['key'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "language_pack_database_path": this.languagePackDatabasePath,
      "localization_target": this.localizationTarget,
      "language_pack_id": this.languagePackId,
      "key": this.key,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getLanguagePackString';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetJsonValue extends TdFunction {
  /// Converts a JSON-serialized string to corresponding JsonValue object. Can be called synchronously
  GetJsonValue(
      {required this.json,
      this.extra});

  /// [json] The JSON-serialized string
  String json;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetJsonValue.fromJson(Map<String, dynamic> json) {
    return GetJsonValue(
      json: json['json'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "json": this.json,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getJsonValue';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetJsonString extends TdFunction {
  /// Converts a JsonValue object to corresponding JSON-serialized string. Can be called synchronously
  GetJsonString(
      {required this.jsonValue,
      this.extra});

  /// [jsonValue] The JsonValue object
  JsonValue jsonValue;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetJsonString.fromJson(Map<String, dynamic> json) {
    return GetJsonString(
      jsonValue: JsonValue.fromJson(json['json_value'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "json_value": this.jsonValue.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getJsonString';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetPollAnswer extends TdFunction {
  /// Changes the user answer to a poll. A poll in quiz mode can be answered only once
  SetPollAnswer(
      {required this.chatId,
      required this.messageId,
      required this.optionIds,
      this.extra});

  /// [chatId] Identifier of the chat to which the poll belongs
  int chatId;

  /// [messageId] Identifier of the message containing the poll
  int messageId;

  /// [optionIds] 0-based identifiers of answer options, chosen by the user. User can choose more than 1 answer option only is the poll allows multiple answers
  List<int> optionIds;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetPollAnswer.fromJson(Map<String, dynamic> json) {
    return SetPollAnswer(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      optionIds: List<int>.from((json['option_ids'] ?? []).map((item) => item ?? 0).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "option_ids": this.optionIds.map((i) => i).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setPollAnswer';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetPollVoters extends TdFunction {
  /// Returns users voted for the specified option in a non-anonymous polls. For optimal performance, the number of returned users is chosen by TDLib
  GetPollVoters(
      {required this.chatId,
      required this.messageId,
      required this.optionId,
      required this.offset,
      required this.limit,
      this.extra});

  /// [chatId] Identifier of the chat to which the poll belongs
  int chatId;

  /// [messageId] Identifier of the message containing the poll
  int messageId;

  /// [optionId] 0-based identifier of the answer option
  int optionId;

  /// [offset] Number of users to skip in the result; must be non-negative
  int offset;

  /// [limit] The maximum number of users to be returned; must be positive and can't be greater than 50. For optimal performance, the number of returned users is chosen by TDLib and can be smaller than the specified limit, even if the end of the voter list has not been reached
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetPollVoters.fromJson(Map<String, dynamic> json) {
    return GetPollVoters(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      optionId: json['option_id'] ?? 0,
      offset: json['offset'] ?? 0,
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "option_id": this.optionId,
      "offset": this.offset,
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getPollVoters';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class StopPoll extends TdFunction {
  /// Stops a poll. A poll in a message can be stopped when the message has can_be_edited flag set
  StopPoll(
      {required this.chatId,
      required this.messageId,
      this.replyMarkup,
      this.extra});

  /// [chatId] Identifier of the chat to which the poll belongs
  int chatId;

  /// [messageId] Identifier of the message containing the poll
  int messageId;

  /// [replyMarkup] The new message reply markup; pass null if none; for bots only
  ReplyMarkup? replyMarkup;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory StopPoll.fromJson(Map<String, dynamic> json) {
    return StopPoll(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      replyMarkup: ReplyMarkup.fromJson(json['reply_markup'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "reply_markup": this.replyMarkup == null ? null : this.replyMarkup!.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'stopPoll';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class HideSuggestedAction extends TdFunction {
  /// Hides a suggested action
  HideSuggestedAction(
      {required this.action,
      this.extra});

  /// [action] Suggested action to hide
  SuggestedAction action;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory HideSuggestedAction.fromJson(Map<String, dynamic> json) {
    return HideSuggestedAction(
      action: SuggestedAction.fromJson(json['action'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "action": this.action.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'hideSuggestedAction';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetLoginUrlInfo extends TdFunction {
  /// Returns information about a button of type inlineKeyboardButtonTypeLoginUrl. The method needs to be called when the user presses the button
  GetLoginUrlInfo(
      {required this.chatId,
      required this.messageId,
      required this.buttonId,
      this.extra});

  /// [chatId] Chat identifier of the message with the button
  int chatId;

  /// [messageId] Message identifier of the message with the button
  int messageId;

  /// [buttonId] Button identifier
  int buttonId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetLoginUrlInfo.fromJson(Map<String, dynamic> json) {
    return GetLoginUrlInfo(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      buttonId: json['button_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "button_id": this.buttonId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getLoginUrlInfo';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetLoginUrl extends TdFunction {
  /// Returns an HTTP URL which can be used to automatically authorize the user on a website after clicking an inline button of type inlineKeyboardButtonTypeLoginUrl.. Use the method getLoginUrlInfo to find whether a prior user confirmation is needed. If an error is returned, then the button must be handled as an ordinary URL button
  GetLoginUrl(
      {required this.chatId,
      required this.messageId,
      required this.buttonId,
      required this.allowWriteAccess,
      this.extra});

  /// [chatId] Chat identifier of the message with the button
  int chatId;

  /// [messageId] Message identifier of the message with the button
  int messageId;

  /// [buttonId] Button identifier
  int buttonId;

  /// [allowWriteAccess] True, if the user allowed the bot to send them messages
  bool allowWriteAccess;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetLoginUrl.fromJson(Map<String, dynamic> json) {
    return GetLoginUrl(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      buttonId: json['button_id'] ?? 0,
      allowWriteAccess: json['allow_write_access'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "button_id": this.buttonId,
      "allow_write_access": this.allowWriteAccess,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getLoginUrl';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetInlineQueryResults extends TdFunction {
  /// Sends an inline query to a bot and returns its results. Returns an error with code 502 if the bot fails to answer the query before the query timeout expires
  GetInlineQueryResults(
      {required this.botUserId,
      required this.chatId,
      required this.userLocation,
      required this.query,
      required this.offset,
      this.extra});

  /// [botUserId] The identifier of the target bot
  int botUserId;

  /// [chatId] Identifier of the chat where the query was sent
  int chatId;

  /// [userLocation] Location of the user; pass null if unknown or the bot doesn't need user's location
  Location userLocation;

  /// [query] Text of the query
  String query;

  /// [offset] Offset of the first entry to return
  String offset;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetInlineQueryResults.fromJson(Map<String, dynamic> json) {
    return GetInlineQueryResults(
      botUserId: json['bot_user_id'] ?? 0,
      chatId: json['chat_id'] ?? 0,
      userLocation: Location.fromJson(json['user_location'] ?? <String, dynamic>{}),
      query: json['query'] ?? "",
      offset: json['offset'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "bot_user_id": this.botUserId,
      "chat_id": this.chatId,
      "user_location": this.userLocation.toJson(),
      "query": this.query,
      "offset": this.offset,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getInlineQueryResults';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class AnswerInlineQuery extends TdFunction {
  /// Sets the result of an inline query; for bots only
  AnswerInlineQuery(
      {required this.inlineQueryId,
      required this.isPersonal,
      required this.results,
      required this.cacheTime,
      required this.nextOffset,
      this.switchPmText,
      required this.switchPmParameter,
      this.extra});

  /// [inlineQueryId] Identifier of the inline query
  int inlineQueryId;

  /// [isPersonal] True, if the result of the query can be cached for the specified user
  bool isPersonal;

  /// [results] The results of the query
  List<InputInlineQueryResult> results;

  /// [cacheTime] Allowed time to cache the results of the query, in seconds
  int cacheTime;

  /// [nextOffset] Offset for the next inline query; pass an empty string if there are no more results
  String nextOffset;

  /// [switchPmText] If non-empty, this text must be shown on the button that opens a private chat with the bot and sends a start message to the bot with the parameter switch_pm_parameter
  String? switchPmText;

  /// [switchPmParameter] The parameter for the bot start message
  String switchPmParameter;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory AnswerInlineQuery.fromJson(Map<String, dynamic> json) {
    return AnswerInlineQuery(
      inlineQueryId: int.tryParse(json['inline_query_id'].toString()) ?? 0,
      isPersonal: json['is_personal'] ?? false,
      results: List<InputInlineQueryResult>.from((json['results'] ?? []).map((item) => InputInlineQueryResult.fromJson(item ?? <String, dynamic>{})).toList()),
      cacheTime: json['cache_time'] ?? 0,
      nextOffset: json['next_offset'] ?? "",
      switchPmText: json['switch_pm_text'],
      switchPmParameter: json['switch_pm_parameter'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "inline_query_id": this.inlineQueryId,
      "is_personal": this.isPersonal,
      "results": this.results.map((i) => i.toJson()).toList(),
      "cache_time": this.cacheTime,
      "next_offset": this.nextOffset,
      "switch_pm_text": this.switchPmText == null ? null : this.switchPmText!,
      "switch_pm_parameter": this.switchPmParameter,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'answerInlineQuery';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetCallbackQueryAnswer extends TdFunction {
  /// Sends a callback query to a bot and returns an answer. Returns an error with code 502 if the bot fails to answer the query before the query timeout expires
  GetCallbackQueryAnswer(
      {required this.chatId,
      required this.messageId,
      required this.payload,
      this.extra});

  /// [chatId] Identifier of the chat with the message
  int chatId;

  /// [messageId] Identifier of the message from which the query originated
  int messageId;

  /// [payload] Query payload
  CallbackQueryPayload payload;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetCallbackQueryAnswer.fromJson(Map<String, dynamic> json) {
    return GetCallbackQueryAnswer(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      payload: CallbackQueryPayload.fromJson(json['payload'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "payload": this.payload.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getCallbackQueryAnswer';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class AnswerCallbackQuery extends TdFunction {
  /// Sets the result of a callback query; for bots only
  AnswerCallbackQuery(
      {required this.callbackQueryId,
      required this.text,
      required this.showAlert,
      required this.url,
      required this.cacheTime,
      this.extra});

  /// [callbackQueryId] Identifier of the callback query
  int callbackQueryId;

  /// [text] Text of the answer
  String text;

  /// [showAlert] If true, an alert must be shown to the user instead of a toast notification
  bool showAlert;

  /// [url] URL to be opened
  String url;

  /// [cacheTime] Time during which the result of the query can be cached, in seconds
  int cacheTime;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory AnswerCallbackQuery.fromJson(Map<String, dynamic> json) {
    return AnswerCallbackQuery(
      callbackQueryId: int.tryParse(json['callback_query_id'].toString()) ?? 0,
      text: json['text'] ?? "",
      showAlert: json['show_alert'] ?? false,
      url: json['url'] ?? "",
      cacheTime: json['cache_time'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "callback_query_id": this.callbackQueryId,
      "text": this.text,
      "show_alert": this.showAlert,
      "url": this.url,
      "cache_time": this.cacheTime,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'answerCallbackQuery';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class AnswerShippingQuery extends TdFunction {
  /// Sets the result of a shipping query; for bots only
  AnswerShippingQuery(
      {required this.shippingQueryId,
      required this.shippingOptions,
      required this.errorMessage,
      this.extra});

  /// [shippingQueryId] Identifier of the shipping query
  int shippingQueryId;

  /// [shippingOptions] Available shipping options
  List<ShippingOption> shippingOptions;

  /// [errorMessage] An error message, empty on success
  String errorMessage;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory AnswerShippingQuery.fromJson(Map<String, dynamic> json) {
    return AnswerShippingQuery(
      shippingQueryId: int.tryParse(json['shipping_query_id'].toString()) ?? 0,
      shippingOptions: List<ShippingOption>.from((json['shipping_options'] ?? []).map((item) => ShippingOption.fromJson(item ?? <String, dynamic>{})).toList()),
      errorMessage: json['error_message'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "shipping_query_id": this.shippingQueryId,
      "shipping_options": this.shippingOptions.map((i) => i.toJson()).toList(),
      "error_message": this.errorMessage,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'answerShippingQuery';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class AnswerPreCheckoutQuery extends TdFunction {
  /// Sets the result of a pre-checkout query; for bots only
  AnswerPreCheckoutQuery(
      {required this.preCheckoutQueryId,
      required this.errorMessage,
      this.extra});

  /// [preCheckoutQueryId] Identifier of the pre-checkout query
  int preCheckoutQueryId;

  /// [errorMessage] An error message, empty on success
  String errorMessage;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory AnswerPreCheckoutQuery.fromJson(Map<String, dynamic> json) {
    return AnswerPreCheckoutQuery(
      preCheckoutQueryId: int.tryParse(json['pre_checkout_query_id'].toString()) ?? 0,
      errorMessage: json['error_message'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "pre_checkout_query_id": this.preCheckoutQueryId,
      "error_message": this.errorMessage,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'answerPreCheckoutQuery';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetGameScore extends TdFunction {
  /// Updates the game score of the specified user in the game; for bots only
  SetGameScore(
      {required this.chatId,
      required this.messageId,
      required this.editMessage,
      required this.userId,
      required this.score,
      required this.force,
      this.extra});

  /// [chatId] The chat to which the message with the game belongs
  int chatId;

  /// [messageId] Identifier of the message
  int messageId;

  /// [editMessage] True, if the message needs to be edited
  bool editMessage;

  /// [userId] User identifier
  int userId;

  /// [score] The new score
  int score;

  /// [force] Pass true to update the score even if it decreases. If the score is 0, the user will be deleted from the high score table
  bool force;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetGameScore.fromJson(Map<String, dynamic> json) {
    return SetGameScore(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      editMessage: json['edit_message'] ?? false,
      userId: json['user_id'] ?? 0,
      score: json['score'] ?? 0,
      force: json['force'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "edit_message": this.editMessage,
      "user_id": this.userId,
      "score": this.score,
      "force": this.force,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setGameScore';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetInlineGameScore extends TdFunction {
  /// Updates the game score of the specified user in a game; for bots only
  SetInlineGameScore(
      {required this.inlineMessageId,
      required this.editMessage,
      required this.userId,
      required this.score,
      required this.force,
      this.extra});

  /// [inlineMessageId] Inline message identifier
  String inlineMessageId;

  /// [editMessage] True, if the message needs to be edited
  bool editMessage;

  /// [userId] User identifier
  int userId;

  /// [score] The new score
  int score;

  /// [force] Pass true to update the score even if it decreases. If the score is 0, the user will be deleted from the high score table
  bool force;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetInlineGameScore.fromJson(Map<String, dynamic> json) {
    return SetInlineGameScore(
      inlineMessageId: json['inline_message_id'] ?? "",
      editMessage: json['edit_message'] ?? false,
      userId: json['user_id'] ?? 0,
      score: json['score'] ?? 0,
      force: json['force'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "inline_message_id": this.inlineMessageId,
      "edit_message": this.editMessage,
      "user_id": this.userId,
      "score": this.score,
      "force": this.force,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setInlineGameScore';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetGameHighScores extends TdFunction {
  /// Returns the high scores for a game and some part of the high score table in the range of the specified user; for bots only
  GetGameHighScores(
      {required this.chatId,
      required this.messageId,
      required this.userId,
      this.extra});

  /// [chatId] The chat that contains the message with the game
  int chatId;

  /// [messageId] Identifier of the message
  int messageId;

  /// [userId] User identifier
  int userId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetGameHighScores.fromJson(Map<String, dynamic> json) {
    return GetGameHighScores(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      userId: json['user_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "user_id": this.userId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getGameHighScores';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetInlineGameHighScores extends TdFunction {
  /// Returns game high scores and some part of the high score table in the range of the specified user; for bots only
  GetInlineGameHighScores(
      {required this.inlineMessageId,
      required this.userId,
      this.extra});

  /// [inlineMessageId] Inline message identifier
  String inlineMessageId;

  /// [userId] User identifier
  int userId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetInlineGameHighScores.fromJson(Map<String, dynamic> json) {
    return GetInlineGameHighScores(
      inlineMessageId: json['inline_message_id'] ?? "",
      userId: json['user_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "inline_message_id": this.inlineMessageId,
      "user_id": this.userId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getInlineGameHighScores';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DeleteChatReplyMarkup extends TdFunction {
  /// Deletes the default reply markup from a chat. Must be called after a one-time keyboard or a ForceReply reply markup has been used. UpdateChatReplyMarkup will be sent if the reply markup is changed
  DeleteChatReplyMarkup(
      {required this.chatId,
      required this.messageId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [messageId] The message identifier of the used keyboard
  int messageId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DeleteChatReplyMarkup.fromJson(Map<String, dynamic> json) {
    return DeleteChatReplyMarkup(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'deleteChatReplyMarkup';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SendChatAction extends TdFunction {
  /// Sends a notification about user activity in a chat
  SendChatAction(
      {required this.chatId,
      this.messageThreadId,
      required this.action,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [messageThreadId] If not 0, a message thread identifier in which the action was performed
  int? messageThreadId;

  /// [action] The action description; pass null to cancel the currently active action
  ChatAction action;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SendChatAction.fromJson(Map<String, dynamic> json) {
    return SendChatAction(
      chatId: json['chat_id'] ?? 0,
      messageThreadId: json['message_thread_id'],
      action: ChatAction.fromJson(json['action'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_thread_id": this.messageThreadId == null ? null : this.messageThreadId!,
      "action": this.action.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'sendChatAction';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class OpenChat extends TdFunction {
  /// Informs TDLib that the chat is opened by the user. Many useful activities depend on the chat being opened or closed (e.g., in supergroups and channels all updates are received only for opened chats)
  OpenChat(
      {required this.chatId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory OpenChat.fromJson(Map<String, dynamic> json) {
    return OpenChat(
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'openChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CloseChat extends TdFunction {
  /// Informs TDLib that the chat is closed by the user. Many useful activities depend on the chat being opened or closed
  CloseChat(
      {required this.chatId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CloseChat.fromJson(Map<String, dynamic> json) {
    return CloseChat(
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'closeChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ViewMessages extends TdFunction {
  /// Informs TDLib that messages are being viewed by the user. Many useful activities depend on whether the messages are currently being viewed or not (e.g., marking messages as read, incrementing a view counter, updating a view counter, removing deleted messages in supergroups and channels)
  ViewMessages(
      {required this.chatId,
      this.messageThreadId,
      required this.messageIds,
      required this.forceRead,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [messageThreadId] If not 0, a message thread identifier in which the messages are being viewed
  int? messageThreadId;

  /// [messageIds] The identifiers of the messages being viewed
  List<int> messageIds;

  /// [forceRead] True, if messages in closed chats must be marked as read by the request
  bool forceRead;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ViewMessages.fromJson(Map<String, dynamic> json) {
    return ViewMessages(
      chatId: json['chat_id'] ?? 0,
      messageThreadId: json['message_thread_id'],
      messageIds: List<int>.from((json['message_ids'] ?? []).map((item) => item ?? 0).toList()),
      forceRead: json['force_read'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_thread_id": this.messageThreadId == null ? null : this.messageThreadId!,
      "message_ids": this.messageIds.map((i) => i).toList(),
      "force_read": this.forceRead,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'viewMessages';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class OpenMessageContent extends TdFunction {
  /// Informs TDLib that the message content has been opened (e.g., the user has opened a photo, video, document, location or venue, or has listened to an audio file or voice note message). An updateMessageContentOpened update will be generated if something has changed
  OpenMessageContent(
      {required this.chatId,
      required this.messageId,
      this.extra});

  /// [chatId] Chat identifier of the message
  int chatId;

  /// [messageId] Identifier of the message with the opened content
  int messageId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory OpenMessageContent.fromJson(Map<String, dynamic> json) {
    return OpenMessageContent(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'openMessageContent';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ClickAnimatedEmojiMessage extends TdFunction {
  /// Informs TDLib that a message with an animated emoji was clicked by the user. Returns a big animated sticker to be played or a 404 error if usual animation needs to be played
  ClickAnimatedEmojiMessage(
      {required this.chatId,
      required this.messageId,
      this.extra});

  /// [chatId] Chat identifier of the message
  int chatId;

  /// [messageId] Identifier of the clicked message
  int messageId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ClickAnimatedEmojiMessage.fromJson(Map<String, dynamic> json) {
    return ClickAnimatedEmojiMessage(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'clickAnimatedEmojiMessage';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetInternalLinkType extends TdFunction {
  /// Returns information about the type of an internal link. Returns a 404 error if the link is not internal. Can be called before authorization
  GetInternalLinkType(
      {required this.link,
      this.extra});

  /// [link] The link
  String link;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetInternalLinkType.fromJson(Map<String, dynamic> json) {
    return GetInternalLinkType(
      link: json['link'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "link": this.link,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getInternalLinkType';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetExternalLinkInfo extends TdFunction {
  /// Returns information about an action to be done when the current user clicks an external link. Don't use this method for links from secret chats if web page preview is disabled in secret chats
  GetExternalLinkInfo(
      {required this.link,
      this.extra});

  /// [link] The link
  String link;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetExternalLinkInfo.fromJson(Map<String, dynamic> json) {
    return GetExternalLinkInfo(
      link: json['link'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "link": this.link,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getExternalLinkInfo';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetExternalLink extends TdFunction {
  /// Returns an HTTP URL which can be used to automatically authorize the current user on a website after clicking an HTTP link. Use the method getExternalLinkInfo to find whether a prior user confirmation is needed
  GetExternalLink(
      {required this.link,
      required this.allowWriteAccess,
      this.extra});

  /// [link] The HTTP link
  String link;

  /// [allowWriteAccess] True, if the current user allowed the bot, returned in getExternalLinkInfo, to send them messages
  bool allowWriteAccess;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetExternalLink.fromJson(Map<String, dynamic> json) {
    return GetExternalLink(
      link: json['link'] ?? "",
      allowWriteAccess: json['allow_write_access'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "link": this.link,
      "allow_write_access": this.allowWriteAccess,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getExternalLink';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ReadAllChatMentions extends TdFunction {
  /// Marks all mentions in a chat as read
  ReadAllChatMentions(
      {required this.chatId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ReadAllChatMentions.fromJson(Map<String, dynamic> json) {
    return ReadAllChatMentions(
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'readAllChatMentions';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CreatePrivateChat extends TdFunction {
  /// Returns an existing chat corresponding to a given user
  CreatePrivateChat(
      {required this.userId,
      required this.force,
      this.extra});

  /// [userId] User identifier
  int userId;

  /// [force] If true, the chat will be created without network request. In this case all information about the chat except its type, title and photo can be incorrect
  bool force;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CreatePrivateChat.fromJson(Map<String, dynamic> json) {
    return CreatePrivateChat(
      userId: json['user_id'] ?? 0,
      force: json['force'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "user_id": this.userId,
      "force": this.force,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'createPrivateChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CreateBasicGroupChat extends TdFunction {
  /// Returns an existing chat corresponding to a known basic group
  CreateBasicGroupChat(
      {required this.basicGroupId,
      required this.force,
      this.extra});

  /// [basicGroupId] Basic group identifier
  int basicGroupId;

  /// [force] If true, the chat will be created without network request. In this case all information about the chat except its type, title and photo can be incorrect
  bool force;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CreateBasicGroupChat.fromJson(Map<String, dynamic> json) {
    return CreateBasicGroupChat(
      basicGroupId: json['basic_group_id'] ?? 0,
      force: json['force'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "basic_group_id": this.basicGroupId,
      "force": this.force,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'createBasicGroupChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CreateSupergroupChat extends TdFunction {
  /// Returns an existing chat corresponding to a known supergroup or channel
  CreateSupergroupChat(
      {required this.supergroupId,
      required this.force,
      this.extra});

  /// [supergroupId] Supergroup or channel identifier
  int supergroupId;

  /// [force] If true, the chat will be created without network request. In this case all information about the chat except its type, title and photo can be incorrect
  bool force;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CreateSupergroupChat.fromJson(Map<String, dynamic> json) {
    return CreateSupergroupChat(
      supergroupId: json['supergroup_id'] ?? 0,
      force: json['force'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "supergroup_id": this.supergroupId,
      "force": this.force,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'createSupergroupChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CreateSecretChat extends TdFunction {
  /// Returns an existing chat corresponding to a known secret chat
  CreateSecretChat(
      {required this.secretChatId,
      this.extra});

  /// [secretChatId] Secret chat identifier
  int secretChatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CreateSecretChat.fromJson(Map<String, dynamic> json) {
    return CreateSecretChat(
      secretChatId: json['secret_chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "secret_chat_id": this.secretChatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'createSecretChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CreateNewBasicGroupChat extends TdFunction {
  /// Creates a new basic group and sends a corresponding messageBasicGroupChatCreate. Returns the newly created chat
  CreateNewBasicGroupChat(
      {required this.userIds,
      required this.title,
      this.extra});

  /// [userIds] Identifiers of users to be added to the basic group
  List<int> userIds;

  /// [title] Title of the new basic group; 1-128 characters
  String title;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CreateNewBasicGroupChat.fromJson(Map<String, dynamic> json) {
    return CreateNewBasicGroupChat(
      userIds: List<int>.from((json['user_ids'] ?? []).map((item) => item ?? 0).toList()),
      title: json['title'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "user_ids": this.userIds.map((i) => i).toList(),
      "title": this.title,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'createNewBasicGroupChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CreateNewSupergroupChat extends TdFunction {
  /// Creates a new supergroup or channel and sends a corresponding messageSupergroupChatCreate. Returns the newly created chat
  CreateNewSupergroupChat(
      {required this.title,
      required this.isChannel,
      required this.description,
      required this.location,
      required this.forImport,
      this.extra});

  /// [title] Title of the new chat; 1-128 characters
  String title;

  /// [isChannel] True, if a channel chat needs to be created
  bool isChannel;

  /// [description] Chat description; 0-255 characters
  String description;

  /// [location] Chat location if a location-based supergroup is being created; pass null to create an ordinary supergroup chat
  ChatLocation location;

  /// [forImport] True, if the supergroup is created for importing messages using importMessage
  bool forImport;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CreateNewSupergroupChat.fromJson(Map<String, dynamic> json) {
    return CreateNewSupergroupChat(
      title: json['title'] ?? "",
      isChannel: json['is_channel'] ?? false,
      description: json['description'] ?? "",
      location: ChatLocation.fromJson(json['location'] ?? <String, dynamic>{}),
      forImport: json['for_import'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "title": this.title,
      "is_channel": this.isChannel,
      "description": this.description,
      "location": this.location.toJson(),
      "for_import": this.forImport,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'createNewSupergroupChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CreateNewSecretChat extends TdFunction {
  /// Creates a new secret chat. Returns the newly created chat
  CreateNewSecretChat(
      {required this.userId,
      this.extra});

  /// [userId] Identifier of the target user
  int userId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CreateNewSecretChat.fromJson(Map<String, dynamic> json) {
    return CreateNewSecretChat(
      userId: json['user_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "user_id": this.userId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'createNewSecretChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class UpgradeBasicGroupChatToSupergroupChat extends TdFunction {
  /// Creates a new supergroup from an existing basic group and sends a corresponding messageChatUpgradeTo and messageChatUpgradeFrom; requires creator privileges. Deactivates the original basic group
  UpgradeBasicGroupChatToSupergroupChat(
      {required this.chatId,
      this.extra});

  /// [chatId] Identifier of the chat to upgrade
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory UpgradeBasicGroupChatToSupergroupChat.fromJson(Map<String, dynamic> json) {
    return UpgradeBasicGroupChatToSupergroupChat(
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'upgradeBasicGroupChatToSupergroupChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChatListsToAddChat extends TdFunction {
  /// Returns chat lists to which the chat can be added. This is an offline request
  GetChatListsToAddChat(
      {required this.chatId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChatListsToAddChat.fromJson(Map<String, dynamic> json) {
    return GetChatListsToAddChat(
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChatListsToAddChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class AddChatToList extends TdFunction {
  /// Adds a chat to a chat list. A chat can't be simultaneously in Main and Archive chat lists, so it is automatically removed from another one if needed
  AddChatToList(
      {required this.chatId,
      required this.chatList,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [chatList] The chat list. Use getChatListsToAddChat to get suitable chat lists
  ChatList chatList;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory AddChatToList.fromJson(Map<String, dynamic> json) {
    return AddChatToList(
      chatId: json['chat_id'] ?? 0,
      chatList: ChatList.fromJson(json['chat_list'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "chat_list": this.chatList.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'addChatToList';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChatFilter extends TdFunction {
  /// Returns information about a chat filter by its identifier
  GetChatFilter(
      {required this.chatFilterId,
      this.extra});

  /// [chatFilterId] Chat filter identifier
  int chatFilterId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChatFilter.fromJson(Map<String, dynamic> json) {
    return GetChatFilter(
      chatFilterId: json['chat_filter_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_filter_id": this.chatFilterId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChatFilter';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CreateChatFilter extends TdFunction {
  /// Creates new chat filter. Returns information about the created chat filter
  CreateChatFilter(
      {required this.filter,
      this.extra});

  /// [filter] Chat filter
  ChatFilter filter;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CreateChatFilter.fromJson(Map<String, dynamic> json) {
    return CreateChatFilter(
      filter: ChatFilter.fromJson(json['filter'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "filter": this.filter.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'createChatFilter';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class EditChatFilter extends TdFunction {
  /// Edits existing chat filter. Returns information about the edited chat filter
  EditChatFilter(
      {required this.chatFilterId,
      required this.filter,
      this.extra});

  /// [chatFilterId] Chat filter identifier
  int chatFilterId;

  /// [filter] The edited chat filter
  ChatFilter filter;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory EditChatFilter.fromJson(Map<String, dynamic> json) {
    return EditChatFilter(
      chatFilterId: json['chat_filter_id'] ?? 0,
      filter: ChatFilter.fromJson(json['filter'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_filter_id": this.chatFilterId,
      "filter": this.filter.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'editChatFilter';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DeleteChatFilter extends TdFunction {
  /// Deletes existing chat filter
  DeleteChatFilter(
      {required this.chatFilterId,
      this.extra});

  /// [chatFilterId] Chat filter identifier
  int chatFilterId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DeleteChatFilter.fromJson(Map<String, dynamic> json) {
    return DeleteChatFilter(
      chatFilterId: json['chat_filter_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_filter_id": this.chatFilterId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'deleteChatFilter';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ReorderChatFilters extends TdFunction {
  /// Changes the order of chat filters
  ReorderChatFilters(
      {required this.chatFilterIds,
      this.extra});

  /// [chatFilterIds] Identifiers of chat filters in the new correct order
  List<int> chatFilterIds;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ReorderChatFilters.fromJson(Map<String, dynamic> json) {
    return ReorderChatFilters(
      chatFilterIds: List<int>.from((json['chat_filter_ids'] ?? []).map((item) => item ?? 0).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_filter_ids": this.chatFilterIds.map((i) => i).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'reorderChatFilters';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetRecommendedChatFilters extends TdFunction {
  /// Returns recommended chat filters for the current user
  GetRecommendedChatFilters(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetRecommendedChatFilters.fromJson(Map<String, dynamic> json) {
    return GetRecommendedChatFilters(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getRecommendedChatFilters';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChatFilterDefaultIconName extends TdFunction {
  /// Returns default icon name for a filter. Can be called synchronously
  GetChatFilterDefaultIconName(
      {required this.filter,
      this.extra});

  /// [filter] Chat filter
  ChatFilter filter;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChatFilterDefaultIconName.fromJson(Map<String, dynamic> json) {
    return GetChatFilterDefaultIconName(
      filter: ChatFilter.fromJson(json['filter'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "filter": this.filter.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChatFilterDefaultIconName';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetChatTitle extends TdFunction {
  /// Changes the chat title. Supported only for basic groups, supergroups and channels. Requires can_change_info administrator right
  SetChatTitle(
      {required this.chatId,
      required this.title,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [title] New title of the chat; 1-128 characters
  String title;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetChatTitle.fromJson(Map<String, dynamic> json) {
    return SetChatTitle(
      chatId: json['chat_id'] ?? 0,
      title: json['title'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "title": this.title,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setChatTitle';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetChatPhoto extends TdFunction {
  /// Changes the photo of a chat. Supported only for basic groups, supergroups and channels. Requires can_change_info administrator right
  SetChatPhoto(
      {required this.chatId,
      required this.photo,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [photo] New chat photo; pass null to delete the chat photo
  InputChatPhoto photo;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetChatPhoto.fromJson(Map<String, dynamic> json) {
    return SetChatPhoto(
      chatId: json['chat_id'] ?? 0,
      photo: InputChatPhoto.fromJson(json['photo'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "photo": this.photo.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setChatPhoto';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetChatMessageTtlSetting extends TdFunction {
  /// Changes the message TTL setting (sets a new self-destruct timer) in a chat. Requires can_delete_messages administrator right in basic groups, supergroups and channels. Message TTL setting of a chat with the current user (Saved Messages) and the chat 777000 (Telegram) can't be changed
  SetChatMessageTtlSetting(
      {required this.chatId,
      required this.ttl,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [ttl] New TTL value, in seconds; must be one of 0, 86400, 7 * 86400, or 31 * 86400 unless the chat is secret
  int ttl;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetChatMessageTtlSetting.fromJson(Map<String, dynamic> json) {
    return SetChatMessageTtlSetting(
      chatId: json['chat_id'] ?? 0,
      ttl: json['ttl'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "ttl": this.ttl,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setChatMessageTtlSetting';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetChatPermissions extends TdFunction {
  /// Changes the chat members permissions. Supported only for basic groups and supergroups. Requires can_restrict_members administrator right
  SetChatPermissions(
      {required this.chatId,
      required this.permissions,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [permissions] New non-administrator members permissions in the chat
  ChatPermissions permissions;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetChatPermissions.fromJson(Map<String, dynamic> json) {
    return SetChatPermissions(
      chatId: json['chat_id'] ?? 0,
      permissions: ChatPermissions.fromJson(json['permissions'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "permissions": this.permissions.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setChatPermissions';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetChatTheme extends TdFunction {
  /// Changes the chat theme. Supported only in private and secret chats
  SetChatTheme(
      {required this.chatId,
      required this.themeName,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [themeName] Name of the new chat theme; pass an empty string to return the default theme
  String themeName;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetChatTheme.fromJson(Map<String, dynamic> json) {
    return SetChatTheme(
      chatId: json['chat_id'] ?? 0,
      themeName: json['theme_name'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "theme_name": this.themeName,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setChatTheme';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetChatDraftMessage extends TdFunction {
  /// Changes the draft message in a chat
  SetChatDraftMessage(
      {required this.chatId,
      this.messageThreadId,
      required this.draftMessage,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [messageThreadId] If not 0, a message thread identifier in which the draft was changed
  int? messageThreadId;

  /// [draftMessage] New draft message; pass null to remove the draft
  DraftMessage draftMessage;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetChatDraftMessage.fromJson(Map<String, dynamic> json) {
    return SetChatDraftMessage(
      chatId: json['chat_id'] ?? 0,
      messageThreadId: json['message_thread_id'],
      draftMessage: DraftMessage.fromJson(json['draft_message'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_thread_id": this.messageThreadId == null ? null : this.messageThreadId!,
      "draft_message": this.draftMessage.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setChatDraftMessage';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetChatNotificationSettings extends TdFunction {
  /// Changes the notification settings of a chat. Notification settings of a chat with the current user (Saved Messages) can't be changed
  SetChatNotificationSettings(
      {required this.chatId,
      required this.notificationSettings,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [notificationSettings] New notification settings for the chat. If the chat is muted for more than 1 week, it is considered to be muted forever
  ChatNotificationSettings notificationSettings;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetChatNotificationSettings.fromJson(Map<String, dynamic> json) {
    return SetChatNotificationSettings(
      chatId: json['chat_id'] ?? 0,
      notificationSettings: ChatNotificationSettings.fromJson(json['notification_settings'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "notification_settings": this.notificationSettings.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setChatNotificationSettings';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ToggleChatIsMarkedAsUnread extends TdFunction {
  /// Changes the marked as unread state of a chat
  ToggleChatIsMarkedAsUnread(
      {required this.chatId,
      required this.isMarkedAsUnread,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [isMarkedAsUnread] New value of is_marked_as_unread
  bool isMarkedAsUnread;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ToggleChatIsMarkedAsUnread.fromJson(Map<String, dynamic> json) {
    return ToggleChatIsMarkedAsUnread(
      chatId: json['chat_id'] ?? 0,
      isMarkedAsUnread: json['is_marked_as_unread'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "is_marked_as_unread": this.isMarkedAsUnread,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'toggleChatIsMarkedAsUnread';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ToggleChatDefaultDisableNotification extends TdFunction {
  /// Changes the value of the default disable_notification parameter, used when a message is sent to a chat
  ToggleChatDefaultDisableNotification(
      {required this.chatId,
      required this.defaultDisableNotification,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [defaultDisableNotification] New value of default_disable_notification
  bool defaultDisableNotification;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ToggleChatDefaultDisableNotification.fromJson(Map<String, dynamic> json) {
    return ToggleChatDefaultDisableNotification(
      chatId: json['chat_id'] ?? 0,
      defaultDisableNotification: json['default_disable_notification'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "default_disable_notification": this.defaultDisableNotification,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'toggleChatDefaultDisableNotification';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetChatClientData extends TdFunction {
  /// Changes application-specific data associated with a chat
  SetChatClientData(
      {required this.chatId,
      required this.clientData,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [clientData] New value of client_data
  String clientData;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetChatClientData.fromJson(Map<String, dynamic> json) {
    return SetChatClientData(
      chatId: json['chat_id'] ?? 0,
      clientData: json['client_data'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "client_data": this.clientData,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setChatClientData';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetChatDescription extends TdFunction {
  /// Changes information about a chat. Available for basic groups, supergroups, and channels. Requires can_change_info administrator right
  SetChatDescription(
      {required this.chatId,
      required this.description,
      this.extra});

  /// [chatId] Identifier of the chat
  int chatId;

  /// [description] New chat description; 0-255 characters
  String description;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetChatDescription.fromJson(Map<String, dynamic> json) {
    return SetChatDescription(
      chatId: json['chat_id'] ?? 0,
      description: json['description'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "description": this.description,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setChatDescription';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetChatDiscussionGroup extends TdFunction {
  /// Changes the discussion group of a channel chat; requires can_change_info administrator right in the channel if it is specified
  SetChatDiscussionGroup(
      {required this.chatId,
      required this.discussionChatId,
      this.extra});

  /// [chatId] Identifier of the channel chat. Pass 0 to remove a link from the supergroup passed in the second argument to a linked channel chat (requires can_pin_messages rights in the supergroup)
  int chatId;

  /// [discussionChatId] Identifier of a new channel's discussion group. Use 0 to remove the discussion group.. Use the method getSuitableDiscussionChats to find all suitable groups. Basic group chats must be first upgraded to supergroup chats. If new chat members don't have access to old messages in the supergroup, then toggleSupergroupIsAllHistoryAvailable must be used first to change that
  int discussionChatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetChatDiscussionGroup.fromJson(Map<String, dynamic> json) {
    return SetChatDiscussionGroup(
      chatId: json['chat_id'] ?? 0,
      discussionChatId: json['discussion_chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "discussion_chat_id": this.discussionChatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setChatDiscussionGroup';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetChatLocation extends TdFunction {
  /// Changes the location of a chat. Available only for some location-based supergroups, use supergroupFullInfo.can_set_location to check whether the method is allowed to use
  SetChatLocation(
      {required this.chatId,
      required this.location,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [location] New location for the chat; must be valid and not null
  ChatLocation location;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetChatLocation.fromJson(Map<String, dynamic> json) {
    return SetChatLocation(
      chatId: json['chat_id'] ?? 0,
      location: ChatLocation.fromJson(json['location'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "location": this.location.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setChatLocation';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetChatSlowModeDelay extends TdFunction {
  /// Changes the slow mode delay of a chat. Available only for supergroups; requires can_restrict_members rights
  SetChatSlowModeDelay(
      {required this.chatId,
      required this.slowModeDelay,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [slowModeDelay] New slow mode delay for the chat, in seconds; must be one of 0, 10, 30, 60, 300, 900, 3600
  int slowModeDelay;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetChatSlowModeDelay.fromJson(Map<String, dynamic> json) {
    return SetChatSlowModeDelay(
      chatId: json['chat_id'] ?? 0,
      slowModeDelay: json['slow_mode_delay'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "slow_mode_delay": this.slowModeDelay,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setChatSlowModeDelay';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class PinChatMessage extends TdFunction {
  /// Pins a message in a chat; requires can_pin_messages rights or can_edit_messages rights in the channel
  PinChatMessage(
      {required this.chatId,
      required this.messageId,
      required this.disableNotification,
      required this.onlyForSelf,
      this.extra});

  /// [chatId] Identifier of the chat
  int chatId;

  /// [messageId] Identifier of the new pinned message
  int messageId;

  /// [disableNotification] True, if there must be no notification about the pinned message. Notifications are always disabled in channels and private chats
  bool disableNotification;

  /// [onlyForSelf] True, if the message needs to be pinned for one side only; private chats only
  bool onlyForSelf;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory PinChatMessage.fromJson(Map<String, dynamic> json) {
    return PinChatMessage(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      disableNotification: json['disable_notification'] ?? false,
      onlyForSelf: json['only_for_self'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "disable_notification": this.disableNotification,
      "only_for_self": this.onlyForSelf,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'pinChatMessage';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class UnpinChatMessage extends TdFunction {
  /// Removes a pinned message from a chat; requires can_pin_messages rights in the group or can_edit_messages rights in the channel
  UnpinChatMessage(
      {required this.chatId,
      required this.messageId,
      this.extra});

  /// [chatId] Identifier of the chat
  int chatId;

  /// [messageId] Identifier of the removed pinned message
  int messageId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory UnpinChatMessage.fromJson(Map<String, dynamic> json) {
    return UnpinChatMessage(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'unpinChatMessage';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class UnpinAllChatMessages extends TdFunction {
  /// Removes all pinned messages from a chat; requires can_pin_messages rights in the group or can_edit_messages rights in the channel
  UnpinAllChatMessages(
      {required this.chatId,
      this.extra});

  /// [chatId] Identifier of the chat
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory UnpinAllChatMessages.fromJson(Map<String, dynamic> json) {
    return UnpinAllChatMessages(
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'unpinAllChatMessages';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class JoinChat extends TdFunction {
  /// Adds the current user as a new member to a chat. Private and secret chats can't be joined using this method
  JoinChat(
      {required this.chatId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory JoinChat.fromJson(Map<String, dynamic> json) {
    return JoinChat(
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'joinChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class LeaveChat extends TdFunction {
  /// Removes the current user from chat members. Private and secret chats can't be left using this method
  LeaveChat(
      {required this.chatId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory LeaveChat.fromJson(Map<String, dynamic> json) {
    return LeaveChat(
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'leaveChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class AddChatMember extends TdFunction {
  /// Adds a new member to a chat. Members can't be added to private or secret chats
  AddChatMember(
      {required this.chatId,
      required this.userId,
      required this.forwardLimit,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [userId] Identifier of the user
  int userId;

  /// [forwardLimit] The number of earlier messages from the chat to be forwarded to the new member; up to 100. Ignored for supergroups and channels, or if the added user is a bot
  int forwardLimit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory AddChatMember.fromJson(Map<String, dynamic> json) {
    return AddChatMember(
      chatId: json['chat_id'] ?? 0,
      userId: json['user_id'] ?? 0,
      forwardLimit: json['forward_limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "user_id": this.userId,
      "forward_limit": this.forwardLimit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'addChatMember';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class AddChatMembers extends TdFunction {
  /// Adds multiple new members to a chat. Currently this method is only available for supergroups and channels. This method can't be used to join a chat. Members can't be added to a channel if it has more than 200 members
  AddChatMembers(
      {required this.chatId,
      required this.userIds,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [userIds] Identifiers of the users to be added to the chat. The maximum number of added users is 20 for supergroups and 100 for channels
  List<int> userIds;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory AddChatMembers.fromJson(Map<String, dynamic> json) {
    return AddChatMembers(
      chatId: json['chat_id'] ?? 0,
      userIds: List<int>.from((json['user_ids'] ?? []).map((item) => item ?? 0).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "user_ids": this.userIds.map((i) => i).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'addChatMembers';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetChatMemberStatus extends TdFunction {
  /// Changes the status of a chat member, needs appropriate privileges. This function is currently not suitable for transferring chat ownership; use transferChatOwnership instead. Use addChatMember or banChatMember if some additional parameters needs to be passed
  SetChatMemberStatus(
      {required this.chatId,
      required this.memberId,
      required this.status,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [memberId] Member identifier. Chats can be only banned and unbanned in supergroups and channels
  MessageSender memberId;

  /// [status] The new status of the member in the chat
  ChatMemberStatus status;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetChatMemberStatus.fromJson(Map<String, dynamic> json) {
    return SetChatMemberStatus(
      chatId: json['chat_id'] ?? 0,
      memberId: MessageSender.fromJson(json['member_id'] ?? <String, dynamic>{}),
      status: ChatMemberStatus.fromJson(json['status'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "member_id": this.memberId.toJson(),
      "status": this.status.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setChatMemberStatus';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class BanChatMember extends TdFunction {
  /// Bans a member in a chat. Members can't be banned in private or secret chats. In supergroups and channels, the user will not be able to return to the group on their own using invite links, etc., unless unbanned first
  BanChatMember(
      {required this.chatId,
      required this.memberId,
      required this.bannedUntilDate,
      required this.revokeMessages,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [memberId] Member identifier
  MessageSender memberId;

  /// [bannedUntilDate] Point in time (Unix timestamp) when the user will be unbanned; 0 if never. If the user is banned for more than 366 days or for less than 30 seconds from the current time, the user is considered to be banned forever. Ignored in basic groups
  int bannedUntilDate;

  /// [revokeMessages] Pass true to delete all messages in the chat for the user that is being removed. Always true for supergroups and channels
  bool revokeMessages;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory BanChatMember.fromJson(Map<String, dynamic> json) {
    return BanChatMember(
      chatId: json['chat_id'] ?? 0,
      memberId: MessageSender.fromJson(json['member_id'] ?? <String, dynamic>{}),
      bannedUntilDate: json['banned_until_date'] ?? 0,
      revokeMessages: json['revoke_messages'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "member_id": this.memberId.toJson(),
      "banned_until_date": this.bannedUntilDate,
      "revoke_messages": this.revokeMessages,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'banChatMember';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CanTransferOwnership extends TdFunction {
  /// Checks whether the current session can be used to transfer a chat ownership to another user
  CanTransferOwnership(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CanTransferOwnership.fromJson(Map<String, dynamic> json) {
    return CanTransferOwnership(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'canTransferOwnership';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class TransferChatOwnership extends TdFunction {
  /// Changes the owner of a chat. The current user must be a current owner of the chat. Use the method canTransferOwnership to check whether the ownership can be transferred from the current session. Available only for supergroups and channel chats
  TransferChatOwnership(
      {required this.chatId,
      required this.userId,
      required this.password,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [userId] Identifier of the user to which transfer the ownership. The ownership can't be transferred to a bot or to a deleted user
  int userId;

  /// [password] The password of the current user
  String password;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory TransferChatOwnership.fromJson(Map<String, dynamic> json) {
    return TransferChatOwnership(
      chatId: json['chat_id'] ?? 0,
      userId: json['user_id'] ?? 0,
      password: json['password'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "user_id": this.userId,
      "password": this.password,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'transferChatOwnership';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChatMember extends TdFunction {
  /// Returns information about a single member of a chat
  GetChatMember(
      {required this.chatId,
      required this.memberId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [memberId] Member identifier
  MessageSender memberId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChatMember.fromJson(Map<String, dynamic> json) {
    return GetChatMember(
      chatId: json['chat_id'] ?? 0,
      memberId: MessageSender.fromJson(json['member_id'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "member_id": this.memberId.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChatMember';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SearchChatMembers extends TdFunction {
  /// Searches for a specified query in the first name, last name and username of the members of a specified chat. Requires administrator rights in channels
  SearchChatMembers(
      {required this.chatId,
      required this.query,
      required this.limit,
      required this.filter,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [query] Query to search for
  String query;

  /// [limit] The maximum number of users to be returned; up to 200
  int limit;

  /// [filter] The type of users to search for; pass null to search among all chat members
  ChatMembersFilter filter;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SearchChatMembers.fromJson(Map<String, dynamic> json) {
    return SearchChatMembers(
      chatId: json['chat_id'] ?? 0,
      query: json['query'] ?? "",
      limit: json['limit'] ?? 0,
      filter: ChatMembersFilter.fromJson(json['filter'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "query": this.query,
      "limit": this.limit,
      "filter": this.filter.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'searchChatMembers';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChatAdministrators extends TdFunction {
  /// Returns a list of administrators of the chat with their custom titles
  GetChatAdministrators(
      {required this.chatId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChatAdministrators.fromJson(Map<String, dynamic> json) {
    return GetChatAdministrators(
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChatAdministrators';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ClearAllDraftMessages extends TdFunction {
  /// Clears draft messages in all chats
  ClearAllDraftMessages(
      {required this.excludeSecretChats,
      this.extra});

  /// [excludeSecretChats] If true, local draft messages in secret chats will not be cleared
  bool excludeSecretChats;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ClearAllDraftMessages.fromJson(Map<String, dynamic> json) {
    return ClearAllDraftMessages(
      excludeSecretChats: json['exclude_secret_chats'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "exclude_secret_chats": this.excludeSecretChats,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'clearAllDraftMessages';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChatNotificationSettingsExceptions extends TdFunction {
  /// Returns list of chats with non-default notification settings
  GetChatNotificationSettingsExceptions(
      {required this.scope,
      required this.compareSound,
      this.extra});

  /// [scope] If specified, only chats from the scope will be returned; pass null to return chats from all scopes
  NotificationSettingsScope scope;

  /// [compareSound] If true, also chats with non-default sound will be returned
  bool compareSound;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChatNotificationSettingsExceptions.fromJson(Map<String, dynamic> json) {
    return GetChatNotificationSettingsExceptions(
      scope: NotificationSettingsScope.fromJson(json['scope'] ?? <String, dynamic>{}),
      compareSound: json['compare_sound'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "scope": this.scope.toJson(),
      "compare_sound": this.compareSound,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChatNotificationSettingsExceptions';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetScopeNotificationSettings extends TdFunction {
  /// Returns the notification settings for chats of a given type
  GetScopeNotificationSettings(
      {required this.scope,
      this.extra});

  /// [scope] Types of chats for which to return the notification settings information
  NotificationSettingsScope scope;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetScopeNotificationSettings.fromJson(Map<String, dynamic> json) {
    return GetScopeNotificationSettings(
      scope: NotificationSettingsScope.fromJson(json['scope'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "scope": this.scope.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getScopeNotificationSettings';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetScopeNotificationSettings extends TdFunction {
  /// Changes notification settings for chats of a given type
  SetScopeNotificationSettings(
      {required this.scope,
      required this.notificationSettings,
      this.extra});

  /// [scope] Types of chats for which to change the notification settings
  NotificationSettingsScope scope;

  /// [notificationSettings] The new notification settings for the given scope
  ScopeNotificationSettings notificationSettings;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetScopeNotificationSettings.fromJson(Map<String, dynamic> json) {
    return SetScopeNotificationSettings(
      scope: NotificationSettingsScope.fromJson(json['scope'] ?? <String, dynamic>{}),
      notificationSettings: ScopeNotificationSettings.fromJson(json['notification_settings'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "scope": this.scope.toJson(),
      "notification_settings": this.notificationSettings.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setScopeNotificationSettings';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ResetAllNotificationSettings extends TdFunction {
  /// Resets all notification settings to their default values. By default, all chats are unmuted, the sound is set to "default" and message previews are shown
  ResetAllNotificationSettings(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ResetAllNotificationSettings.fromJson(Map<String, dynamic> json) {
    return ResetAllNotificationSettings(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'resetAllNotificationSettings';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ToggleChatIsPinned extends TdFunction {
  /// Changes the pinned state of a chat. There can be up to GetOption("pinned_chat_count_max")/GetOption("pinned_archived_chat_count_max") pinned non-secret chats and the same number of secret chats in the main/arhive chat list
  ToggleChatIsPinned(
      {required this.chatList,
      required this.chatId,
      required this.isPinned,
      this.extra});

  /// [chatList] Chat list in which to change the pinned state of the chat
  ChatList chatList;

  /// [chatId] Chat identifier
  int chatId;

  /// [isPinned] True, if the chat is pinned
  bool isPinned;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ToggleChatIsPinned.fromJson(Map<String, dynamic> json) {
    return ToggleChatIsPinned(
      chatList: ChatList.fromJson(json['chat_list'] ?? <String, dynamic>{}),
      chatId: json['chat_id'] ?? 0,
      isPinned: json['is_pinned'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_list": this.chatList.toJson(),
      "chat_id": this.chatId,
      "is_pinned": this.isPinned,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'toggleChatIsPinned';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetPinnedChats extends TdFunction {
  /// Changes the order of pinned chats
  SetPinnedChats(
      {required this.chatList,
      required this.chatIds,
      this.extra});

  /// [chatList] Chat list in which to change the order of pinned chats
  ChatList chatList;

  /// [chatIds] The new list of pinned chats
  List<int> chatIds;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetPinnedChats.fromJson(Map<String, dynamic> json) {
    return SetPinnedChats(
      chatList: ChatList.fromJson(json['chat_list'] ?? <String, dynamic>{}),
      chatIds: List<int>.from((json['chat_ids'] ?? []).map((item) => item ?? 0).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_list": this.chatList.toJson(),
      "chat_ids": this.chatIds.map((i) => i).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setPinnedChats';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DownloadFile extends TdFunction {
  /// Downloads a file from the cloud. Download progress and completion of the download will be notified through updateFile updates
  DownloadFile(
      {required this.fileId,
      required this.priority,
      required this.offset,
      required this.limit,
      required this.synchronous,
      this.extra});

  /// [fileId] Identifier of the file to download
  int fileId;

  /// [priority] Priority of the download (1-32). The higher the priority, the earlier the file will be downloaded. If the priorities of two files are equal, then the last one for which downloadFile was called will be downloaded first
  int priority;

  /// [offset] The starting position from which the file needs to be downloaded
  int offset;

  /// [limit] Number of bytes which need to be downloaded starting from the "offset" position before the download will be automatically canceled; use 0 to download without a limit
  int limit;

  /// [synchronous] If false, this request returns file state just after the download has been started. If true, this request returns file state only after. the download has succeeded, has failed, has been canceled or a new downloadFile request with different offset/limit parameters was sent
  bool synchronous;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DownloadFile.fromJson(Map<String, dynamic> json) {
    return DownloadFile(
      fileId: json['file_id'] ?? 0,
      priority: json['priority'] ?? 0,
      offset: json['offset'] ?? 0,
      limit: json['limit'] ?? 0,
      synchronous: json['synchronous'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "file_id": this.fileId,
      "priority": this.priority,
      "offset": this.offset,
      "limit": this.limit,
      "synchronous": this.synchronous,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'downloadFile';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetFileDownloadedPrefixSize extends TdFunction {
  /// Returns file downloaded prefix size from a given offset, in bytes
  GetFileDownloadedPrefixSize(
      {required this.fileId,
      required this.offset,
      this.extra});

  /// [fileId] Identifier of the file
  int fileId;

  /// [offset] Offset from which downloaded prefix size needs to be calculated
  int offset;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetFileDownloadedPrefixSize.fromJson(Map<String, dynamic> json) {
    return GetFileDownloadedPrefixSize(
      fileId: json['file_id'] ?? 0,
      offset: json['offset'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "file_id": this.fileId,
      "offset": this.offset,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getFileDownloadedPrefixSize';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CancelDownloadFile extends TdFunction {
  /// Stops the downloading of a file. If a file has already been downloaded, does nothing
  CancelDownloadFile(
      {required this.fileId,
      required this.onlyIfPending,
      this.extra});

  /// [fileId] Identifier of a file to stop downloading
  int fileId;

  /// [onlyIfPending] Pass true to stop downloading only if it hasn't been started, i.e. request hasn't been sent to server
  bool onlyIfPending;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CancelDownloadFile.fromJson(Map<String, dynamic> json) {
    return CancelDownloadFile(
      fileId: json['file_id'] ?? 0,
      onlyIfPending: json['only_if_pending'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "file_id": this.fileId,
      "only_if_pending": this.onlyIfPending,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'cancelDownloadFile';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetSuggestedFileName extends TdFunction {
  /// Returns suggested name for saving a file in a given directory
  GetSuggestedFileName(
      {required this.fileId,
      required this.directory,
      this.extra});

  /// [fileId] Identifier of the file
  int fileId;

  /// [directory] Directory in which the file is supposed to be saved
  String directory;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetSuggestedFileName.fromJson(Map<String, dynamic> json) {
    return GetSuggestedFileName(
      fileId: json['file_id'] ?? 0,
      directory: json['directory'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "file_id": this.fileId,
      "directory": this.directory,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getSuggestedFileName';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class UploadFile extends TdFunction {
  /// Asynchronously uploads a file to the cloud without sending it in a message. updateFile will be used to notify about upload progress and successful completion of the upload. The file will not have a persistent remote identifier until it will be sent in a message
  UploadFile(
      {required this.file,
      required this.fileType,
      required this.priority,
      this.extra});

  /// [file] File to upload
  InputFile file;

  /// [fileType] File type; pass null if unknown
  FileType fileType;

  /// [priority] Priority of the upload (1-32). The higher the priority, the earlier the file will be uploaded. If the priorities of two files are equal, then the first one for which uploadFile was called will be uploaded first
  int priority;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory UploadFile.fromJson(Map<String, dynamic> json) {
    return UploadFile(
      file: InputFile.fromJson(json['file'] ?? <String, dynamic>{}),
      fileType: FileType.fromJson(json['file_type'] ?? <String, dynamic>{}),
      priority: json['priority'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "file": this.file.toJson(),
      "file_type": this.fileType.toJson(),
      "priority": this.priority,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'uploadFile';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CancelUploadFile extends TdFunction {
  /// Stops the uploading of a file. Supported only for files uploaded by using uploadFile. For other files the behavior is undefined
  CancelUploadFile(
      {required this.fileId,
      this.extra});

  /// [fileId] Identifier of the file to stop uploading
  int fileId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CancelUploadFile.fromJson(Map<String, dynamic> json) {
    return CancelUploadFile(
      fileId: json['file_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "file_id": this.fileId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'cancelUploadFile';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class WriteGeneratedFilePart extends TdFunction {
  /// Writes a part of a generated file. This method is intended to be used only if the application has no direct access to TDLib's file system, because it is usually slower than a direct write to the destination file
  WriteGeneratedFilePart(
      {required this.generationId,
      required this.offset,
      required this.data,
      this.extra});

  /// [generationId] The identifier of the generation process
  int generationId;

  /// [offset] The offset from which to write the data to the file
  int offset;

  /// [data] The data to write
  String data;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory WriteGeneratedFilePart.fromJson(Map<String, dynamic> json) {
    return WriteGeneratedFilePart(
      generationId: int.tryParse(json['generation_id'].toString()) ?? 0,
      offset: json['offset'] ?? 0,
      data: json['data'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "generation_id": this.generationId,
      "offset": this.offset,
      "data": this.data,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'writeGeneratedFilePart';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetFileGenerationProgress extends TdFunction {
  /// Informs TDLib on a file generation progress
  SetFileGenerationProgress(
      {required this.generationId,
      required this.expectedSize,
      required this.localPrefixSize,
      this.extra});

  /// [generationId] The identifier of the generation process
  int generationId;

  /// [expectedSize] Expected size of the generated file, in bytes; 0 if unknown
  int expectedSize;

  /// [localPrefixSize] The number of bytes already generated
  int localPrefixSize;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetFileGenerationProgress.fromJson(Map<String, dynamic> json) {
    return SetFileGenerationProgress(
      generationId: int.tryParse(json['generation_id'].toString()) ?? 0,
      expectedSize: json['expected_size'] ?? 0,
      localPrefixSize: json['local_prefix_size'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "generation_id": this.generationId,
      "expected_size": this.expectedSize,
      "local_prefix_size": this.localPrefixSize,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setFileGenerationProgress';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class FinishFileGeneration extends TdFunction {
  /// Finishes the file generation
  FinishFileGeneration(
      {required this.generationId,
      required this.error,
      this.extra});

  /// [generationId] The identifier of the generation process
  int generationId;

  /// [error] If passed, the file generation has failed and must be terminated; pass null if the file generation succeeded
  TdError error;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory FinishFileGeneration.fromJson(Map<String, dynamic> json) {
    return FinishFileGeneration(
      generationId: int.tryParse(json['generation_id'].toString()) ?? 0,
      error: TdError.fromJson(json['error'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "generation_id": this.generationId,
      "error": this.error.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'finishFileGeneration';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ReadFilePart extends TdFunction {
  /// Reads a part of a file from the TDLib file cache and returns read bytes. This method is intended to be used only if the application has no direct access to TDLib's file system, because it is usually slower than a direct read from the file
  ReadFilePart(
      {required this.fileId,
      required this.offset,
      required this.count,
      this.extra});

  /// [fileId] Identifier of the file. The file must be located in the TDLib file cache
  int fileId;

  /// [offset] The offset from which to read the file
  int offset;

  /// [count] Number of bytes to read. An error will be returned if there are not enough bytes available in the file from the specified position. Pass 0 to read all available data from the specified position
  int count;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ReadFilePart.fromJson(Map<String, dynamic> json) {
    return ReadFilePart(
      fileId: json['file_id'] ?? 0,
      offset: json['offset'] ?? 0,
      count: json['count'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "file_id": this.fileId,
      "offset": this.offset,
      "count": this.count,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'readFilePart';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DeleteFile extends TdFunction {
  /// Deletes a file from the TDLib file cache
  DeleteFile(
      {required this.fileId,
      this.extra});

  /// [fileId] Identifier of the file to delete
  int fileId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DeleteFile.fromJson(Map<String, dynamic> json) {
    return DeleteFile(
      fileId: json['file_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "file_id": this.fileId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'deleteFile';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetMessageFileType extends TdFunction {
  /// Returns information about a file with messages exported from another app
  GetMessageFileType(
      {required this.messageFileHead,
      this.extra});

  /// [messageFileHead] Beginning of the message file; up to 100 first lines
  String messageFileHead;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetMessageFileType.fromJson(Map<String, dynamic> json) {
    return GetMessageFileType(
      messageFileHead: json['message_file_head'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "message_file_head": this.messageFileHead,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getMessageFileType';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetMessageImportConfirmationText extends TdFunction {
  /// Returns a confirmation text to be shown to the user before starting message import
  GetMessageImportConfirmationText(
      {required this.chatId,
      this.extra});

  /// [chatId] Identifier of a chat to which the messages will be imported. It must be an identifier of a private chat with a mutual contact or an identifier of a supergroup chat with can_change_info administrator right
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetMessageImportConfirmationText.fromJson(Map<String, dynamic> json) {
    return GetMessageImportConfirmationText(
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getMessageImportConfirmationText';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ImportMessages extends TdFunction {
  /// Imports messages exported from another app
  ImportMessages(
      {required this.chatId,
      required this.messageFile,
      required this.attachedFiles,
      this.extra});

  /// [chatId] Identifier of a chat to which the messages will be imported. It must be an identifier of a private chat with a mutual contact or an identifier of a supergroup chat with can_change_info administrator right
  int chatId;

  /// [messageFile] File with messages to import. Only inputFileLocal and inputFileGenerated are supported. The file must not be previously uploaded
  InputFile messageFile;

  /// [attachedFiles] Files used in the imported messages. Only inputFileLocal and inputFileGenerated are supported. The files must not be previously uploaded
  List<InputFile> attachedFiles;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ImportMessages.fromJson(Map<String, dynamic> json) {
    return ImportMessages(
      chatId: json['chat_id'] ?? 0,
      messageFile: InputFile.fromJson(json['message_file'] ?? <String, dynamic>{}),
      attachedFiles: List<InputFile>.from((json['attached_files'] ?? []).map((item) => InputFile.fromJson(item ?? <String, dynamic>{})).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_file": this.messageFile.toJson(),
      "attached_files": this.attachedFiles.map((i) => i.toJson()).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'importMessages';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ReplacePrimaryChatInviteLink extends TdFunction {
  /// Replaces current primary invite link for a chat with a new primary invite link. Available for basic groups, supergroups, and channels. Requires administrator privileges and can_invite_users right
  ReplacePrimaryChatInviteLink(
      {required this.chatId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ReplacePrimaryChatInviteLink.fromJson(Map<String, dynamic> json) {
    return ReplacePrimaryChatInviteLink(
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'replacePrimaryChatInviteLink';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CreateChatInviteLink extends TdFunction {
  /// Creates a new invite link for a chat. Available for basic groups, supergroups, and channels. Requires administrator privileges and can_invite_users right in the chat
  CreateChatInviteLink(
      {required this.chatId,
      required this.name,
      required this.expireDate,
      required this.memberLimit,
      required this.createsJoinRequest,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [name] Invite link name; 0-32 characters
  String name;

  /// [expireDate] Point in time (Unix timestamp) when the link will expire; pass 0 if never
  int expireDate;

  /// [memberLimit] The maximum number of chat members that can join the chat by the link simultaneously; 0-99999; pass 0 if not limited
  int memberLimit;

  /// [createsJoinRequest] True, if the link only creates join request. If true, member_limit must not be specified
  bool createsJoinRequest;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CreateChatInviteLink.fromJson(Map<String, dynamic> json) {
    return CreateChatInviteLink(
      chatId: json['chat_id'] ?? 0,
      name: json['name'] ?? "",
      expireDate: json['expire_date'] ?? 0,
      memberLimit: json['member_limit'] ?? 0,
      createsJoinRequest: json['creates_join_request'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "name": this.name,
      "expire_date": this.expireDate,
      "member_limit": this.memberLimit,
      "creates_join_request": this.createsJoinRequest,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'createChatInviteLink';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class EditChatInviteLink extends TdFunction {
  /// Edits a non-primary invite link for a chat. Available for basic groups, supergroups, and channels. Requires administrator privileges and can_invite_users right in the chat for own links and owner privileges for other links
  EditChatInviteLink(
      {required this.chatId,
      required this.inviteLink,
      required this.name,
      required this.expireDate,
      required this.memberLimit,
      required this.createsJoinRequest,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [inviteLink] Invite link to be edited
  String inviteLink;

  /// [name] Invite link name; 0-32 characters
  String name;

  /// [expireDate] Point in time (Unix timestamp) when the link will expire; pass 0 if never
  int expireDate;

  /// [memberLimit] The maximum number of chat members that can join the chat by the link simultaneously; 0-99999; pass 0 if not limited
  int memberLimit;

  /// [createsJoinRequest] True, if the link only creates join request. If true, member_limit must not be specified
  bool createsJoinRequest;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory EditChatInviteLink.fromJson(Map<String, dynamic> json) {
    return EditChatInviteLink(
      chatId: json['chat_id'] ?? 0,
      inviteLink: json['invite_link'] ?? "",
      name: json['name'] ?? "",
      expireDate: json['expire_date'] ?? 0,
      memberLimit: json['member_limit'] ?? 0,
      createsJoinRequest: json['creates_join_request'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "invite_link": this.inviteLink,
      "name": this.name,
      "expire_date": this.expireDate,
      "member_limit": this.memberLimit,
      "creates_join_request": this.createsJoinRequest,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'editChatInviteLink';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChatInviteLink extends TdFunction {
  /// Returns information about an invite link. Requires administrator privileges and can_invite_users right in the chat to get own links and owner privileges to get other links
  GetChatInviteLink(
      {required this.chatId,
      required this.inviteLink,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [inviteLink] Invite link to get
  String inviteLink;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChatInviteLink.fromJson(Map<String, dynamic> json) {
    return GetChatInviteLink(
      chatId: json['chat_id'] ?? 0,
      inviteLink: json['invite_link'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "invite_link": this.inviteLink,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChatInviteLink';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChatInviteLinkCounts extends TdFunction {
  /// Returns list of chat administrators with number of their invite links. Requires owner privileges in the chat
  GetChatInviteLinkCounts(
      {required this.chatId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChatInviteLinkCounts.fromJson(Map<String, dynamic> json) {
    return GetChatInviteLinkCounts(
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChatInviteLinkCounts';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChatInviteLinks extends TdFunction {
  /// Returns invite links for a chat created by specified administrator. Requires administrator privileges and can_invite_users right in the chat to get own links and owner privileges to get other links
  GetChatInviteLinks(
      {required this.chatId,
      required this.creatorUserId,
      required this.isRevoked,
      required this.offsetDate,
      required this.offsetInviteLink,
      required this.limit,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [creatorUserId] User identifier of a chat administrator. Must be an identifier of the current user for non-owner
  int creatorUserId;

  /// [isRevoked] Pass true if revoked links needs to be returned instead of active or expired
  bool isRevoked;

  /// [offsetDate] Creation date of an invite link starting after which to return invite links; use 0 to get results from the beginning
  int offsetDate;

  /// [offsetInviteLink] Invite link starting after which to return invite links; use empty string to get results from the beginning
  String offsetInviteLink;

  /// [limit] The maximum number of invite links to return; up to 100
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChatInviteLinks.fromJson(Map<String, dynamic> json) {
    return GetChatInviteLinks(
      chatId: json['chat_id'] ?? 0,
      creatorUserId: json['creator_user_id'] ?? 0,
      isRevoked: json['is_revoked'] ?? false,
      offsetDate: json['offset_date'] ?? 0,
      offsetInviteLink: json['offset_invite_link'] ?? "",
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "creator_user_id": this.creatorUserId,
      "is_revoked": this.isRevoked,
      "offset_date": this.offsetDate,
      "offset_invite_link": this.offsetInviteLink,
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChatInviteLinks';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChatInviteLinkMembers extends TdFunction {
  /// Returns chat members joined a chat by an invite link. Requires administrator privileges and can_invite_users right in the chat for own links and owner privileges for other links
  GetChatInviteLinkMembers(
      {required this.chatId,
      required this.inviteLink,
      required this.offsetMember,
      required this.limit,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [inviteLink] Invite link for which to return chat members
  String inviteLink;

  /// [offsetMember] A chat member from which to return next chat members; pass null to get results from the beginning
  ChatInviteLinkMember offsetMember;

  /// [limit] The maximum number of chat members to return; up to 100
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChatInviteLinkMembers.fromJson(Map<String, dynamic> json) {
    return GetChatInviteLinkMembers(
      chatId: json['chat_id'] ?? 0,
      inviteLink: json['invite_link'] ?? "",
      offsetMember: ChatInviteLinkMember.fromJson(json['offset_member'] ?? <String, dynamic>{}),
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "invite_link": this.inviteLink,
      "offset_member": this.offsetMember.toJson(),
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChatInviteLinkMembers';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class RevokeChatInviteLink extends TdFunction {
  /// Revokes invite link for a chat. Available for basic groups, supergroups, and channels. Requires administrator privileges and can_invite_users right in the chat for own links and owner privileges for other links.. If a primary link is revoked, then additionally to the revoked link returns new primary link
  RevokeChatInviteLink(
      {required this.chatId,
      required this.inviteLink,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [inviteLink] Invite link to be revoked
  String inviteLink;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory RevokeChatInviteLink.fromJson(Map<String, dynamic> json) {
    return RevokeChatInviteLink(
      chatId: json['chat_id'] ?? 0,
      inviteLink: json['invite_link'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "invite_link": this.inviteLink,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'revokeChatInviteLink';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DeleteRevokedChatInviteLink extends TdFunction {
  /// Deletes revoked chat invite links. Requires administrator privileges and can_invite_users right in the chat for own links and owner privileges for other links
  DeleteRevokedChatInviteLink(
      {required this.chatId,
      required this.inviteLink,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [inviteLink] Invite link to revoke
  String inviteLink;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DeleteRevokedChatInviteLink.fromJson(Map<String, dynamic> json) {
    return DeleteRevokedChatInviteLink(
      chatId: json['chat_id'] ?? 0,
      inviteLink: json['invite_link'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "invite_link": this.inviteLink,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'deleteRevokedChatInviteLink';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DeleteAllRevokedChatInviteLinks extends TdFunction {
  /// Deletes all revoked chat invite links created by a given chat administrator. Requires administrator privileges and can_invite_users right in the chat for own links and owner privileges for other links
  DeleteAllRevokedChatInviteLinks(
      {required this.chatId,
      required this.creatorUserId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [creatorUserId] User identifier of a chat administrator, which links will be deleted. Must be an identifier of the current user for non-owner
  int creatorUserId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DeleteAllRevokedChatInviteLinks.fromJson(Map<String, dynamic> json) {
    return DeleteAllRevokedChatInviteLinks(
      chatId: json['chat_id'] ?? 0,
      creatorUserId: json['creator_user_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "creator_user_id": this.creatorUserId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'deleteAllRevokedChatInviteLinks';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CheckChatInviteLink extends TdFunction {
  /// Checks the validity of an invite link for a chat and returns information about the corresponding chat
  CheckChatInviteLink(
      {required this.inviteLink,
      this.extra});

  /// [inviteLink] Invite link to be checked
  String inviteLink;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CheckChatInviteLink.fromJson(Map<String, dynamic> json) {
    return CheckChatInviteLink(
      inviteLink: json['invite_link'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "invite_link": this.inviteLink,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'checkChatInviteLink';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class JoinChatByInviteLink extends TdFunction {
  /// Uses an invite link to add the current user to the chat if possible
  JoinChatByInviteLink(
      {required this.inviteLink,
      this.extra});

  /// [inviteLink] Invite link to use
  String inviteLink;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory JoinChatByInviteLink.fromJson(Map<String, dynamic> json) {
    return JoinChatByInviteLink(
      inviteLink: json['invite_link'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "invite_link": this.inviteLink,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'joinChatByInviteLink';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChatJoinRequests extends TdFunction {
  /// Returns pending join requests in a chat
  GetChatJoinRequests(
      {required this.chatId,
      required this.inviteLink,
      required this.query,
      required this.offsetRequest,
      required this.limit,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [inviteLink] Invite link for which to return join requests. If empty, all join requests will be returned. Requires administrator privileges and can_invite_users right in the chat for own links and owner privileges for other links
  String inviteLink;

  /// [query] A query to search for in the first names, last names and usernames of the users to return
  String query;

  /// [offsetRequest] A chat join request from which to return next requests; pass null to get results from the beginning
  ChatJoinRequest offsetRequest;

  /// [limit] The maximum number of chat join requests to return
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChatJoinRequests.fromJson(Map<String, dynamic> json) {
    return GetChatJoinRequests(
      chatId: json['chat_id'] ?? 0,
      inviteLink: json['invite_link'] ?? "",
      query: json['query'] ?? "",
      offsetRequest: ChatJoinRequest.fromJson(json['offset_request'] ?? <String, dynamic>{}),
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "invite_link": this.inviteLink,
      "query": this.query,
      "offset_request": this.offsetRequest.toJson(),
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChatJoinRequests';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ApproveChatJoinRequest extends TdFunction {
  /// Approves pending join request in a chat
  ApproveChatJoinRequest(
      {required this.chatId,
      required this.userId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [userId] Identifier of the user, which request will be approved
  int userId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ApproveChatJoinRequest.fromJson(Map<String, dynamic> json) {
    return ApproveChatJoinRequest(
      chatId: json['chat_id'] ?? 0,
      userId: json['user_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "user_id": this.userId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'approveChatJoinRequest';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DeclineChatJoinRequest extends TdFunction {
  /// Declines pending join request in a chat
  DeclineChatJoinRequest(
      {required this.chatId,
      required this.userId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [userId] Identifier of the user, which request will be declined
  int userId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DeclineChatJoinRequest.fromJson(Map<String, dynamic> json) {
    return DeclineChatJoinRequest(
      chatId: json['chat_id'] ?? 0,
      userId: json['user_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "user_id": this.userId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'declineChatJoinRequest';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CreateCall extends TdFunction {
  /// Creates a new call
  CreateCall(
      {required this.userId,
      required this.protocol,
      required this.isVideo,
      this.extra});

  /// [userId] Identifier of the user to be called
  int userId;

  /// [protocol] The call protocols supported by the application
  CallProtocol protocol;

  /// [isVideo] True, if a video call needs to be created
  bool isVideo;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CreateCall.fromJson(Map<String, dynamic> json) {
    return CreateCall(
      userId: json['user_id'] ?? 0,
      protocol: CallProtocol.fromJson(json['protocol'] ?? <String, dynamic>{}),
      isVideo: json['is_video'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "user_id": this.userId,
      "protocol": this.protocol.toJson(),
      "is_video": this.isVideo,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'createCall';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class AcceptCall extends TdFunction {
  /// Accepts an incoming call
  AcceptCall(
      {required this.callId,
      required this.protocol,
      this.extra});

  /// [callId] Call identifier
  int callId;

  /// [protocol] The call protocols supported by the application
  CallProtocol protocol;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory AcceptCall.fromJson(Map<String, dynamic> json) {
    return AcceptCall(
      callId: json['call_id'] ?? 0,
      protocol: CallProtocol.fromJson(json['protocol'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "call_id": this.callId,
      "protocol": this.protocol.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'acceptCall';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SendCallSignalingData extends TdFunction {
  /// Sends call signaling data
  SendCallSignalingData(
      {required this.callId,
      required this.data,
      this.extra});

  /// [callId] Call identifier
  int callId;

  /// [data] The data
  String data;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SendCallSignalingData.fromJson(Map<String, dynamic> json) {
    return SendCallSignalingData(
      callId: json['call_id'] ?? 0,
      data: json['data'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "call_id": this.callId,
      "data": this.data,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'sendCallSignalingData';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DiscardCall extends TdFunction {
  /// Discards a call
  DiscardCall(
      {required this.callId,
      required this.isDisconnected,
      required this.duration,
      required this.isVideo,
      required this.connectionId,
      this.extra});

  /// [callId] Call identifier
  int callId;

  /// [isDisconnected] True, if the user was disconnected
  bool isDisconnected;

  /// [duration] The call duration, in seconds
  int duration;

  /// [isVideo] True, if the call was a video call
  bool isVideo;

  /// [connectionId] Identifier of the connection used during the call
  int connectionId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DiscardCall.fromJson(Map<String, dynamic> json) {
    return DiscardCall(
      callId: json['call_id'] ?? 0,
      isDisconnected: json['is_disconnected'] ?? false,
      duration: json['duration'] ?? 0,
      isVideo: json['is_video'] ?? false,
      connectionId: int.tryParse(json['connection_id'].toString()) ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "call_id": this.callId,
      "is_disconnected": this.isDisconnected,
      "duration": this.duration,
      "is_video": this.isVideo,
      "connection_id": this.connectionId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'discardCall';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SendCallRating extends TdFunction {
  /// Sends a call rating
  SendCallRating(
      {required this.callId,
      required this.rating,
      required this.comment,
      required this.problems,
      this.extra});

  /// [callId] Call identifier
  int callId;

  /// [rating] Call rating; 1-5
  int rating;

  /// [comment] An optional user comment if the rating is less than 5
  String comment;

  /// [problems] List of the exact types of problems with the call, specified by the user
  List<CallProblem> problems;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SendCallRating.fromJson(Map<String, dynamic> json) {
    return SendCallRating(
      callId: json['call_id'] ?? 0,
      rating: json['rating'] ?? 0,
      comment: json['comment'] ?? "",
      problems: List<CallProblem>.from((json['problems'] ?? []).map((item) => CallProblem.fromJson(item ?? <String, dynamic>{})).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "call_id": this.callId,
      "rating": this.rating,
      "comment": this.comment,
      "problems": this.problems.map((i) => i.toJson()).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'sendCallRating';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SendCallDebugInformation extends TdFunction {
  /// Sends debug information for a call
  SendCallDebugInformation(
      {required this.callId,
      required this.debugInformation,
      this.extra});

  /// [callId] Call identifier
  int callId;

  /// [debugInformation] Debug information in application-specific format
  String debugInformation;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SendCallDebugInformation.fromJson(Map<String, dynamic> json) {
    return SendCallDebugInformation(
      callId: json['call_id'] ?? 0,
      debugInformation: json['debug_information'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "call_id": this.callId,
      "debug_information": this.debugInformation,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'sendCallDebugInformation';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetVideoChatAvailableParticipants extends TdFunction {
  /// Returns list of participant identifiers, which can be used to join video chats in a chat
  GetVideoChatAvailableParticipants(
      {required this.chatId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetVideoChatAvailableParticipants.fromJson(Map<String, dynamic> json) {
    return GetVideoChatAvailableParticipants(
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getVideoChatAvailableParticipants';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetVideoChatDefaultParticipant extends TdFunction {
  /// Changes default participant identifier, which can be used to join video chats in a chat
  SetVideoChatDefaultParticipant(
      {required this.chatId,
      required this.defaultParticipantId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [defaultParticipantId] Default group call participant identifier to join the video chats
  MessageSender defaultParticipantId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetVideoChatDefaultParticipant.fromJson(Map<String, dynamic> json) {
    return SetVideoChatDefaultParticipant(
      chatId: json['chat_id'] ?? 0,
      defaultParticipantId: MessageSender.fromJson(json['default_participant_id'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "default_participant_id": this.defaultParticipantId.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setVideoChatDefaultParticipant';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CreateVideoChat extends TdFunction {
  /// Creates a video chat (a group call bound to a chat). Available only for basic groups, supergroups and channels; requires can_manage_video_chats rights
  CreateVideoChat(
      {required this.chatId,
      required this.title,
      required this.startDate,
      this.extra});

  /// [chatId] Chat identifier, in which the video chat will be created
  int chatId;

  /// [title] Group call title; if empty, chat title will be used
  String title;

  /// [startDate] Point in time (Unix timestamp) when the group call is supposed to be started by an administrator; 0 to start the video chat immediately. The date must be at least 10 seconds and at most 8 days in the future
  int startDate;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CreateVideoChat.fromJson(Map<String, dynamic> json) {
    return CreateVideoChat(
      chatId: json['chat_id'] ?? 0,
      title: json['title'] ?? "",
      startDate: json['start_date'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "title": this.title,
      "start_date": this.startDate,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'createVideoChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetGroupCall extends TdFunction {
  /// Returns information about a group call
  GetGroupCall(
      {required this.groupCallId,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetGroupCall.fromJson(Map<String, dynamic> json) {
    return GetGroupCall(
      groupCallId: json['group_call_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getGroupCall';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class StartScheduledGroupCall extends TdFunction {
  /// Starts a scheduled group call
  StartScheduledGroupCall(
      {required this.groupCallId,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory StartScheduledGroupCall.fromJson(Map<String, dynamic> json) {
    return StartScheduledGroupCall(
      groupCallId: json['group_call_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'startScheduledGroupCall';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ToggleGroupCallEnabledStartNotification extends TdFunction {
  /// Toggles whether the current user will receive a notification when the group call will start; scheduled group calls only
  ToggleGroupCallEnabledStartNotification(
      {required this.groupCallId,
      required this.enabledStartNotification,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// [enabledStartNotification] New value of the enabled_start_notification setting
  bool enabledStartNotification;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ToggleGroupCallEnabledStartNotification.fromJson(Map<String, dynamic> json) {
    return ToggleGroupCallEnabledStartNotification(
      groupCallId: json['group_call_id'] ?? 0,
      enabledStartNotification: json['enabled_start_notification'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "enabled_start_notification": this.enabledStartNotification,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'toggleGroupCallEnabledStartNotification';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class JoinGroupCall extends TdFunction {
  /// Joins an active group call. Returns join response payload for tgcalls
  JoinGroupCall(
      {required this.groupCallId,
      required this.participantId,
      required this.audioSourceId,
      required this.payload,
      required this.isMuted,
      required this.isMyVideoEnabled,
      this.inviteHash,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// [participantId] Identifier of a group call participant, which will be used to join the call; pass null to join as self; video chats only
  MessageSender participantId;

  /// [audioSourceId] Caller audio channel synchronization source identifier; received from tgcalls
  int audioSourceId;

  /// [payload] Group call join payload; received from tgcalls
  String payload;

  /// [isMuted] True, if the user's microphone is muted
  bool isMuted;

  /// [isMyVideoEnabled] True, if the user's video is enabled
  bool isMyVideoEnabled;

  /// [inviteHash] If non-empty, invite hash to be used to join the group call without being muted by administrators
  String? inviteHash;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory JoinGroupCall.fromJson(Map<String, dynamic> json) {
    return JoinGroupCall(
      groupCallId: json['group_call_id'] ?? 0,
      participantId: MessageSender.fromJson(json['participant_id'] ?? <String, dynamic>{}),
      audioSourceId: json['audio_source_id'] ?? 0,
      payload: json['payload'] ?? "",
      isMuted: json['is_muted'] ?? false,
      isMyVideoEnabled: json['is_my_video_enabled'] ?? false,
      inviteHash: json['invite_hash'],
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "participant_id": this.participantId.toJson(),
      "audio_source_id": this.audioSourceId,
      "payload": this.payload,
      "is_muted": this.isMuted,
      "is_my_video_enabled": this.isMyVideoEnabled,
      "invite_hash": this.inviteHash == null ? null : this.inviteHash!,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'joinGroupCall';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class StartGroupCallScreenSharing extends TdFunction {
  /// Starts screen sharing in a joined group call. Returns join response payload for tgcalls
  StartGroupCallScreenSharing(
      {required this.groupCallId,
      required this.audioSourceId,
      required this.payload,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// [audioSourceId] Screen sharing audio channel synchronization source identifier; received from tgcalls
  int audioSourceId;

  /// [payload] Group call join payload; received from tgcalls
  String payload;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory StartGroupCallScreenSharing.fromJson(Map<String, dynamic> json) {
    return StartGroupCallScreenSharing(
      groupCallId: json['group_call_id'] ?? 0,
      audioSourceId: json['audio_source_id'] ?? 0,
      payload: json['payload'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "audio_source_id": this.audioSourceId,
      "payload": this.payload,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'startGroupCallScreenSharing';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ToggleGroupCallScreenSharingIsPaused extends TdFunction {
  /// Pauses or unpauses screen sharing in a joined group call
  ToggleGroupCallScreenSharingIsPaused(
      {required this.groupCallId,
      required this.isPaused,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// [isPaused] True if screen sharing is paused
  bool isPaused;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ToggleGroupCallScreenSharingIsPaused.fromJson(Map<String, dynamic> json) {
    return ToggleGroupCallScreenSharingIsPaused(
      groupCallId: json['group_call_id'] ?? 0,
      isPaused: json['is_paused'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "is_paused": this.isPaused,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'toggleGroupCallScreenSharingIsPaused';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class EndGroupCallScreenSharing extends TdFunction {
  /// Ends screen sharing in a joined group call
  EndGroupCallScreenSharing(
      {required this.groupCallId,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory EndGroupCallScreenSharing.fromJson(Map<String, dynamic> json) {
    return EndGroupCallScreenSharing(
      groupCallId: json['group_call_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'endGroupCallScreenSharing';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetGroupCallTitle extends TdFunction {
  /// Sets group call title. Requires groupCall.can_be_managed group call flag
  SetGroupCallTitle(
      {required this.groupCallId,
      required this.title,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// [title] New group call title; 1-64 characters
  String title;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetGroupCallTitle.fromJson(Map<String, dynamic> json) {
    return SetGroupCallTitle(
      groupCallId: json['group_call_id'] ?? 0,
      title: json['title'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "title": this.title,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setGroupCallTitle';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ToggleGroupCallMuteNewParticipants extends TdFunction {
  /// Toggles whether new participants of a group call can be unmuted only by administrators of the group call. Requires groupCall.can_toggle_mute_new_participants group call flag
  ToggleGroupCallMuteNewParticipants(
      {required this.groupCallId,
      required this.muteNewParticipants,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// [muteNewParticipants] New value of the mute_new_participants setting
  bool muteNewParticipants;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ToggleGroupCallMuteNewParticipants.fromJson(Map<String, dynamic> json) {
    return ToggleGroupCallMuteNewParticipants(
      groupCallId: json['group_call_id'] ?? 0,
      muteNewParticipants: json['mute_new_participants'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "mute_new_participants": this.muteNewParticipants,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'toggleGroupCallMuteNewParticipants';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class RevokeGroupCallInviteLink extends TdFunction {
  /// Revokes invite link for a group call. Requires groupCall.can_be_managed group call flag
  RevokeGroupCallInviteLink(
      {required this.groupCallId,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory RevokeGroupCallInviteLink.fromJson(Map<String, dynamic> json) {
    return RevokeGroupCallInviteLink(
      groupCallId: json['group_call_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'revokeGroupCallInviteLink';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class InviteGroupCallParticipants extends TdFunction {
  /// Invites users to an active group call. Sends a service message of type messageInviteToGroupCall for video chats
  InviteGroupCallParticipants(
      {required this.groupCallId,
      required this.userIds,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// [userIds] User identifiers. At most 10 users can be invited simultaneously
  List<int> userIds;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory InviteGroupCallParticipants.fromJson(Map<String, dynamic> json) {
    return InviteGroupCallParticipants(
      groupCallId: json['group_call_id'] ?? 0,
      userIds: List<int>.from((json['user_ids'] ?? []).map((item) => item ?? 0).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "user_ids": this.userIds.map((i) => i).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'inviteGroupCallParticipants';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetGroupCallInviteLink extends TdFunction {
  /// Returns invite link to a video chat in a public chat
  GetGroupCallInviteLink(
      {required this.groupCallId,
      required this.canSelfUnmute,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// [canSelfUnmute] Pass true if the invite link needs to contain an invite hash, passing which to joinGroupCall would allow the invited user to unmute themselves. Requires groupCall.can_be_managed group call flag
  bool canSelfUnmute;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetGroupCallInviteLink.fromJson(Map<String, dynamic> json) {
    return GetGroupCallInviteLink(
      groupCallId: json['group_call_id'] ?? 0,
      canSelfUnmute: json['can_self_unmute'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "can_self_unmute": this.canSelfUnmute,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getGroupCallInviteLink';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class StartGroupCallRecording extends TdFunction {
  /// Starts recording of an active group call. Requires groupCall.can_be_managed group call flag
  StartGroupCallRecording(
      {required this.groupCallId,
      required this.title,
      required this.recordVideo,
      required this.usePortraitOrientation,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// [title] Group call recording title; 0-64 characters
  String title;

  /// [recordVideo] Pass true to record a video file instead of an audio file
  bool recordVideo;

  /// [usePortraitOrientation] Pass true to use portrait orientation for video instead of landscape one
  bool usePortraitOrientation;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory StartGroupCallRecording.fromJson(Map<String, dynamic> json) {
    return StartGroupCallRecording(
      groupCallId: json['group_call_id'] ?? 0,
      title: json['title'] ?? "",
      recordVideo: json['record_video'] ?? false,
      usePortraitOrientation: json['use_portrait_orientation'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "title": this.title,
      "record_video": this.recordVideo,
      "use_portrait_orientation": this.usePortraitOrientation,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'startGroupCallRecording';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class EndGroupCallRecording extends TdFunction {
  /// Ends recording of an active group call. Requires groupCall.can_be_managed group call flag
  EndGroupCallRecording(
      {required this.groupCallId,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory EndGroupCallRecording.fromJson(Map<String, dynamic> json) {
    return EndGroupCallRecording(
      groupCallId: json['group_call_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'endGroupCallRecording';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ToggleGroupCallIsMyVideoPaused extends TdFunction {
  /// Toggles whether current user's video is paused
  ToggleGroupCallIsMyVideoPaused(
      {required this.groupCallId,
      required this.isMyVideoPaused,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// [isMyVideoPaused] Pass true if the current user's video is paused
  bool isMyVideoPaused;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ToggleGroupCallIsMyVideoPaused.fromJson(Map<String, dynamic> json) {
    return ToggleGroupCallIsMyVideoPaused(
      groupCallId: json['group_call_id'] ?? 0,
      isMyVideoPaused: json['is_my_video_paused'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "is_my_video_paused": this.isMyVideoPaused,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'toggleGroupCallIsMyVideoPaused';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ToggleGroupCallIsMyVideoEnabled extends TdFunction {
  /// Toggles whether current user's video is enabled
  ToggleGroupCallIsMyVideoEnabled(
      {required this.groupCallId,
      required this.isMyVideoEnabled,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// [isMyVideoEnabled] Pass true if the current user's video is enabled
  bool isMyVideoEnabled;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ToggleGroupCallIsMyVideoEnabled.fromJson(Map<String, dynamic> json) {
    return ToggleGroupCallIsMyVideoEnabled(
      groupCallId: json['group_call_id'] ?? 0,
      isMyVideoEnabled: json['is_my_video_enabled'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "is_my_video_enabled": this.isMyVideoEnabled,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'toggleGroupCallIsMyVideoEnabled';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetGroupCallParticipantIsSpeaking extends TdFunction {
  /// Informs TDLib that speaking state of a participant of an active group has changed
  SetGroupCallParticipantIsSpeaking(
      {required this.groupCallId,
      this.audioSource,
      required this.isSpeaking,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// [audioSource] Group call participant's synchronization audio source identifier, or 0 for the current user
  int? audioSource;

  /// [isSpeaking] True, if the user is speaking
  bool isSpeaking;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetGroupCallParticipantIsSpeaking.fromJson(Map<String, dynamic> json) {
    return SetGroupCallParticipantIsSpeaking(
      groupCallId: json['group_call_id'] ?? 0,
      audioSource: json['audio_source'],
      isSpeaking: json['is_speaking'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "audio_source": this.audioSource == null ? null : this.audioSource!,
      "is_speaking": this.isSpeaking,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setGroupCallParticipantIsSpeaking';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ToggleGroupCallParticipantIsMuted extends TdFunction {
  /// Toggles whether a participant of an active group call is muted, unmuted, or allowed to unmute themselves
  ToggleGroupCallParticipantIsMuted(
      {required this.groupCallId,
      required this.participantId,
      required this.isMuted,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// [participantId] Participant identifier
  MessageSender participantId;

  /// [isMuted] Pass true if the user must be muted and false otherwise
  bool isMuted;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ToggleGroupCallParticipantIsMuted.fromJson(Map<String, dynamic> json) {
    return ToggleGroupCallParticipantIsMuted(
      groupCallId: json['group_call_id'] ?? 0,
      participantId: MessageSender.fromJson(json['participant_id'] ?? <String, dynamic>{}),
      isMuted: json['is_muted'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "participant_id": this.participantId.toJson(),
      "is_muted": this.isMuted,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'toggleGroupCallParticipantIsMuted';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetGroupCallParticipantVolumeLevel extends TdFunction {
  /// Changes volume level of a participant of an active group call. If the current user can manage the group call, then the participant's volume level will be changed for all users with the default volume level
  SetGroupCallParticipantVolumeLevel(
      {required this.groupCallId,
      required this.participantId,
      required this.volumeLevel,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// [participantId] Participant identifier
  MessageSender participantId;

  /// [volumeLevel] New participant's volume level; 1-20000 in hundreds of percents
  int volumeLevel;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetGroupCallParticipantVolumeLevel.fromJson(Map<String, dynamic> json) {
    return SetGroupCallParticipantVolumeLevel(
      groupCallId: json['group_call_id'] ?? 0,
      participantId: MessageSender.fromJson(json['participant_id'] ?? <String, dynamic>{}),
      volumeLevel: json['volume_level'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "participant_id": this.participantId.toJson(),
      "volume_level": this.volumeLevel,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setGroupCallParticipantVolumeLevel';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ToggleGroupCallParticipantIsHandRaised extends TdFunction {
  /// Toggles whether a group call participant hand is rased
  ToggleGroupCallParticipantIsHandRaised(
      {required this.groupCallId,
      required this.participantId,
      required this.isHandRaised,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// [participantId] Participant identifier
  MessageSender participantId;

  /// [isHandRaised] Pass true if the user's hand needs to be raised. Only self hand can be raised. Requires groupCall.can_be_managed group call flag to lower other's hand
  bool isHandRaised;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ToggleGroupCallParticipantIsHandRaised.fromJson(Map<String, dynamic> json) {
    return ToggleGroupCallParticipantIsHandRaised(
      groupCallId: json['group_call_id'] ?? 0,
      participantId: MessageSender.fromJson(json['participant_id'] ?? <String, dynamic>{}),
      isHandRaised: json['is_hand_raised'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "participant_id": this.participantId.toJson(),
      "is_hand_raised": this.isHandRaised,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'toggleGroupCallParticipantIsHandRaised';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class LoadGroupCallParticipants extends TdFunction {
  /// Loads more participants of a group call. The loaded participants will be received through updates. Use the field groupCall.loaded_all_participants to check whether all participants has already been loaded
  LoadGroupCallParticipants(
      {required this.groupCallId,
      required this.limit,
      this.extra});

  /// [groupCallId] Group call identifier. The group call must be previously received through getGroupCall and must be joined or being joined
  int groupCallId;

  /// [limit] The maximum number of participants to load; up to 100
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory LoadGroupCallParticipants.fromJson(Map<String, dynamic> json) {
    return LoadGroupCallParticipants(
      groupCallId: json['group_call_id'] ?? 0,
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'loadGroupCallParticipants';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class LeaveGroupCall extends TdFunction {
  /// Leaves a group call
  LeaveGroupCall(
      {required this.groupCallId,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory LeaveGroupCall.fromJson(Map<String, dynamic> json) {
    return LeaveGroupCall(
      groupCallId: json['group_call_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'leaveGroupCall';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DiscardGroupCall extends TdFunction {
  /// Discards a group call. Requires groupCall.can_be_managed
  DiscardGroupCall(
      {required this.groupCallId,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DiscardGroupCall.fromJson(Map<String, dynamic> json) {
    return DiscardGroupCall(
      groupCallId: json['group_call_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'discardGroupCall';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetGroupCallStreamSegment extends TdFunction {
  /// Returns a file with a segment of a group call stream in a modified OGG format for audio or MPEG-4 format for video
  GetGroupCallStreamSegment(
      {required this.groupCallId,
      required this.timeOffset,
      required this.scale,
      required this.channelId,
      required this.videoQuality,
      this.extra});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// [timeOffset] Point in time when the stream segment begins; Unix timestamp in milliseconds
  int timeOffset;

  /// [scale] Segment duration scale; 0-1. Segment's duration is 1000/(2**scale) milliseconds
  int scale;

  /// [channelId] Identifier of an audio/video channel to get as received from tgcalls
  int channelId;

  /// [videoQuality] Video quality as received from tgcalls; pass null to get the worst available quality
  GroupCallVideoQuality videoQuality;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetGroupCallStreamSegment.fromJson(Map<String, dynamic> json) {
    return GetGroupCallStreamSegment(
      groupCallId: json['group_call_id'] ?? 0,
      timeOffset: json['time_offset'] ?? 0,
      scale: json['scale'] ?? 0,
      channelId: json['channel_id'] ?? 0,
      videoQuality: GroupCallVideoQuality.fromJson(json['video_quality'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "time_offset": this.timeOffset,
      "scale": this.scale,
      "channel_id": this.channelId,
      "video_quality": this.videoQuality.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getGroupCallStreamSegment';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ToggleMessageSenderIsBlocked extends TdFunction {
  /// Changes the block state of a message sender. Currently, only users and supergroup chats can be blocked
  ToggleMessageSenderIsBlocked(
      {required this.sender,
      required this.isBlocked,
      this.extra});

  /// [sender] Message Sender
  MessageSender sender;

  /// [isBlocked] New value of is_blocked
  bool isBlocked;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ToggleMessageSenderIsBlocked.fromJson(Map<String, dynamic> json) {
    return ToggleMessageSenderIsBlocked(
      sender: MessageSender.fromJson(json['sender'] ?? <String, dynamic>{}),
      isBlocked: json['is_blocked'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "sender": this.sender.toJson(),
      "is_blocked": this.isBlocked,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'toggleMessageSenderIsBlocked';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class BlockMessageSenderFromReplies extends TdFunction {
  /// Blocks an original sender of a message in the Replies chat
  BlockMessageSenderFromReplies(
      {required this.messageId,
      required this.deleteMessage,
      required this.deleteAllMessages,
      required this.reportSpam,
      this.extra});

  /// [messageId] The identifier of an incoming message in the Replies chat
  int messageId;

  /// [deleteMessage] Pass true if the message must be deleted
  bool deleteMessage;

  /// [deleteAllMessages] Pass true if all messages from the same sender must be deleted
  bool deleteAllMessages;

  /// [reportSpam] Pass true if the sender must be reported to the Telegram moderators
  bool reportSpam;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory BlockMessageSenderFromReplies.fromJson(Map<String, dynamic> json) {
    return BlockMessageSenderFromReplies(
      messageId: json['message_id'] ?? 0,
      deleteMessage: json['delete_message'] ?? false,
      deleteAllMessages: json['delete_all_messages'] ?? false,
      reportSpam: json['report_spam'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "message_id": this.messageId,
      "delete_message": this.deleteMessage,
      "delete_all_messages": this.deleteAllMessages,
      "report_spam": this.reportSpam,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'blockMessageSenderFromReplies';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetBlockedMessageSenders extends TdFunction {
  /// Returns users and chats that were blocked by the current user
  GetBlockedMessageSenders(
      {required this.offset,
      required this.limit,
      this.extra});

  /// [offset] Number of users and chats to skip in the result; must be non-negative
  int offset;

  /// [limit] The maximum number of users and chats to return; up to 100
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetBlockedMessageSenders.fromJson(Map<String, dynamic> json) {
    return GetBlockedMessageSenders(
      offset: json['offset'] ?? 0,
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "offset": this.offset,
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getBlockedMessageSenders';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class AddContact extends TdFunction {
  /// Adds a user to the contact list or edits an existing contact by their user identifier
  AddContact(
      {required this.contact,
      required this.sharePhoneNumber,
      this.extra});

  /// [contact] The contact to add or edit; phone number can be empty and needs to be specified only if known, vCard is ignored
  Contact contact;

  /// [sharePhoneNumber] True, if the new contact needs to be allowed to see current user's phone number. A corresponding rule to userPrivacySettingShowPhoneNumber will be added if needed. Use the field userFullInfo.need_phone_number_privacy_exception to check whether the current user needs to be asked to share their phone number
  bool sharePhoneNumber;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory AddContact.fromJson(Map<String, dynamic> json) {
    return AddContact(
      contact: Contact.fromJson(json['contact'] ?? <String, dynamic>{}),
      sharePhoneNumber: json['share_phone_number'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "contact": this.contact.toJson(),
      "share_phone_number": this.sharePhoneNumber,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'addContact';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ImportContacts extends TdFunction {
  /// Adds new contacts or edits existing contacts by their phone numbers; contacts' user identifiers are ignored
  ImportContacts(
      {required this.contacts,
      this.extra});

  /// [contacts] The list of contacts to import or edit; contacts' vCard are ignored and are not imported
  List<Contact> contacts;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ImportContacts.fromJson(Map<String, dynamic> json) {
    return ImportContacts(
      contacts: List<Contact>.from((json['contacts'] ?? []).map((item) => Contact.fromJson(item ?? <String, dynamic>{})).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "contacts": this.contacts.map((i) => i.toJson()).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'importContacts';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetContacts extends TdFunction {
  /// Returns all user contacts
  GetContacts(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetContacts.fromJson(Map<String, dynamic> json) {
    return GetContacts(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getContacts';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SearchContacts extends TdFunction {
  /// Searches for the specified query in the first names, last names and usernames of the known user contacts
  SearchContacts(
      {required this.query,
      required this.limit,
      this.extra});

  /// [query] Query to search for; may be empty to return all contacts
  String query;

  /// [limit] The maximum number of users to be returned
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SearchContacts.fromJson(Map<String, dynamic> json) {
    return SearchContacts(
      query: json['query'] ?? "",
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "query": this.query,
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'searchContacts';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class RemoveContacts extends TdFunction {
  /// Removes users from the contact list
  RemoveContacts(
      {required this.userIds,
      this.extra});

  /// [userIds] Identifiers of users to be deleted
  List<int> userIds;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory RemoveContacts.fromJson(Map<String, dynamic> json) {
    return RemoveContacts(
      userIds: List<int>.from((json['user_ids'] ?? []).map((item) => item ?? 0).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "user_ids": this.userIds.map((i) => i).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'removeContacts';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetImportedContactCount extends TdFunction {
  /// Returns the total number of imported contacts
  GetImportedContactCount(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetImportedContactCount.fromJson(Map<String, dynamic> json) {
    return GetImportedContactCount(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getImportedContactCount';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ChangeImportedContacts extends TdFunction {
  /// Changes imported contacts using the list of contacts saved on the device. Imports newly added contacts and, if at least the file database is enabled, deletes recently deleted contacts.. Query result depends on the result of the previous query, so only one query is possible at the same time
  ChangeImportedContacts(
      {required this.contacts,
      this.extra});

  /// [contacts] The new list of contacts, contact's vCard are ignored and are not imported
  List<Contact> contacts;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ChangeImportedContacts.fromJson(Map<String, dynamic> json) {
    return ChangeImportedContacts(
      contacts: List<Contact>.from((json['contacts'] ?? []).map((item) => Contact.fromJson(item ?? <String, dynamic>{})).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "contacts": this.contacts.map((i) => i.toJson()).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'changeImportedContacts';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ClearImportedContacts extends TdFunction {
  /// Clears all imported contacts, contact list remains unchanged
  ClearImportedContacts(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ClearImportedContacts.fromJson(Map<String, dynamic> json) {
    return ClearImportedContacts(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'clearImportedContacts';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SharePhoneNumber extends TdFunction {
  /// Shares the phone number of the current user with a mutual contact. Supposed to be called when the user clicks on chatActionBarSharePhoneNumber
  SharePhoneNumber(
      {required this.userId,
      this.extra});

  /// [userId] Identifier of the user with whom to share the phone number. The user must be a mutual contact
  int userId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SharePhoneNumber.fromJson(Map<String, dynamic> json) {
    return SharePhoneNumber(
      userId: json['user_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "user_id": this.userId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'sharePhoneNumber';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetUserProfilePhotos extends TdFunction {
  /// Returns the profile photos of a user. The result of this query may be outdated: some photos might have been deleted already
  GetUserProfilePhotos(
      {required this.userId,
      required this.offset,
      required this.limit,
      this.extra});

  /// [userId] User identifier
  int userId;

  /// [offset] The number of photos to skip; must be non-negative
  int offset;

  /// [limit] The maximum number of photos to be returned; up to 100
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetUserProfilePhotos.fromJson(Map<String, dynamic> json) {
    return GetUserProfilePhotos(
      userId: json['user_id'] ?? 0,
      offset: json['offset'] ?? 0,
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "user_id": this.userId,
      "offset": this.offset,
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getUserProfilePhotos';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetStickers extends TdFunction {
  /// Returns stickers from the installed sticker sets that correspond to a given emoji. If the emoji is non-empty, favorite and recently used stickers may also be returned
  GetStickers(
      {required this.emoji,
      required this.limit,
      this.extra});

  /// [emoji] String representation of emoji. If empty, returns all known installed stickers
  String emoji;

  /// [limit] The maximum number of stickers to be returned
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetStickers.fromJson(Map<String, dynamic> json) {
    return GetStickers(
      emoji: json['emoji'] ?? "",
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "emoji": this.emoji,
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getStickers';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SearchStickers extends TdFunction {
  /// Searches for stickers from public sticker sets that correspond to a given emoji
  SearchStickers(
      {required this.emoji,
      required this.limit,
      this.extra});

  /// [emoji] String representation of emoji; must be non-empty
  String emoji;

  /// [limit] The maximum number of stickers to be returned
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SearchStickers.fromJson(Map<String, dynamic> json) {
    return SearchStickers(
      emoji: json['emoji'] ?? "",
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "emoji": this.emoji,
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'searchStickers';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetInstalledStickerSets extends TdFunction {
  /// Returns a list of installed sticker sets
  GetInstalledStickerSets(
      {required this.isMasks,
      this.extra});

  /// [isMasks] Pass true to return mask sticker sets; pass false to return ordinary sticker sets
  bool isMasks;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetInstalledStickerSets.fromJson(Map<String, dynamic> json) {
    return GetInstalledStickerSets(
      isMasks: json['is_masks'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "is_masks": this.isMasks,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getInstalledStickerSets';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetArchivedStickerSets extends TdFunction {
  /// Returns a list of archived sticker sets
  GetArchivedStickerSets(
      {required this.isMasks,
      required this.offsetStickerSetId,
      required this.limit,
      this.extra});

  /// [isMasks] Pass true to return mask stickers sets; pass false to return ordinary sticker sets
  bool isMasks;

  /// [offsetStickerSetId] Identifier of the sticker set from which to return the result
  int offsetStickerSetId;

  /// [limit] The maximum number of sticker sets to return; up to 100
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetArchivedStickerSets.fromJson(Map<String, dynamic> json) {
    return GetArchivedStickerSets(
      isMasks: json['is_masks'] ?? false,
      offsetStickerSetId: int.tryParse(json['offset_sticker_set_id'].toString()) ?? 0,
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "is_masks": this.isMasks,
      "offset_sticker_set_id": this.offsetStickerSetId,
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getArchivedStickerSets';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetTrendingStickerSets extends TdFunction {
  /// Returns a list of trending sticker sets. For optimal performance, the number of returned sticker sets is chosen by TDLib
  GetTrendingStickerSets(
      {required this.offset,
      required this.limit,
      this.extra});

  /// [offset] The offset from which to return the sticker sets; must be non-negative
  int offset;

  /// [limit] The maximum number of sticker sets to be returned; up to 100. For optimal performance, the number of returned sticker sets is chosen by TDLib and can be smaller than the specified limit, even if the end of the list has not been reached
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetTrendingStickerSets.fromJson(Map<String, dynamic> json) {
    return GetTrendingStickerSets(
      offset: json['offset'] ?? 0,
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "offset": this.offset,
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getTrendingStickerSets';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetAttachedStickerSets extends TdFunction {
  /// Returns a list of sticker sets attached to a file. Currently only photos and videos can have attached sticker sets
  GetAttachedStickerSets(
      {required this.fileId,
      this.extra});

  /// [fileId] File identifier
  int fileId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetAttachedStickerSets.fromJson(Map<String, dynamic> json) {
    return GetAttachedStickerSets(
      fileId: json['file_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "file_id": this.fileId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getAttachedStickerSets';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetStickerSet extends TdFunction {
  /// Returns information about a sticker set by its identifier
  GetStickerSet(
      {required this.setId,
      this.extra});

  /// [setId] Identifier of the sticker set
  int setId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetStickerSet.fromJson(Map<String, dynamic> json) {
    return GetStickerSet(
      setId: int.tryParse(json['set_id'].toString()) ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "set_id": this.setId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getStickerSet';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SearchStickerSet extends TdFunction {
  /// Searches for a sticker set by its name
  SearchStickerSet(
      {required this.name,
      this.extra});

  /// [name] Name of the sticker set
  String name;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SearchStickerSet.fromJson(Map<String, dynamic> json) {
    return SearchStickerSet(
      name: json['name'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "name": this.name,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'searchStickerSet';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SearchInstalledStickerSets extends TdFunction {
  /// Searches for installed sticker sets by looking for specified query in their title and name
  SearchInstalledStickerSets(
      {required this.isMasks,
      required this.query,
      required this.limit,
      this.extra});

  /// [isMasks] Pass true to return mask sticker sets; pass false to return ordinary sticker sets
  bool isMasks;

  /// [query] Query to search for
  String query;

  /// [limit] The maximum number of sticker sets to return
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SearchInstalledStickerSets.fromJson(Map<String, dynamic> json) {
    return SearchInstalledStickerSets(
      isMasks: json['is_masks'] ?? false,
      query: json['query'] ?? "",
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "is_masks": this.isMasks,
      "query": this.query,
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'searchInstalledStickerSets';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SearchStickerSets extends TdFunction {
  /// Searches for ordinary sticker sets by looking for specified query in their title and name. Excludes installed sticker sets from the results
  SearchStickerSets(
      {required this.query,
      this.extra});

  /// [query] Query to search for
  String query;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SearchStickerSets.fromJson(Map<String, dynamic> json) {
    return SearchStickerSets(
      query: json['query'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "query": this.query,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'searchStickerSets';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ChangeStickerSet extends TdFunction {
  /// Installs/uninstalls or activates/archives a sticker set
  ChangeStickerSet(
      {required this.setId,
      required this.isInstalled,
      required this.isArchived,
      this.extra});

  /// [setId] Identifier of the sticker set
  int setId;

  /// [isInstalled] The new value of is_installed
  bool isInstalled;

  /// [isArchived] The new value of is_archived. A sticker set can't be installed and archived simultaneously
  bool isArchived;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ChangeStickerSet.fromJson(Map<String, dynamic> json) {
    return ChangeStickerSet(
      setId: int.tryParse(json['set_id'].toString()) ?? 0,
      isInstalled: json['is_installed'] ?? false,
      isArchived: json['is_archived'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "set_id": this.setId,
      "is_installed": this.isInstalled,
      "is_archived": this.isArchived,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'changeStickerSet';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ViewTrendingStickerSets extends TdFunction {
  /// Informs the server that some trending sticker sets have been viewed by the user
  ViewTrendingStickerSets(
      {required this.stickerSetIds,
      this.extra});

  /// [stickerSetIds] Identifiers of viewed trending sticker sets
  List<int> stickerSetIds;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ViewTrendingStickerSets.fromJson(Map<String, dynamic> json) {
    return ViewTrendingStickerSets(
      stickerSetIds: List<int>.from((json['sticker_set_ids'] ?? []).map((item) => item ?? 0).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "sticker_set_ids": this.stickerSetIds.map((i) => i).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'viewTrendingStickerSets';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ReorderInstalledStickerSets extends TdFunction {
  /// Changes the order of installed sticker sets
  ReorderInstalledStickerSets(
      {required this.isMasks,
      required this.stickerSetIds,
      this.extra});

  /// [isMasks] Pass true to change the order of mask sticker sets; pass false to change the order of ordinary sticker sets
  bool isMasks;

  /// [stickerSetIds] Identifiers of installed sticker sets in the new correct order
  List<int> stickerSetIds;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ReorderInstalledStickerSets.fromJson(Map<String, dynamic> json) {
    return ReorderInstalledStickerSets(
      isMasks: json['is_masks'] ?? false,
      stickerSetIds: List<int>.from((json['sticker_set_ids'] ?? []).map((item) => item ?? 0).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "is_masks": this.isMasks,
      "sticker_set_ids": this.stickerSetIds.map((i) => i).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'reorderInstalledStickerSets';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetRecentStickers extends TdFunction {
  /// Returns a list of recently used stickers
  GetRecentStickers(
      {required this.isAttached,
      this.extra});

  /// [isAttached] Pass true to return stickers and masks that were recently attached to photos or video files; pass false to return recently sent stickers
  bool isAttached;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetRecentStickers.fromJson(Map<String, dynamic> json) {
    return GetRecentStickers(
      isAttached: json['is_attached'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "is_attached": this.isAttached,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getRecentStickers';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class AddRecentSticker extends TdFunction {
  /// Manually adds a new sticker to the list of recently used stickers. The new sticker is added to the top of the list. If the sticker was already in the list, it is removed from the list first. Only stickers belonging to a sticker set can be added to this list
  AddRecentSticker(
      {required this.isAttached,
      required this.sticker,
      this.extra});

  /// [isAttached] Pass true to add the sticker to the list of stickers recently attached to photo or video files; pass false to add the sticker to the list of recently sent stickers
  bool isAttached;

  /// [sticker] Sticker file to add
  InputFile sticker;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory AddRecentSticker.fromJson(Map<String, dynamic> json) {
    return AddRecentSticker(
      isAttached: json['is_attached'] ?? false,
      sticker: InputFile.fromJson(json['sticker'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "is_attached": this.isAttached,
      "sticker": this.sticker.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'addRecentSticker';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class RemoveRecentSticker extends TdFunction {
  /// Removes a sticker from the list of recently used stickers
  RemoveRecentSticker(
      {required this.isAttached,
      required this.sticker,
      this.extra});

  /// [isAttached] Pass true to remove the sticker from the list of stickers recently attached to photo or video files; pass false to remove the sticker from the list of recently sent stickers
  bool isAttached;

  /// [sticker] Sticker file to delete
  InputFile sticker;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory RemoveRecentSticker.fromJson(Map<String, dynamic> json) {
    return RemoveRecentSticker(
      isAttached: json['is_attached'] ?? false,
      sticker: InputFile.fromJson(json['sticker'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "is_attached": this.isAttached,
      "sticker": this.sticker.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'removeRecentSticker';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ClearRecentStickers extends TdFunction {
  /// Clears the list of recently used stickers
  ClearRecentStickers(
      {required this.isAttached,
      this.extra});

  /// [isAttached] Pass true to clear the list of stickers recently attached to photo or video files; pass false to clear the list of recently sent stickers
  bool isAttached;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ClearRecentStickers.fromJson(Map<String, dynamic> json) {
    return ClearRecentStickers(
      isAttached: json['is_attached'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "is_attached": this.isAttached,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'clearRecentStickers';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetFavoriteStickers extends TdFunction {
  /// Returns favorite stickers
  GetFavoriteStickers(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetFavoriteStickers.fromJson(Map<String, dynamic> json) {
    return GetFavoriteStickers(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getFavoriteStickers';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class AddFavoriteSticker extends TdFunction {
  /// Adds a new sticker to the list of favorite stickers. The new sticker is added to the top of the list. If the sticker was already in the list, it is removed from the list first. Only stickers belonging to a sticker set can be added to this list
  AddFavoriteSticker(
      {required this.sticker,
      this.extra});

  /// [sticker] Sticker file to add
  InputFile sticker;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory AddFavoriteSticker.fromJson(Map<String, dynamic> json) {
    return AddFavoriteSticker(
      sticker: InputFile.fromJson(json['sticker'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "sticker": this.sticker.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'addFavoriteSticker';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class RemoveFavoriteSticker extends TdFunction {
  /// Removes a sticker from the list of favorite stickers
  RemoveFavoriteSticker(
      {required this.sticker,
      this.extra});

  /// [sticker] Sticker file to delete from the list
  InputFile sticker;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory RemoveFavoriteSticker.fromJson(Map<String, dynamic> json) {
    return RemoveFavoriteSticker(
      sticker: InputFile.fromJson(json['sticker'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "sticker": this.sticker.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'removeFavoriteSticker';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetStickerEmojis extends TdFunction {
  /// Returns emoji corresponding to a sticker. The list is only for informational purposes, because a sticker is always sent with a fixed emoji from the corresponding Sticker object
  GetStickerEmojis(
      {required this.sticker,
      this.extra});

  /// [sticker] Sticker file identifier
  InputFile sticker;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetStickerEmojis.fromJson(Map<String, dynamic> json) {
    return GetStickerEmojis(
      sticker: InputFile.fromJson(json['sticker'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "sticker": this.sticker.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getStickerEmojis';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SearchEmojis extends TdFunction {
  /// Searches for emojis by keywords. Supported only if the file database is enabled
  SearchEmojis(
      {required this.text,
      required this.exactMatch,
      required this.inputLanguageCodes,
      this.extra});

  /// [text] Text to search for
  String text;

  /// [exactMatch] True, if only emojis, which exactly match text needs to be returned
  bool exactMatch;

  /// [inputLanguageCodes] List of possible IETF language tags of the user's input language; may be empty if unknown
  List<String> inputLanguageCodes;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SearchEmojis.fromJson(Map<String, dynamic> json) {
    return SearchEmojis(
      text: json['text'] ?? "",
      exactMatch: json['exact_match'] ?? false,
      inputLanguageCodes: List<String>.from((json['input_language_codes'] ?? []).map((item) => item ?? "").toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "text": this.text,
      "exact_match": this.exactMatch,
      "input_language_codes": this.inputLanguageCodes.map((i) => i).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'searchEmojis';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetAnimatedEmoji extends TdFunction {
  /// Returns an animated emoji corresponding to a given emoji. Returns a 404 error if the emoji has no animated emoji
  GetAnimatedEmoji(
      {required this.emoji,
      this.extra});

  /// [emoji] The emoji
  String emoji;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetAnimatedEmoji.fromJson(Map<String, dynamic> json) {
    return GetAnimatedEmoji(
      emoji: json['emoji'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "emoji": this.emoji,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getAnimatedEmoji';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetEmojiSuggestionsUrl extends TdFunction {
  /// Returns an HTTP URL which can be used to automatically log in to the translation platform and suggest new emoji replacements. The URL will be valid for 30 seconds after generation
  GetEmojiSuggestionsUrl(
      {required this.languageCode,
      this.extra});

  /// [languageCode] Language code for which the emoji replacements will be suggested
  String languageCode;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetEmojiSuggestionsUrl.fromJson(Map<String, dynamic> json) {
    return GetEmojiSuggestionsUrl(
      languageCode: json['language_code'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "language_code": this.languageCode,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getEmojiSuggestionsUrl';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetSavedAnimations extends TdFunction {
  /// Returns saved animations
  GetSavedAnimations(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetSavedAnimations.fromJson(Map<String, dynamic> json) {
    return GetSavedAnimations(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getSavedAnimations';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class AddSavedAnimation extends TdFunction {
  /// Manually adds a new animation to the list of saved animations. The new animation is added to the beginning of the list. If the animation was already in the list, it is removed first. Only non-secret video animations with MIME type "video/mp4" can be added to the list
  AddSavedAnimation(
      {required this.animation,
      this.extra});

  /// [animation] The animation file to be added. Only animations known to the server (i.e., successfully sent via a message) can be added to the list
  InputFile animation;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory AddSavedAnimation.fromJson(Map<String, dynamic> json) {
    return AddSavedAnimation(
      animation: InputFile.fromJson(json['animation'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "animation": this.animation.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'addSavedAnimation';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class RemoveSavedAnimation extends TdFunction {
  /// Removes an animation from the list of saved animations
  RemoveSavedAnimation(
      {required this.animation,
      this.extra});

  /// [animation] Animation file to be removed
  InputFile animation;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory RemoveSavedAnimation.fromJson(Map<String, dynamic> json) {
    return RemoveSavedAnimation(
      animation: InputFile.fromJson(json['animation'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "animation": this.animation.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'removeSavedAnimation';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetRecentInlineBots extends TdFunction {
  /// Returns up to 20 recently used inline bots in the order of their last usage
  GetRecentInlineBots(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetRecentInlineBots.fromJson(Map<String, dynamic> json) {
    return GetRecentInlineBots(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getRecentInlineBots';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SearchHashtags extends TdFunction {
  /// Searches for recently used hashtags by their prefix
  SearchHashtags(
      {required this.prefix,
      required this.limit,
      this.extra});

  /// [prefix] Hashtag prefix to search for
  String prefix;

  /// [limit] The maximum number of hashtags to be returned
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SearchHashtags.fromJson(Map<String, dynamic> json) {
    return SearchHashtags(
      prefix: json['prefix'] ?? "",
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "prefix": this.prefix,
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'searchHashtags';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class RemoveRecentHashtag extends TdFunction {
  /// Removes a hashtag from the list of recently used hashtags
  RemoveRecentHashtag(
      {required this.hashtag,
      this.extra});

  /// [hashtag] Hashtag to delete
  String hashtag;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory RemoveRecentHashtag.fromJson(Map<String, dynamic> json) {
    return RemoveRecentHashtag(
      hashtag: json['hashtag'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "hashtag": this.hashtag,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'removeRecentHashtag';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetWebPagePreview extends TdFunction {
  /// Returns a web page preview by the text of the message. Do not call this function too often. Returns a 404 error if the web page has no preview
  GetWebPagePreview(
      {required this.text,
      this.extra});

  /// [text] Message text with formatting
  FormattedText text;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetWebPagePreview.fromJson(Map<String, dynamic> json) {
    return GetWebPagePreview(
      text: FormattedText.fromJson(json['text'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "text": this.text.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getWebPagePreview';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetWebPageInstantView extends TdFunction {
  /// Returns an instant view version of a web page if available. Returns a 404 error if the web page has no instant view page
  GetWebPageInstantView(
      {required this.url,
      required this.forceFull,
      this.extra});

  /// [url] The web page URL
  String url;

  /// [forceFull] If true, the full instant view for the web page will be returned
  bool forceFull;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetWebPageInstantView.fromJson(Map<String, dynamic> json) {
    return GetWebPageInstantView(
      url: json['url'] ?? "",
      forceFull: json['force_full'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "url": this.url,
      "force_full": this.forceFull,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getWebPageInstantView';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetProfilePhoto extends TdFunction {
  /// Changes a profile photo for the current user
  SetProfilePhoto(
      {required this.photo,
      this.extra});

  /// [photo] Profile photo to set
  InputChatPhoto photo;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetProfilePhoto.fromJson(Map<String, dynamic> json) {
    return SetProfilePhoto(
      photo: InputChatPhoto.fromJson(json['photo'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "photo": this.photo.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setProfilePhoto';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DeleteProfilePhoto extends TdFunction {
  /// Deletes a profile photo
  DeleteProfilePhoto(
      {required this.profilePhotoId,
      this.extra});

  /// [profilePhotoId] Identifier of the profile photo to delete
  int profilePhotoId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DeleteProfilePhoto.fromJson(Map<String, dynamic> json) {
    return DeleteProfilePhoto(
      profilePhotoId: int.tryParse(json['profile_photo_id'].toString()) ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "profile_photo_id": this.profilePhotoId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'deleteProfilePhoto';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetName extends TdFunction {
  /// Changes the first and last name of the current user
  SetName(
      {required this.firstName,
      required this.lastName,
      this.extra});

  /// [firstName] The new value of the first name for the current user; 1-64 characters
  String firstName;

  /// [lastName] The new value of the optional last name for the current user; 0-64 characters
  String lastName;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetName.fromJson(Map<String, dynamic> json) {
    return SetName(
      firstName: json['first_name'] ?? "",
      lastName: json['last_name'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "first_name": this.firstName,
      "last_name": this.lastName,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setName';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetBio extends TdFunction {
  /// Changes the bio of the current user
  SetBio(
      {required this.bio,
      this.extra});

  /// [bio] The new value of the user bio; 0-70 characters without line feeds
  String bio;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetBio.fromJson(Map<String, dynamic> json) {
    return SetBio(
      bio: json['bio'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "bio": this.bio,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setBio';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetUsername extends TdFunction {
  /// Changes the username of the current user
  SetUsername(
      {required this.username,
      this.extra});

  /// [username] The new value of the username. Use an empty string to remove the username
  String username;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetUsername.fromJson(Map<String, dynamic> json) {
    return SetUsername(
      username: json['username'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "username": this.username,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setUsername';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetLocation extends TdFunction {
  /// Changes the location of the current user. Needs to be called if GetOption("is_location_visible") is true and location changes for more than 1 kilometer
  SetLocation(
      {required this.location,
      this.extra});

  /// [location] The new location of the user
  Location location;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetLocation.fromJson(Map<String, dynamic> json) {
    return SetLocation(
      location: Location.fromJson(json['location'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "location": this.location.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setLocation';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ChangePhoneNumber extends TdFunction {
  /// Changes the phone number of the user and sends an authentication code to the user's new phone number. On success, returns information about the sent code
  ChangePhoneNumber(
      {required this.phoneNumber,
      required this.settings,
      this.extra});

  /// [phoneNumber] The new phone number of the user in international format
  String phoneNumber;

  /// [settings] Settings for the authentication of the user's phone number; pass null to use default settings
  PhoneNumberAuthenticationSettings settings;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ChangePhoneNumber.fromJson(Map<String, dynamic> json) {
    return ChangePhoneNumber(
      phoneNumber: json['phone_number'] ?? "",
      settings: PhoneNumberAuthenticationSettings.fromJson(json['settings'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "phone_number": this.phoneNumber,
      "settings": this.settings.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'changePhoneNumber';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ResendChangePhoneNumberCode extends TdFunction {
  /// Re-sends the authentication code sent to confirm a new phone number for the current user. Works only if the previously received authenticationCodeInfo next_code_type was not null and the server-specified timeout has passed
  ResendChangePhoneNumberCode(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ResendChangePhoneNumberCode.fromJson(Map<String, dynamic> json) {
    return ResendChangePhoneNumberCode(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'resendChangePhoneNumberCode';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CheckChangePhoneNumberCode extends TdFunction {
  /// Checks the authentication code sent to confirm a new phone number of the user
  CheckChangePhoneNumberCode(
      {required this.code,
      this.extra});

  /// [code] Verification code received by SMS, phone call or flash call
  String code;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CheckChangePhoneNumberCode.fromJson(Map<String, dynamic> json) {
    return CheckChangePhoneNumberCode(
      code: json['code'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "code": this.code,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'checkChangePhoneNumberCode';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetCommands extends TdFunction {
  /// Sets the list of commands supported by the bot for the given user scope and language; for bots only
  SetCommands(
      {required this.scope,
      required this.languageCode,
      required this.commands,
      this.extra});

  /// [scope] The scope to which the commands are relevant; pass null to change commands in the default bot command scope
  BotCommandScope scope;

  /// [languageCode] A two-letter ISO 639-1 country code. If empty, the commands will be applied to all users from the given scope, for which language there are no dedicated commands
  String languageCode;

  /// [commands] List of the bot's commands
  List<BotCommand> commands;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetCommands.fromJson(Map<String, dynamic> json) {
    return SetCommands(
      scope: BotCommandScope.fromJson(json['scope'] ?? <String, dynamic>{}),
      languageCode: json['language_code'] ?? "",
      commands: List<BotCommand>.from((json['commands'] ?? []).map((item) => BotCommand.fromJson(item ?? <String, dynamic>{})).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "scope": this.scope.toJson(),
      "language_code": this.languageCode,
      "commands": this.commands.map((i) => i.toJson()).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setCommands';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DeleteCommands extends TdFunction {
  /// Deletes commands supported by the bot for the given user scope and language; for bots only
  DeleteCommands(
      {required this.scope,
      required this.languageCode,
      this.extra});

  /// [scope] The scope to which the commands are relevant; pass null to delete commands in the default bot command scope
  BotCommandScope scope;

  /// [languageCode] A two-letter ISO 639-1 country code or an empty string
  String languageCode;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DeleteCommands.fromJson(Map<String, dynamic> json) {
    return DeleteCommands(
      scope: BotCommandScope.fromJson(json['scope'] ?? <String, dynamic>{}),
      languageCode: json['language_code'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "scope": this.scope.toJson(),
      "language_code": this.languageCode,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'deleteCommands';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetCommands extends TdFunction {
  /// Returns the list of commands supported by the bot for the given user scope and language; for bots only
  GetCommands(
      {required this.scope,
      required this.languageCode,
      this.extra});

  /// [scope] The scope to which the commands are relevant; pass null to get commands in the default bot command scope
  BotCommandScope scope;

  /// [languageCode] A two-letter ISO 639-1 country code or an empty string
  String languageCode;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetCommands.fromJson(Map<String, dynamic> json) {
    return GetCommands(
      scope: BotCommandScope.fromJson(json['scope'] ?? <String, dynamic>{}),
      languageCode: json['language_code'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "scope": this.scope.toJson(),
      "language_code": this.languageCode,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getCommands';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetActiveSessions extends TdFunction {
  /// Returns all active sessions of the current user
  GetActiveSessions(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetActiveSessions.fromJson(Map<String, dynamic> json) {
    return GetActiveSessions(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getActiveSessions';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class TerminateSession extends TdFunction {
  /// Terminates a session of the current user
  TerminateSession(
      {required this.sessionId,
      this.extra});

  /// [sessionId] Session identifier
  int sessionId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory TerminateSession.fromJson(Map<String, dynamic> json) {
    return TerminateSession(
      sessionId: int.tryParse(json['session_id'].toString()) ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "session_id": this.sessionId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'terminateSession';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class TerminateAllOtherSessions extends TdFunction {
  /// Terminates all other sessions of the current user
  TerminateAllOtherSessions(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory TerminateAllOtherSessions.fromJson(Map<String, dynamic> json) {
    return TerminateAllOtherSessions(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'terminateAllOtherSessions';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetConnectedWebsites extends TdFunction {
  /// Returns all website where the current user used Telegram to log in
  GetConnectedWebsites(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetConnectedWebsites.fromJson(Map<String, dynamic> json) {
    return GetConnectedWebsites(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getConnectedWebsites';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DisconnectWebsite extends TdFunction {
  /// Disconnects website from the current user's Telegram account
  DisconnectWebsite(
      {required this.websiteId,
      this.extra});

  /// [websiteId] Website identifier
  int websiteId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DisconnectWebsite.fromJson(Map<String, dynamic> json) {
    return DisconnectWebsite(
      websiteId: int.tryParse(json['website_id'].toString()) ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "website_id": this.websiteId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'disconnectWebsite';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DisconnectAllWebsites extends TdFunction {
  /// Disconnects all websites from the current user's Telegram account
  DisconnectAllWebsites(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DisconnectAllWebsites.fromJson(Map<String, dynamic> json) {
    return DisconnectAllWebsites(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'disconnectAllWebsites';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetSupergroupUsername extends TdFunction {
  /// Changes the username of a supergroup or channel, requires owner privileges in the supergroup or channel
  SetSupergroupUsername(
      {required this.supergroupId,
      required this.username,
      this.extra});

  /// [supergroupId] Identifier of the supergroup or channel
  int supergroupId;

  /// [username] New value of the username. Use an empty string to remove the username
  String username;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetSupergroupUsername.fromJson(Map<String, dynamic> json) {
    return SetSupergroupUsername(
      supergroupId: json['supergroup_id'] ?? 0,
      username: json['username'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "supergroup_id": this.supergroupId,
      "username": this.username,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setSupergroupUsername';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetSupergroupStickerSet extends TdFunction {
  /// Changes the sticker set of a supergroup; requires can_change_info administrator right
  SetSupergroupStickerSet(
      {required this.supergroupId,
      required this.stickerSetId,
      this.extra});

  /// [supergroupId] Identifier of the supergroup
  int supergroupId;

  /// [stickerSetId] New value of the supergroup sticker set identifier. Use 0 to remove the supergroup sticker set
  int stickerSetId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetSupergroupStickerSet.fromJson(Map<String, dynamic> json) {
    return SetSupergroupStickerSet(
      supergroupId: json['supergroup_id'] ?? 0,
      stickerSetId: int.tryParse(json['sticker_set_id'].toString()) ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "supergroup_id": this.supergroupId,
      "sticker_set_id": this.stickerSetId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setSupergroupStickerSet';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ToggleSupergroupSignMessages extends TdFunction {
  /// Toggles sender signatures messages sent in a channel; requires can_change_info administrator right
  ToggleSupergroupSignMessages(
      {required this.supergroupId,
      required this.signMessages,
      this.extra});

  /// [supergroupId] Identifier of the channel
  int supergroupId;

  /// [signMessages] New value of sign_messages
  bool signMessages;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ToggleSupergroupSignMessages.fromJson(Map<String, dynamic> json) {
    return ToggleSupergroupSignMessages(
      supergroupId: json['supergroup_id'] ?? 0,
      signMessages: json['sign_messages'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "supergroup_id": this.supergroupId,
      "sign_messages": this.signMessages,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'toggleSupergroupSignMessages';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ToggleSupergroupIsAllHistoryAvailable extends TdFunction {
  /// Toggles whether the message history of a supergroup is available to new members; requires can_change_info administrator right
  ToggleSupergroupIsAllHistoryAvailable(
      {required this.supergroupId,
      required this.isAllHistoryAvailable,
      this.extra});

  /// [supergroupId] The identifier of the supergroup
  int supergroupId;

  /// [isAllHistoryAvailable] The new value of is_all_history_available
  bool isAllHistoryAvailable;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ToggleSupergroupIsAllHistoryAvailable.fromJson(Map<String, dynamic> json) {
    return ToggleSupergroupIsAllHistoryAvailable(
      supergroupId: json['supergroup_id'] ?? 0,
      isAllHistoryAvailable: json['is_all_history_available'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "supergroup_id": this.supergroupId,
      "is_all_history_available": this.isAllHistoryAvailable,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'toggleSupergroupIsAllHistoryAvailable';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ToggleSupergroupIsBroadcastGroup extends TdFunction {
  /// Upgrades supergroup to a broadcast group; requires owner privileges in the supergroup
  ToggleSupergroupIsBroadcastGroup(
      {required this.supergroupId,
      this.extra});

  /// [supergroupId] Identifier of the supergroup
  int supergroupId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ToggleSupergroupIsBroadcastGroup.fromJson(Map<String, dynamic> json) {
    return ToggleSupergroupIsBroadcastGroup(
      supergroupId: json['supergroup_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "supergroup_id": this.supergroupId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'toggleSupergroupIsBroadcastGroup';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ReportSupergroupSpam extends TdFunction {
  /// Reports some messages from a user in a supergroup as spam; requires administrator rights in the supergroup
  ReportSupergroupSpam(
      {required this.supergroupId,
      required this.userId,
      required this.messageIds,
      this.extra});

  /// [supergroupId] Supergroup identifier
  int supergroupId;

  /// [userId] User identifier
  int userId;

  /// [messageIds] Identifiers of messages sent in the supergroup by the user. This list must be non-empty
  List<int> messageIds;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ReportSupergroupSpam.fromJson(Map<String, dynamic> json) {
    return ReportSupergroupSpam(
      supergroupId: json['supergroup_id'] ?? 0,
      userId: json['user_id'] ?? 0,
      messageIds: List<int>.from((json['message_ids'] ?? []).map((item) => item ?? 0).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "supergroup_id": this.supergroupId,
      "user_id": this.userId,
      "message_ids": this.messageIds.map((i) => i).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'reportSupergroupSpam';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetSupergroupMembers extends TdFunction {
  /// Returns information about members or banned users in a supergroup or channel. Can be used only if supergroupFullInfo.can_get_members == true; additionally, administrator privileges may be required for some filters
  GetSupergroupMembers(
      {required this.supergroupId,
      required this.filter,
      required this.offset,
      required this.limit,
      this.extra});

  /// [supergroupId] Identifier of the supergroup or channel
  int supergroupId;

  /// [filter] The type of users to return; pass null to use supergroupMembersFilterRecent
  SupergroupMembersFilter filter;

  /// [offset] Number of users to skip
  int offset;

  /// [limit] The maximum number of users be returned; up to 200
  int limit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetSupergroupMembers.fromJson(Map<String, dynamic> json) {
    return GetSupergroupMembers(
      supergroupId: json['supergroup_id'] ?? 0,
      filter: SupergroupMembersFilter.fromJson(json['filter'] ?? <String, dynamic>{}),
      offset: json['offset'] ?? 0,
      limit: json['limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "supergroup_id": this.supergroupId,
      "filter": this.filter.toJson(),
      "offset": this.offset,
      "limit": this.limit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getSupergroupMembers';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CloseSecretChat extends TdFunction {
  /// Closes a secret chat, effectively transferring its state to secretChatStateClosed
  CloseSecretChat(
      {required this.secretChatId,
      this.extra});

  /// [secretChatId] Secret chat identifier
  int secretChatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CloseSecretChat.fromJson(Map<String, dynamic> json) {
    return CloseSecretChat(
      secretChatId: json['secret_chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "secret_chat_id": this.secretChatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'closeSecretChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChatEventLog extends TdFunction {
  /// Returns a list of service actions taken by chat members and administrators in the last 48 hours. Available only for supergroups and channels. Requires administrator rights. Returns results in reverse chronological order (i. e., in order of decreasing event_id)
  GetChatEventLog(
      {required this.chatId,
      required this.query,
      required this.fromEventId,
      required this.limit,
      required this.filters,
      required this.userIds,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [query] Search query by which to filter events
  String query;

  /// [fromEventId] Identifier of an event from which to return results. Use 0 to get results from the latest events
  int fromEventId;

  /// [limit] The maximum number of events to return; up to 100
  int limit;

  /// [filters] The types of events to return; pass null to get chat events of all types
  ChatEventLogFilters filters;

  /// [userIds] User identifiers by which to filter events. By default, events relating to all users will be returned
  List<int> userIds;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChatEventLog.fromJson(Map<String, dynamic> json) {
    return GetChatEventLog(
      chatId: json['chat_id'] ?? 0,
      query: json['query'] ?? "",
      fromEventId: int.tryParse(json['from_event_id'].toString()) ?? 0,
      limit: json['limit'] ?? 0,
      filters: ChatEventLogFilters.fromJson(json['filters'] ?? <String, dynamic>{}),
      userIds: List<int>.from((json['user_ids'] ?? []).map((item) => item ?? 0).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "query": this.query,
      "from_event_id": this.fromEventId,
      "limit": this.limit,
      "filters": this.filters.toJson(),
      "user_ids": this.userIds.map((i) => i).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChatEventLog';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetPaymentForm extends TdFunction {
  /// Returns an invoice payment form. This method must be called when the user presses inlineKeyboardButtonBuy
  GetPaymentForm(
      {required this.chatId,
      required this.messageId,
      required this.theme,
      this.extra});

  /// [chatId] Chat identifier of the Invoice message
  int chatId;

  /// [messageId] Message identifier
  int messageId;

  /// [theme] Preferred payment form theme; pass null to use the default theme
  PaymentFormTheme theme;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetPaymentForm.fromJson(Map<String, dynamic> json) {
    return GetPaymentForm(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      theme: PaymentFormTheme.fromJson(json['theme'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "theme": this.theme.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getPaymentForm';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ValidateOrderInfo extends TdFunction {
  /// Validates the order information provided by a user and returns the available shipping options for a flexible invoice
  ValidateOrderInfo(
      {required this.chatId,
      required this.messageId,
      required this.orderInfo,
      required this.allowSave,
      this.extra});

  /// [chatId] Chat identifier of the Invoice message
  int chatId;

  /// [messageId] Message identifier
  int messageId;

  /// [orderInfo] The order information, provided by the user; pass null if empty
  OrderInfo orderInfo;

  /// [allowSave] True, if the order information can be saved
  bool allowSave;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ValidateOrderInfo.fromJson(Map<String, dynamic> json) {
    return ValidateOrderInfo(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      orderInfo: OrderInfo.fromJson(json['order_info'] ?? <String, dynamic>{}),
      allowSave: json['allow_save'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "order_info": this.orderInfo.toJson(),
      "allow_save": this.allowSave,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'validateOrderInfo';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SendPaymentForm extends TdFunction {
  /// Sends a filled-out payment form to the bot for final verification
  SendPaymentForm(
      {required this.chatId,
      required this.messageId,
      required this.paymentFormId,
      required this.orderInfoId,
      required this.shippingOptionId,
      required this.credentials,
      required this.tipAmount,
      this.extra});

  /// [chatId] Chat identifier of the Invoice message
  int chatId;

  /// [messageId] Message identifier
  int messageId;

  /// [paymentFormId] Payment form identifier returned by getPaymentForm
  int paymentFormId;

  /// [orderInfoId] Identifier returned by validateOrderInfo, or an empty string
  String orderInfoId;

  /// [shippingOptionId] Identifier of a chosen shipping option, if applicable
  String shippingOptionId;

  /// [credentials] The credentials chosen by user for payment
  InputCredentials credentials;

  /// [tipAmount] Chosen by the user amount of tip in the smallest units of the currency
  int tipAmount;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SendPaymentForm.fromJson(Map<String, dynamic> json) {
    return SendPaymentForm(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      paymentFormId: int.tryParse(json['payment_form_id'].toString()) ?? 0,
      orderInfoId: json['order_info_id'] ?? "",
      shippingOptionId: json['shipping_option_id'] ?? "",
      credentials: InputCredentials.fromJson(json['credentials'] ?? <String, dynamic>{}),
      tipAmount: json['tip_amount'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "payment_form_id": this.paymentFormId,
      "order_info_id": this.orderInfoId,
      "shipping_option_id": this.shippingOptionId,
      "credentials": this.credentials.toJson(),
      "tip_amount": this.tipAmount,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'sendPaymentForm';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetPaymentReceipt extends TdFunction {
  /// Returns information about a successful payment
  GetPaymentReceipt(
      {required this.chatId,
      required this.messageId,
      this.extra});

  /// [chatId] Chat identifier of the PaymentSuccessful message
  int chatId;

  /// [messageId] Message identifier
  int messageId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetPaymentReceipt.fromJson(Map<String, dynamic> json) {
    return GetPaymentReceipt(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getPaymentReceipt';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetSavedOrderInfo extends TdFunction {
  /// Returns saved order info, if any
  GetSavedOrderInfo(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetSavedOrderInfo.fromJson(Map<String, dynamic> json) {
    return GetSavedOrderInfo(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getSavedOrderInfo';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DeleteSavedOrderInfo extends TdFunction {
  /// Deletes saved order info
  DeleteSavedOrderInfo(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DeleteSavedOrderInfo.fromJson(Map<String, dynamic> json) {
    return DeleteSavedOrderInfo(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'deleteSavedOrderInfo';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DeleteSavedCredentials extends TdFunction {
  /// Deletes saved credentials for all payment provider bots
  DeleteSavedCredentials(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DeleteSavedCredentials.fromJson(Map<String, dynamic> json) {
    return DeleteSavedCredentials(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'deleteSavedCredentials';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetSupportUser extends TdFunction {
  /// Returns a user that can be contacted to get support
  GetSupportUser(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetSupportUser.fromJson(Map<String, dynamic> json) {
    return GetSupportUser(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getSupportUser';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetBackgrounds extends TdFunction {
  /// Returns backgrounds installed by the user
  GetBackgrounds(
      {required this.forDarkTheme,
      this.extra});

  /// [forDarkTheme] True, if the backgrounds must be ordered for dark theme
  bool forDarkTheme;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetBackgrounds.fromJson(Map<String, dynamic> json) {
    return GetBackgrounds(
      forDarkTheme: json['for_dark_theme'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "for_dark_theme": this.forDarkTheme,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getBackgrounds';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetBackgroundUrl extends TdFunction {
  /// Constructs a persistent HTTP URL for a background
  GetBackgroundUrl(
      {required this.name,
      required this.type,
      this.extra});

  /// [name] Background name
  String name;

  /// [type] Background type
  BackgroundType type;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetBackgroundUrl.fromJson(Map<String, dynamic> json) {
    return GetBackgroundUrl(
      name: json['name'] ?? "",
      type: BackgroundType.fromJson(json['type'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "name": this.name,
      "type": this.type.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getBackgroundUrl';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SearchBackground extends TdFunction {
  /// Searches for a background by its name
  SearchBackground(
      {required this.name,
      this.extra});

  /// [name] The name of the background
  String name;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SearchBackground.fromJson(Map<String, dynamic> json) {
    return SearchBackground(
      name: json['name'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "name": this.name,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'searchBackground';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetBackground extends TdFunction {
  /// Changes the background selected by the user; adds background to the list of installed backgrounds
  SetBackground(
      {required this.background,
      required this.type,
      required this.forDarkTheme,
      this.extra});

  /// [background] The input background to use; pass null to create a new filled backgrounds or to remove the current background
  InputBackground background;

  /// [type] Background type; pass null to use the default type of the remote background or to remove the current background
  BackgroundType type;

  /// [forDarkTheme] True, if the background is chosen for dark theme
  bool forDarkTheme;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetBackground.fromJson(Map<String, dynamic> json) {
    return SetBackground(
      background: InputBackground.fromJson(json['background'] ?? <String, dynamic>{}),
      type: BackgroundType.fromJson(json['type'] ?? <String, dynamic>{}),
      forDarkTheme: json['for_dark_theme'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "background": this.background.toJson(),
      "type": this.type.toJson(),
      "for_dark_theme": this.forDarkTheme,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setBackground';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class RemoveBackground extends TdFunction {
  /// Removes background from the list of installed backgrounds
  RemoveBackground(
      {required this.backgroundId,
      this.extra});

  /// [backgroundId] The background identifier
  int backgroundId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory RemoveBackground.fromJson(Map<String, dynamic> json) {
    return RemoveBackground(
      backgroundId: int.tryParse(json['background_id'].toString()) ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "background_id": this.backgroundId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'removeBackground';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ResetBackgrounds extends TdFunction {
  /// Resets list of installed backgrounds to its default value
  ResetBackgrounds(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ResetBackgrounds.fromJson(Map<String, dynamic> json) {
    return ResetBackgrounds(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'resetBackgrounds';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetLocalizationTargetInfo extends TdFunction {
  /// Returns information about the current localization target. This is an offline request if only_local is true. Can be called before authorization
  GetLocalizationTargetInfo(
      {required this.onlyLocal,
      this.extra});

  /// [onlyLocal] If true, returns only locally available information without sending network requests
  bool onlyLocal;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetLocalizationTargetInfo.fromJson(Map<String, dynamic> json) {
    return GetLocalizationTargetInfo(
      onlyLocal: json['only_local'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "only_local": this.onlyLocal,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getLocalizationTargetInfo';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetLanguagePackInfo extends TdFunction {
  /// Returns information about a language pack. Returned language pack identifier may be different from a provided one. Can be called before authorization
  GetLanguagePackInfo(
      {required this.languagePackId,
      this.extra});

  /// [languagePackId] Language pack identifier
  String languagePackId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetLanguagePackInfo.fromJson(Map<String, dynamic> json) {
    return GetLanguagePackInfo(
      languagePackId: json['language_pack_id'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "language_pack_id": this.languagePackId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getLanguagePackInfo';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetLanguagePackStrings extends TdFunction {
  /// Returns strings from a language pack in the current localization target by their keys. Can be called before authorization
  GetLanguagePackStrings(
      {required this.languagePackId,
      required this.keys,
      this.extra});

  /// [languagePackId] Language pack identifier of the strings to be returned
  String languagePackId;

  /// [keys] Language pack keys of the strings to be returned; leave empty to request all available strings
  List<String> keys;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetLanguagePackStrings.fromJson(Map<String, dynamic> json) {
    return GetLanguagePackStrings(
      languagePackId: json['language_pack_id'] ?? "",
      keys: List<String>.from((json['keys'] ?? []).map((item) => item ?? "").toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "language_pack_id": this.languagePackId,
      "keys": this.keys.map((i) => i).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getLanguagePackStrings';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SynchronizeLanguagePack extends TdFunction {
  /// Fetches the latest versions of all strings from a language pack in the current localization target from the server. This method doesn't need to be called explicitly for the current used/base language packs. Can be called before authorization
  SynchronizeLanguagePack(
      {required this.languagePackId,
      this.extra});

  /// [languagePackId] Language pack identifier
  String languagePackId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SynchronizeLanguagePack.fromJson(Map<String, dynamic> json) {
    return SynchronizeLanguagePack(
      languagePackId: json['language_pack_id'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "language_pack_id": this.languagePackId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'synchronizeLanguagePack';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class AddCustomServerLanguagePack extends TdFunction {
  /// Adds a custom server language pack to the list of installed language packs in current localization target. Can be called before authorization
  AddCustomServerLanguagePack(
      {required this.languagePackId,
      this.extra});

  /// [languagePackId] Identifier of a language pack to be added; may be different from a name that is used in an "https://t.me/setlanguage/" link
  String languagePackId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory AddCustomServerLanguagePack.fromJson(Map<String, dynamic> json) {
    return AddCustomServerLanguagePack(
      languagePackId: json['language_pack_id'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "language_pack_id": this.languagePackId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'addCustomServerLanguagePack';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetCustomLanguagePack extends TdFunction {
  /// Adds or changes a custom local language pack to the current localization target
  SetCustomLanguagePack(
      {required this.info,
      required this.strings,
      this.extra});

  /// [info] Information about the language pack. Language pack setCustomLanguagePack must start with 'X', consist only of English letters, digits and hyphens, and must not exceed 64 characters. Can be called before authorization
  LanguagePackInfo info;

  /// [strings] Strings of the new language pack
  List<LanguagePackString> strings;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetCustomLanguagePack.fromJson(Map<String, dynamic> json) {
    return SetCustomLanguagePack(
      info: LanguagePackInfo.fromJson(json['info'] ?? <String, dynamic>{}),
      strings: List<LanguagePackString>.from((json['strings'] ?? []).map((item) => LanguagePackString.fromJson(item ?? <String, dynamic>{})).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "info": this.info.toJson(),
      "strings": this.strings.map((i) => i.toJson()).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setCustomLanguagePack';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class EditCustomLanguagePackInfo extends TdFunction {
  /// Edits information about a custom local language pack in the current localization target. Can be called before authorization
  EditCustomLanguagePackInfo(
      {required this.info,
      this.extra});

  /// [info] New information about the custom local language pack
  LanguagePackInfo info;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory EditCustomLanguagePackInfo.fromJson(Map<String, dynamic> json) {
    return EditCustomLanguagePackInfo(
      info: LanguagePackInfo.fromJson(json['info'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "info": this.info.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'editCustomLanguagePackInfo';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetCustomLanguagePackString extends TdFunction {
  /// Adds, edits or deletes a string in a custom local language pack. Can be called before authorization
  SetCustomLanguagePackString(
      {required this.languagePackId,
      required this.newString,
      this.extra});

  /// [languagePackId] Identifier of a previously added custom local language pack in the current localization target
  String languagePackId;

  /// [newString] New language pack string
  LanguagePackString newString;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetCustomLanguagePackString.fromJson(Map<String, dynamic> json) {
    return SetCustomLanguagePackString(
      languagePackId: json['language_pack_id'] ?? "",
      newString: LanguagePackString.fromJson(json['new_string'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "language_pack_id": this.languagePackId,
      "new_string": this.newString.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setCustomLanguagePackString';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DeleteLanguagePack extends TdFunction {
  /// Deletes all information about a language pack in the current localization target. The language pack which is currently in use (including base language pack) or is being synchronized can't be deleted. Can be called before authorization
  DeleteLanguagePack(
      {required this.languagePackId,
      this.extra});

  /// [languagePackId] Identifier of the language pack to delete
  String languagePackId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DeleteLanguagePack.fromJson(Map<String, dynamic> json) {
    return DeleteLanguagePack(
      languagePackId: json['language_pack_id'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "language_pack_id": this.languagePackId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'deleteLanguagePack';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class RegisterDevice extends TdFunction {
  /// Registers the currently used device for receiving push notifications. Returns a globally unique identifier of the push notification subscription
  RegisterDevice(
      {required this.deviceToken,
      required this.otherUserIds,
      this.extra});

  /// [deviceToken] Device token
  DeviceToken deviceToken;

  /// [otherUserIds] List of user identifiers of other users currently using the application
  List<int> otherUserIds;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory RegisterDevice.fromJson(Map<String, dynamic> json) {
    return RegisterDevice(
      deviceToken: DeviceToken.fromJson(json['device_token'] ?? <String, dynamic>{}),
      otherUserIds: List<int>.from((json['other_user_ids'] ?? []).map((item) => item ?? 0).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "device_token": this.deviceToken.toJson(),
      "other_user_ids": this.otherUserIds.map((i) => i).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'registerDevice';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ProcessPushNotification extends TdFunction {
  /// Handles a push notification. Returns error with code 406 if the push notification is not supported and connection to the server is required to fetch new data. Can be called before authorization
  ProcessPushNotification(
      {required this.payload,
      this.extra});

  /// [payload] JSON-encoded push notification payload with all fields sent by the server, and "google.sent_time" and "google.notification.sound" fields added
  String payload;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ProcessPushNotification.fromJson(Map<String, dynamic> json) {
    return ProcessPushNotification(
      payload: json['payload'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "payload": this.payload,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'processPushNotification';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetPushReceiverId extends TdFunction {
  /// Returns a globally unique push notification subscription identifier for identification of an account, which has received a push notification. Can be called synchronously
  GetPushReceiverId(
      {required this.payload,
      this.extra});

  /// [payload] JSON-encoded push notification payload
  String payload;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetPushReceiverId.fromJson(Map<String, dynamic> json) {
    return GetPushReceiverId(
      payload: json['payload'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "payload": this.payload,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getPushReceiverId';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetRecentlyVisitedTMeUrls extends TdFunction {
  /// Returns t.me URLs recently visited by a newly registered user
  GetRecentlyVisitedTMeUrls(
      {required this.referrer,
      this.extra});

  /// [referrer] Google Play referrer to identify the user
  String referrer;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetRecentlyVisitedTMeUrls.fromJson(Map<String, dynamic> json) {
    return GetRecentlyVisitedTMeUrls(
      referrer: json['referrer'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "referrer": this.referrer,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getRecentlyVisitedTMeUrls';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetUserPrivacySettingRules extends TdFunction {
  /// Changes user privacy settings
  SetUserPrivacySettingRules(
      {required this.setting,
      required this.rules,
      this.extra});

  /// [setting] The privacy setting
  UserPrivacySetting setting;

  /// [rules] The new privacy rules
  UserPrivacySettingRules rules;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetUserPrivacySettingRules.fromJson(Map<String, dynamic> json) {
    return SetUserPrivacySettingRules(
      setting: UserPrivacySetting.fromJson(json['setting'] ?? <String, dynamic>{}),
      rules: UserPrivacySettingRules.fromJson(json['rules'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "setting": this.setting.toJson(),
      "rules": this.rules.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setUserPrivacySettingRules';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetUserPrivacySettingRules extends TdFunction {
  /// Returns the current privacy settings
  GetUserPrivacySettingRules(
      {required this.setting,
      this.extra});

  /// [setting] The privacy setting
  UserPrivacySetting setting;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetUserPrivacySettingRules.fromJson(Map<String, dynamic> json) {
    return GetUserPrivacySettingRules(
      setting: UserPrivacySetting.fromJson(json['setting'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "setting": this.setting.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getUserPrivacySettingRules';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetOption extends TdFunction {
  /// Returns the value of an option by its name. (Check the list of available options on https://core.telegram.org/tdlib/options.) Can be called before authorization
  GetOption(
      {required this.name,
      this.extra});

  /// [name] The name of the option
  String name;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetOption.fromJson(Map<String, dynamic> json) {
    return GetOption(
      name: json['name'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "name": this.name,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getOption';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetOption extends TdFunction {
  /// Sets the value of an option. (Check the list of available options on https://core.telegram.org/tdlib/options.) Only writable options can be set. Can be called before authorization
  SetOption(
      {required this.name,
      required this.value,
      this.extra});

  /// [name] The name of the option
  String name;

  /// [value] The new value of the option; pass null to reset option value to a default value
  OptionValue value;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetOption.fromJson(Map<String, dynamic> json) {
    return SetOption(
      name: json['name'] ?? "",
      value: OptionValue.fromJson(json['value'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "name": this.name,
      "value": this.value.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setOption';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetAccountTtl extends TdFunction {
  /// Changes the period of inactivity after which the account of the current user will automatically be deleted
  SetAccountTtl(
      {required this.ttl,
      this.extra});

  /// [ttl] New account TTL
  AccountTtl ttl;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetAccountTtl.fromJson(Map<String, dynamic> json) {
    return SetAccountTtl(
      ttl: AccountTtl.fromJson(json['ttl'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "ttl": this.ttl.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setAccountTtl';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetAccountTtl extends TdFunction {
  /// Returns the period of inactivity after which the account of the current user will automatically be deleted
  GetAccountTtl(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetAccountTtl.fromJson(Map<String, dynamic> json) {
    return GetAccountTtl(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getAccountTtl';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DeleteAccount extends TdFunction {
  /// Deletes the account of the current user, deleting all information associated with the user from the server. The phone number of the account can be used to create a new account. Can be called before authorization when the current authorization state is authorizationStateWaitPassword
  DeleteAccount(
      {required this.reason,
      this.extra});

  /// [reason] The reason why the account was deleted; optional
  String reason;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DeleteAccount.fromJson(Map<String, dynamic> json) {
    return DeleteAccount(
      reason: json['reason'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "reason": this.reason,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'deleteAccount';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class RemoveChatActionBar extends TdFunction {
  /// Removes a chat action bar without any other action
  RemoveChatActionBar(
      {required this.chatId,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory RemoveChatActionBar.fromJson(Map<String, dynamic> json) {
    return RemoveChatActionBar(
      chatId: json['chat_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'removeChatActionBar';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ReportChat extends TdFunction {
  /// Reports a chat to the Telegram moderators. A chat can be reported only from the chat action bar, or if this is a private chat with a bot, a private chat with a user sharing their location, a supergroup, or a channel, since other chats can't be checked by moderators
  ReportChat(
      {required this.chatId,
      required this.messageIds,
      required this.reason,
      required this.text,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [messageIds] Identifiers of reported messages, if any
  List<int> messageIds;

  /// [reason] The reason for reporting the chat
  ChatReportReason reason;

  /// [text] Additional report details; 0-1024 characters
  String text;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ReportChat.fromJson(Map<String, dynamic> json) {
    return ReportChat(
      chatId: json['chat_id'] ?? 0,
      messageIds: List<int>.from((json['message_ids'] ?? []).map((item) => item ?? 0).toList()),
      reason: ChatReportReason.fromJson(json['reason'] ?? <String, dynamic>{}),
      text: json['text'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_ids": this.messageIds.map((i) => i).toList(),
      "reason": this.reason.toJson(),
      "text": this.text,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'reportChat';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ReportChatPhoto extends TdFunction {
  /// Reports a chat photo to the Telegram moderators. A chat photo can be reported only if this is a private chat with a bot, a private chat with a user sharing their location, a supergroup, or a channel, since other chats can't be checked by moderators
  ReportChatPhoto(
      {required this.chatId,
      required this.fileId,
      required this.reason,
      required this.text,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [fileId] Identifier of the photo to report. Only full photos from chatPhoto can be reported
  int fileId;

  /// [reason] The reason for reporting the chat photo
  ChatReportReason reason;

  /// [text] Additional report details; 0-1024 characters
  String text;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ReportChatPhoto.fromJson(Map<String, dynamic> json) {
    return ReportChatPhoto(
      chatId: json['chat_id'] ?? 0,
      fileId: json['file_id'] ?? 0,
      reason: ChatReportReason.fromJson(json['reason'] ?? <String, dynamic>{}),
      text: json['text'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "file_id": this.fileId,
      "reason": this.reason.toJson(),
      "text": this.text,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'reportChatPhoto';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetChatStatistics extends TdFunction {
  /// Returns detailed statistics about a chat. Currently this method can be used only for supergroups and channels. Can be used only if supergroupFullInfo.can_get_statistics == true
  GetChatStatistics(
      {required this.chatId,
      required this.isDark,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [isDark] Pass true if a dark theme is used by the application
  bool isDark;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetChatStatistics.fromJson(Map<String, dynamic> json) {
    return GetChatStatistics(
      chatId: json['chat_id'] ?? 0,
      isDark: json['is_dark'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "is_dark": this.isDark,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getChatStatistics';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetMessageStatistics extends TdFunction {
  /// Returns detailed statistics about a message. Can be used only if message.can_get_statistics == true
  GetMessageStatistics(
      {required this.chatId,
      required this.messageId,
      required this.isDark,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [messageId] Message identifier
  int messageId;

  /// [isDark] Pass true if a dark theme is used by the application
  bool isDark;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetMessageStatistics.fromJson(Map<String, dynamic> json) {
    return GetMessageStatistics(
      chatId: json['chat_id'] ?? 0,
      messageId: json['message_id'] ?? 0,
      isDark: json['is_dark'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "is_dark": this.isDark,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getMessageStatistics';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetStatisticalGraph extends TdFunction {
  /// Loads an asynchronous or a zoomed in statistical graph
  GetStatisticalGraph(
      {required this.chatId,
      required this.token,
      this.x,
      this.extra});

  /// [chatId] Chat identifier
  int chatId;

  /// [token] The token for graph loading
  String token;

  /// [x] X-value for zoomed in graph or 0 otherwise
  int? x;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetStatisticalGraph.fromJson(Map<String, dynamic> json) {
    return GetStatisticalGraph(
      chatId: json['chat_id'] ?? 0,
      token: json['token'] ?? "",
      x: json['x'],
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "token": this.token,
      "x": this.x == null ? null : this.x!,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getStatisticalGraph';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetStorageStatistics extends TdFunction {
  /// Returns storage usage statistics. Can be called before authorization
  GetStorageStatistics(
      {required this.chatLimit,
      this.extra});

  /// [chatLimit] The maximum number of chats with the largest storage usage for which separate statistics need to be returned. All other chats will be grouped in entries with chat_id == 0. If the chat info database is not used, the chat_limit is ignored and is always set to 0
  int chatLimit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetStorageStatistics.fromJson(Map<String, dynamic> json) {
    return GetStorageStatistics(
      chatLimit: json['chat_limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_limit": this.chatLimit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getStorageStatistics';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetStorageStatisticsFast extends TdFunction {
  /// Quickly returns approximate storage usage statistics. Can be called before authorization
  GetStorageStatisticsFast(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetStorageStatisticsFast.fromJson(Map<String, dynamic> json) {
    return GetStorageStatisticsFast(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getStorageStatisticsFast';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetDatabaseStatistics extends TdFunction {
  /// Returns database statistics
  GetDatabaseStatistics(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetDatabaseStatistics.fromJson(Map<String, dynamic> json) {
    return GetDatabaseStatistics(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getDatabaseStatistics';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class OptimizeStorage extends TdFunction {
  /// Optimizes storage usage, i.e. deletes some files and returns new storage usage statistics. Secret thumbnails can't be deleted
  OptimizeStorage(
      {required this.size,
      required this.ttl,
      required this.count,
      required this.immunityDelay,
      this.fileTypes,
      this.chatIds,
      this.excludeChatIds,
      required this.returnDeletedFileStatistics,
      required this.chatLimit,
      this.extra});

  /// [size] Limit on the total size of files after deletion, in bytes. Pass -1 to use the default limit
  int size;

  /// [ttl] Limit on the time that has passed since the last time a file was accessed (or creation time for some filesystems). Pass -1 to use the default limit
  int ttl;

  /// [count] Limit on the total count of files after deletion. Pass -1 to use the default limit
  int count;

  /// [immunityDelay] The amount of time after the creation of a file during which it can't be deleted, in seconds. Pass -1 to use the default value
  int immunityDelay;

  /// [fileTypes] If non-empty, only files with the given types are considered. By default, all types except thumbnails, profile photos, stickers and wallpapers are deleted
  List<FileType>? fileTypes;

  /// [chatIds] If non-empty, only files from the given chats are considered. Use 0 as chat identifier to delete files not belonging to any chat (e.g., profile photos)
  List<int>? chatIds;

  /// [excludeChatIds] If non-empty, files from the given chats are excluded. Use 0 as chat identifier to exclude all files not belonging to any chat (e.g., profile photos)
  List<int>? excludeChatIds;

  /// [returnDeletedFileStatistics] Pass true if statistics about the files that were deleted must be returned instead of the whole storage usage statistics. Affects only returned statistics
  bool returnDeletedFileStatistics;

  /// [chatLimit] Same as in getStorageStatistics. Affects only returned statistics
  int chatLimit;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory OptimizeStorage.fromJson(Map<String, dynamic> json) {
    return OptimizeStorage(
      size: json['size'] ?? 0,
      ttl: json['ttl'] ?? 0,
      count: json['count'] ?? 0,
      immunityDelay: json['immunity_delay'] ?? 0,
      fileTypes: List<FileType>.from((json['file_types'] ?? []).map((item) => FileType.fromJson(item ?? <String, dynamic>{})).toList()),
      chatIds: List<int>.from((json['chat_ids'] ?? []).map((item) => item ?? 0).toList()),
      excludeChatIds: List<int>.from((json['exclude_chat_ids'] ?? []).map((item) => item ?? 0).toList()),
      returnDeletedFileStatistics: json['return_deleted_file_statistics'] ?? false,
      chatLimit: json['chat_limit'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "size": this.size,
      "ttl": this.ttl,
      "count": this.count,
      "immunity_delay": this.immunityDelay,
      "file_types": this.fileTypes == null ? null : this.fileTypes!.map((i) => i.toJson()).toList(),
      "chat_ids": this.chatIds == null ? null : this.chatIds!.map((i) => i).toList(),
      "exclude_chat_ids": this.excludeChatIds == null ? null : this.excludeChatIds!.map((i) => i).toList(),
      "return_deleted_file_statistics": this.returnDeletedFileStatistics,
      "chat_limit": this.chatLimit,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'optimizeStorage';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetNetworkType extends TdFunction {
  /// Sets the current network type. Can be called before authorization. Calling this method forces all network connections to reopen, mitigating the delay in switching between different networks, so it must be called whenever the network is changed, even if the network type remains the same.. Network type is used to check whether the library can use the network at all and also for collecting detailed network data usage statistics
  SetNetworkType(
      {required this.type,
      this.extra});

  /// [type] The new network type; pass null to set network type to networkTypeOther
  NetworkType type;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetNetworkType.fromJson(Map<String, dynamic> json) {
    return SetNetworkType(
      type: NetworkType.fromJson(json['type'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "type": this.type.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setNetworkType';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetNetworkStatistics extends TdFunction {
  /// Returns network data usage statistics. Can be called before authorization
  GetNetworkStatistics(
      {required this.onlyCurrent,
      this.extra});

  /// [onlyCurrent] If true, returns only data for the current library launch
  bool onlyCurrent;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetNetworkStatistics.fromJson(Map<String, dynamic> json) {
    return GetNetworkStatistics(
      onlyCurrent: json['only_current'] ?? false,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "only_current": this.onlyCurrent,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getNetworkStatistics';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class AddNetworkStatistics extends TdFunction {
  /// Adds the specified data to data usage statistics. Can be called before authorization
  AddNetworkStatistics(
      {required this.entry,
      this.extra});

  /// [entry] The network statistics entry with the data to be added to statistics
  NetworkStatisticsEntry entry;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory AddNetworkStatistics.fromJson(Map<String, dynamic> json) {
    return AddNetworkStatistics(
      entry: NetworkStatisticsEntry.fromJson(json['entry'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "entry": this.entry.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'addNetworkStatistics';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ResetNetworkStatistics extends TdFunction {
  /// Resets all network data usage statistics to zero. Can be called before authorization
  ResetNetworkStatistics(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ResetNetworkStatistics.fromJson(Map<String, dynamic> json) {
    return ResetNetworkStatistics(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'resetNetworkStatistics';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetAutoDownloadSettingsPresets extends TdFunction {
  /// Returns auto-download settings presets for the current user
  GetAutoDownloadSettingsPresets(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetAutoDownloadSettingsPresets.fromJson(Map<String, dynamic> json) {
    return GetAutoDownloadSettingsPresets(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getAutoDownloadSettingsPresets';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetAutoDownloadSettings extends TdFunction {
  /// Sets auto-download settings
  SetAutoDownloadSettings(
      {required this.settings,
      required this.type,
      this.extra});

  /// [settings] New user auto-download settings
  AutoDownloadSettings settings;

  /// [type] Type of the network for which the new settings are relevant
  NetworkType type;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetAutoDownloadSettings.fromJson(Map<String, dynamic> json) {
    return SetAutoDownloadSettings(
      settings: AutoDownloadSettings.fromJson(json['settings'] ?? <String, dynamic>{}),
      type: NetworkType.fromJson(json['type'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "settings": this.settings.toJson(),
      "type": this.type.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setAutoDownloadSettings';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetBankCardInfo extends TdFunction {
  /// Returns information about a bank card
  GetBankCardInfo(
      {required this.bankCardNumber,
      this.extra});

  /// [bankCardNumber] The bank card number
  String bankCardNumber;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetBankCardInfo.fromJson(Map<String, dynamic> json) {
    return GetBankCardInfo(
      bankCardNumber: json['bank_card_number'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "bank_card_number": this.bankCardNumber,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getBankCardInfo';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetPassportElement extends TdFunction {
  /// Returns one of the available Telegram Passport elements
  GetPassportElement(
      {required this.type,
      required this.password,
      this.extra});

  /// [type] Telegram Passport element type
  PassportElementType type;

  /// [password] Password of the current user
  String password;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetPassportElement.fromJson(Map<String, dynamic> json) {
    return GetPassportElement(
      type: PassportElementType.fromJson(json['type'] ?? <String, dynamic>{}),
      password: json['password'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "type": this.type.toJson(),
      "password": this.password,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getPassportElement';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetAllPassportElements extends TdFunction {
  /// Returns all available Telegram Passport elements
  GetAllPassportElements(
      {required this.password,
      this.extra});

  /// [password] Password of the current user
  String password;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetAllPassportElements.fromJson(Map<String, dynamic> json) {
    return GetAllPassportElements(
      password: json['password'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "password": this.password,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getAllPassportElements';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetPassportElement extends TdFunction {
  /// Adds an element to the user's Telegram Passport. May return an error with a message "PHONE_VERIFICATION_NEEDED" or "EMAIL_VERIFICATION_NEEDED" if the chosen phone number or the chosen email address must be verified first
  SetPassportElement(
      {required this.element,
      required this.password,
      this.extra});

  /// [element] Input Telegram Passport element
  InputPassportElement element;

  /// [password] Password of the current user
  String password;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetPassportElement.fromJson(Map<String, dynamic> json) {
    return SetPassportElement(
      element: InputPassportElement.fromJson(json['element'] ?? <String, dynamic>{}),
      password: json['password'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "element": this.element.toJson(),
      "password": this.password,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setPassportElement';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DeletePassportElement extends TdFunction {
  /// Deletes a Telegram Passport element
  DeletePassportElement(
      {required this.type,
      this.extra});

  /// [type] Element type
  PassportElementType type;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DeletePassportElement.fromJson(Map<String, dynamic> json) {
    return DeletePassportElement(
      type: PassportElementType.fromJson(json['type'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "type": this.type.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'deletePassportElement';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetPassportElementErrors extends TdFunction {
  /// Informs the user that some of the elements in their Telegram Passport contain errors; for bots only. The user will not be able to resend the elements, until the errors are fixed
  SetPassportElementErrors(
      {required this.userId,
      required this.errors,
      this.extra});

  /// [userId] User identifier
  int userId;

  /// [errors] The errors
  List<InputPassportElementError> errors;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetPassportElementErrors.fromJson(Map<String, dynamic> json) {
    return SetPassportElementErrors(
      userId: json['user_id'] ?? 0,
      errors: List<InputPassportElementError>.from((json['errors'] ?? []).map((item) => InputPassportElementError.fromJson(item ?? <String, dynamic>{})).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "user_id": this.userId,
      "errors": this.errors.map((i) => i.toJson()).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setPassportElementErrors';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetPreferredCountryLanguage extends TdFunction {
  /// Returns an IETF language tag of the language preferred in the country, which must be used to fill native fields in Telegram Passport personal details. Returns a 404 error if unknown
  GetPreferredCountryLanguage(
      {required this.countryCode,
      this.extra});

  /// [countryCode] A two-letter ISO 3166-1 alpha-2 country code
  String countryCode;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetPreferredCountryLanguage.fromJson(Map<String, dynamic> json) {
    return GetPreferredCountryLanguage(
      countryCode: json['country_code'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "country_code": this.countryCode,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getPreferredCountryLanguage';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SendPhoneNumberVerificationCode extends TdFunction {
  /// Sends a code to verify a phone number to be added to a user's Telegram Passport
  SendPhoneNumberVerificationCode(
      {required this.phoneNumber,
      required this.settings,
      this.extra});

  /// [phoneNumber] The phone number of the user, in international format
  String phoneNumber;

  /// [settings] Settings for the authentication of the user's phone number; pass null to use default settings
  PhoneNumberAuthenticationSettings settings;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SendPhoneNumberVerificationCode.fromJson(Map<String, dynamic> json) {
    return SendPhoneNumberVerificationCode(
      phoneNumber: json['phone_number'] ?? "",
      settings: PhoneNumberAuthenticationSettings.fromJson(json['settings'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "phone_number": this.phoneNumber,
      "settings": this.settings.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'sendPhoneNumberVerificationCode';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ResendPhoneNumberVerificationCode extends TdFunction {
  /// Re-sends the code to verify a phone number to be added to a user's Telegram Passport
  ResendPhoneNumberVerificationCode(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ResendPhoneNumberVerificationCode.fromJson(Map<String, dynamic> json) {
    return ResendPhoneNumberVerificationCode(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'resendPhoneNumberVerificationCode';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CheckPhoneNumberVerificationCode extends TdFunction {
  /// Checks the phone number verification code for Telegram Passport
  CheckPhoneNumberVerificationCode(
      {required this.code,
      this.extra});

  /// [code] Verification code
  String code;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CheckPhoneNumberVerificationCode.fromJson(Map<String, dynamic> json) {
    return CheckPhoneNumberVerificationCode(
      code: json['code'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "code": this.code,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'checkPhoneNumberVerificationCode';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SendEmailAddressVerificationCode extends TdFunction {
  /// Sends a code to verify an email address to be added to a user's Telegram Passport
  SendEmailAddressVerificationCode(
      {required this.emailAddress,
      this.extra});

  /// [emailAddress] Email address
  String emailAddress;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SendEmailAddressVerificationCode.fromJson(Map<String, dynamic> json) {
    return SendEmailAddressVerificationCode(
      emailAddress: json['email_address'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "email_address": this.emailAddress,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'sendEmailAddressVerificationCode';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ResendEmailAddressVerificationCode extends TdFunction {
  /// Re-sends the code to verify an email address to be added to a user's Telegram Passport
  ResendEmailAddressVerificationCode(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ResendEmailAddressVerificationCode.fromJson(Map<String, dynamic> json) {
    return ResendEmailAddressVerificationCode(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'resendEmailAddressVerificationCode';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CheckEmailAddressVerificationCode extends TdFunction {
  /// Checks the email address verification code for Telegram Passport
  CheckEmailAddressVerificationCode(
      {required this.code,
      this.extra});

  /// [code] Verification code
  String code;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CheckEmailAddressVerificationCode.fromJson(Map<String, dynamic> json) {
    return CheckEmailAddressVerificationCode(
      code: json['code'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "code": this.code,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'checkEmailAddressVerificationCode';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetPassportAuthorizationForm extends TdFunction {
  /// Returns a Telegram Passport authorization form for sharing data with a service
  GetPassportAuthorizationForm(
      {required this.botUserId,
      required this.scope,
      required this.publicKey,
      required this.nonce,
      this.extra});

  /// [botUserId] User identifier of the service's bot
  int botUserId;

  /// [scope] Telegram Passport element types requested by the service
  String scope;

  /// [publicKey] Service's public key
  String publicKey;

  /// [nonce] Unique request identifier provided by the service
  String nonce;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetPassportAuthorizationForm.fromJson(Map<String, dynamic> json) {
    return GetPassportAuthorizationForm(
      botUserId: json['bot_user_id'] ?? 0,
      scope: json['scope'] ?? "",
      publicKey: json['public_key'] ?? "",
      nonce: json['nonce'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "bot_user_id": this.botUserId,
      "scope": this.scope,
      "public_key": this.publicKey,
      "nonce": this.nonce,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getPassportAuthorizationForm';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetPassportAuthorizationFormAvailableElements extends TdFunction {
  /// Returns already available Telegram Passport elements suitable for completing a Telegram Passport authorization form. Result can be received only once for each authorization form
  GetPassportAuthorizationFormAvailableElements(
      {required this.autorizationFormId,
      required this.password,
      this.extra});

  /// [autorizationFormId] Authorization form identifier
  int autorizationFormId;

  /// [password] Password of the current user
  String password;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetPassportAuthorizationFormAvailableElements.fromJson(Map<String, dynamic> json) {
    return GetPassportAuthorizationFormAvailableElements(
      autorizationFormId: json['autorization_form_id'] ?? 0,
      password: json['password'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "autorization_form_id": this.autorizationFormId,
      "password": this.password,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getPassportAuthorizationFormAvailableElements';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SendPassportAuthorizationForm extends TdFunction {
  /// Sends a Telegram Passport authorization form, effectively sharing data with the service. This method must be called after getPassportAuthorizationFormAvailableElements if some previously available elements are going to be reused
  SendPassportAuthorizationForm(
      {required this.autorizationFormId,
      required this.types,
      this.extra});

  /// [autorizationFormId] Authorization form identifier
  int autorizationFormId;

  /// [types] Types of Telegram Passport elements chosen by user to complete the authorization form
  List<PassportElementType> types;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SendPassportAuthorizationForm.fromJson(Map<String, dynamic> json) {
    return SendPassportAuthorizationForm(
      autorizationFormId: json['autorization_form_id'] ?? 0,
      types: List<PassportElementType>.from((json['types'] ?? []).map((item) => PassportElementType.fromJson(item ?? <String, dynamic>{})).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "autorization_form_id": this.autorizationFormId,
      "types": this.types.map((i) => i.toJson()).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'sendPassportAuthorizationForm';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SendPhoneNumberConfirmationCode extends TdFunction {
  /// Sends phone number confirmation code to handle links of the type internalLinkTypePhoneNumberConfirmation
  SendPhoneNumberConfirmationCode(
      {required this.hash,
      required this.phoneNumber,
      required this.settings,
      this.extra});

  /// [hash] Hash value from the link
  String hash;

  /// [phoneNumber] Phone number value from the link
  String phoneNumber;

  /// [settings] Settings for the authentication of the user's phone number; pass null to use default settings
  PhoneNumberAuthenticationSettings settings;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SendPhoneNumberConfirmationCode.fromJson(Map<String, dynamic> json) {
    return SendPhoneNumberConfirmationCode(
      hash: json['hash'] ?? "",
      phoneNumber: json['phone_number'] ?? "",
      settings: PhoneNumberAuthenticationSettings.fromJson(json['settings'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "hash": this.hash,
      "phone_number": this.phoneNumber,
      "settings": this.settings.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'sendPhoneNumberConfirmationCode';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class ResendPhoneNumberConfirmationCode extends TdFunction {
  /// Resends phone number confirmation code
  ResendPhoneNumberConfirmationCode(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory ResendPhoneNumberConfirmationCode.fromJson(Map<String, dynamic> json) {
    return ResendPhoneNumberConfirmationCode(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'resendPhoneNumberConfirmationCode';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CheckPhoneNumberConfirmationCode extends TdFunction {
  /// Checks phone number confirmation code
  CheckPhoneNumberConfirmationCode(
      {required this.code,
      this.extra});

  /// [code] The phone number confirmation code
  String code;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CheckPhoneNumberConfirmationCode.fromJson(Map<String, dynamic> json) {
    return CheckPhoneNumberConfirmationCode(
      code: json['code'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "code": this.code,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'checkPhoneNumberConfirmationCode';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetBotUpdatesStatus extends TdFunction {
  /// Informs the server about the number of pending bot updates if they haven't been processed for a long time; for bots only
  SetBotUpdatesStatus(
      {required this.pendingUpdateCount,
      required this.errorMessage,
      this.extra});

  /// [pendingUpdateCount] The number of pending updates
  int pendingUpdateCount;

  /// [errorMessage] The last error message
  String errorMessage;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetBotUpdatesStatus.fromJson(Map<String, dynamic> json) {
    return SetBotUpdatesStatus(
      pendingUpdateCount: json['pending_update_count'] ?? 0,
      errorMessage: json['error_message'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "pending_update_count": this.pendingUpdateCount,
      "error_message": this.errorMessage,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setBotUpdatesStatus';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class UploadStickerFile extends TdFunction {
  /// Uploads a PNG image with a sticker; returns the uploaded file
  UploadStickerFile(
      {required this.userId,
      required this.sticker,
      this.extra});

  /// [userId] Sticker file owner; ignored for regular users
  int userId;

  /// [sticker] Sticker file to upload
  InputSticker sticker;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory UploadStickerFile.fromJson(Map<String, dynamic> json) {
    return UploadStickerFile(
      userId: json['user_id'] ?? 0,
      sticker: InputSticker.fromJson(json['sticker'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "user_id": this.userId,
      "sticker": this.sticker.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'uploadStickerFile';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetSuggestedStickerSetName extends TdFunction {
  /// Returns a suggested name for a new sticker set with a given title
  GetSuggestedStickerSetName(
      {required this.title,
      this.extra});

  /// [title] Sticker set title; 1-64 characters
  String title;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetSuggestedStickerSetName.fromJson(Map<String, dynamic> json) {
    return GetSuggestedStickerSetName(
      title: json['title'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "title": this.title,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getSuggestedStickerSetName';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CheckStickerSetName extends TdFunction {
  /// Checks whether a name can be used for a new sticker set
  CheckStickerSetName(
      {required this.name,
      this.extra});

  /// [name] Name to be checked
  String name;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CheckStickerSetName.fromJson(Map<String, dynamic> json) {
    return CheckStickerSetName(
      name: json['name'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "name": this.name,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'checkStickerSetName';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class CreateNewStickerSet extends TdFunction {
  /// Creates a new sticker set. Returns the newly created sticker set
  CreateNewStickerSet(
      {required this.userId,
      required this.title,
      required this.name,
      required this.isMasks,
      required this.stickers,
      required this.source,
      this.extra});

  /// [userId] Sticker set owner; ignored for regular users
  int userId;

  /// [title] Sticker set title; 1-64 characters
  String title;

  /// [name] Sticker set name. Can contain only English letters, digits and underscores. Must end with *"_by_
  String name;

  /// [isMasks] True, if stickers are masks. Animated stickers can't be masks
  bool isMasks;

  /// [stickers] List of stickers to be added to the set; must be non-empty. All stickers must be of the same type. For animated stickers, uploadStickerFile must be used before the sticker is shown
  List<InputSticker> stickers;

  /// [source] Source of the sticker set; may be empty if unknown
  String source;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory CreateNewStickerSet.fromJson(Map<String, dynamic> json) {
    return CreateNewStickerSet(
      userId: json['user_id'] ?? 0,
      title: json['title'] ?? "",
      name: json['name'] ?? "",
      isMasks: json['is_masks'] ?? false,
      stickers: List<InputSticker>.from((json['stickers'] ?? []).map((item) => InputSticker.fromJson(item ?? <String, dynamic>{})).toList()),
      source: json['source'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "user_id": this.userId,
      "title": this.title,
      "name": this.name,
      "is_masks": this.isMasks,
      "stickers": this.stickers.map((i) => i.toJson()).toList(),
      "source": this.source,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'createNewStickerSet';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class AddStickerToSet extends TdFunction {
  /// Adds a new sticker to a set; for bots only. Returns the sticker set
  AddStickerToSet(
      {required this.userId,
      required this.name,
      required this.sticker,
      this.extra});

  /// [userId] Sticker set owner
  int userId;

  /// [name] Sticker set name
  String name;

  /// [sticker] Sticker to add to the set
  InputSticker sticker;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory AddStickerToSet.fromJson(Map<String, dynamic> json) {
    return AddStickerToSet(
      userId: json['user_id'] ?? 0,
      name: json['name'] ?? "",
      sticker: InputSticker.fromJson(json['sticker'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "user_id": this.userId,
      "name": this.name,
      "sticker": this.sticker.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'addStickerToSet';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetStickerSetThumbnail extends TdFunction {
  /// Sets a sticker set thumbnail; for bots only. Returns the sticker set
  SetStickerSetThumbnail(
      {required this.userId,
      required this.name,
      this.thumbnail,
      this.extra});

  /// [userId] Sticker set owner
  int userId;

  /// [name] Sticker set name
  String name;

  /// [thumbnail] Thumbnail to set in PNG or TGS format; pass null to remove the sticker set thumbnail. Animated thumbnail must be set for animated sticker sets and only for them
  InputFile? thumbnail;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetStickerSetThumbnail.fromJson(Map<String, dynamic> json) {
    return SetStickerSetThumbnail(
      userId: json['user_id'] ?? 0,
      name: json['name'] ?? "",
      thumbnail: InputFile.fromJson(json['thumbnail'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "user_id": this.userId,
      "name": this.name,
      "thumbnail": this.thumbnail == null ? null : this.thumbnail!.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setStickerSetThumbnail';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetStickerPositionInSet extends TdFunction {
  /// Changes the position of a sticker in the set to which it belongs; for bots only. The sticker set must have been created by the bot
  SetStickerPositionInSet(
      {required this.sticker,
      required this.position,
      this.extra});

  /// [sticker] Sticker
  InputFile sticker;

  /// [position] New position of the sticker in the set, zero-based
  int position;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetStickerPositionInSet.fromJson(Map<String, dynamic> json) {
    return SetStickerPositionInSet(
      sticker: InputFile.fromJson(json['sticker'] ?? <String, dynamic>{}),
      position: json['position'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "sticker": this.sticker.toJson(),
      "position": this.position,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setStickerPositionInSet';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class RemoveStickerFromSet extends TdFunction {
  /// Removes a sticker from the set to which it belongs; for bots only. The sticker set must have been created by the bot
  RemoveStickerFromSet(
      {required this.sticker,
      this.extra});

  /// [sticker] Sticker
  InputFile sticker;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory RemoveStickerFromSet.fromJson(Map<String, dynamic> json) {
    return RemoveStickerFromSet(
      sticker: InputFile.fromJson(json['sticker'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "sticker": this.sticker.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'removeStickerFromSet';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetMapThumbnailFile extends TdFunction {
  /// Returns information about a file with a map thumbnail in PNG format. Only map thumbnail files with size less than 1MB can be downloaded
  GetMapThumbnailFile(
      {required this.location,
      required this.zoom,
      required this.width,
      required this.height,
      required this.scale,
      this.chatId,
      this.extra});

  /// [location] Location of the map center
  Location location;

  /// [zoom] Map zoom level; 13-20
  int zoom;

  /// [width] Map width in pixels before applying scale; 16-1024
  int width;

  /// [height] Map height in pixels before applying scale; 16-1024
  int height;

  /// [scale] Map scale; 1-3
  int scale;

  /// [chatId] Identifier of a chat, in which the thumbnail will be shown. Use 0 if unknown
  int? chatId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetMapThumbnailFile.fromJson(Map<String, dynamic> json) {
    return GetMapThumbnailFile(
      location: Location.fromJson(json['location'] ?? <String, dynamic>{}),
      zoom: json['zoom'] ?? 0,
      width: json['width'] ?? 0,
      height: json['height'] ?? 0,
      scale: json['scale'] ?? 0,
      chatId: json['chat_id'],
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "location": this.location.toJson(),
      "zoom": this.zoom,
      "width": this.width,
      "height": this.height,
      "scale": this.scale,
      "chat_id": this.chatId == null ? null : this.chatId!,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getMapThumbnailFile';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class AcceptTermsOfService extends TdFunction {
  /// Accepts Telegram terms of services
  AcceptTermsOfService(
      {required this.termsOfServiceId,
      this.extra});

  /// [termsOfServiceId] Terms of service identifier
  String termsOfServiceId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory AcceptTermsOfService.fromJson(Map<String, dynamic> json) {
    return AcceptTermsOfService(
      termsOfServiceId: json['terms_of_service_id'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "terms_of_service_id": this.termsOfServiceId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'acceptTermsOfService';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SendCustomRequest extends TdFunction {
  /// Sends a custom request; for bots only
  SendCustomRequest(
      {required this.method,
      required this.parameters,
      this.extra});

  /// [method] The method name
  String method;

  /// [parameters] JSON-serialized method parameters
  String parameters;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SendCustomRequest.fromJson(Map<String, dynamic> json) {
    return SendCustomRequest(
      method: json['method'] ?? "",
      parameters: json['parameters'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "method": this.method,
      "parameters": this.parameters,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'sendCustomRequest';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class AnswerCustomQuery extends TdFunction {
  /// Answers a custom query; for bots only
  AnswerCustomQuery(
      {required this.customQueryId,
      required this.data,
      this.extra});

  /// [customQueryId] Identifier of a custom query
  int customQueryId;

  /// [data] JSON-serialized answer to the query
  String data;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory AnswerCustomQuery.fromJson(Map<String, dynamic> json) {
    return AnswerCustomQuery(
      customQueryId: int.tryParse(json['custom_query_id'].toString()) ?? 0,
      data: json['data'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "custom_query_id": this.customQueryId,
      "data": this.data,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'answerCustomQuery';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetAlarm extends TdFunction {
  /// Succeeds after a specified amount of time has passed. Can be called before initialization
  SetAlarm(
      {required this.seconds,
      this.extra});

  /// [seconds] Number of seconds before the function returns
  double seconds;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetAlarm.fromJson(Map<String, dynamic> json) {
    return SetAlarm(
      seconds: double.tryParse(json['seconds'].toString()) ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "seconds": this.seconds,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setAlarm';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetCountries extends TdFunction {
  /// Returns information about existing countries. Can be called before authorization
  GetCountries(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetCountries.fromJson(Map<String, dynamic> json) {
    return GetCountries(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getCountries';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetCountryCode extends TdFunction {
  /// Uses the current IP address to find the current country. Returns two-letter ISO 3166-1 alpha-2 country code. Can be called before authorization
  GetCountryCode(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetCountryCode.fromJson(Map<String, dynamic> json) {
    return GetCountryCode(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getCountryCode';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetPhoneNumberInfo extends TdFunction {
  /// Returns information about a phone number by its prefix. Can be called before authorization
  GetPhoneNumberInfo(
      {required this.phoneNumberPrefix,
      this.extra});

  /// [phoneNumberPrefix] The phone number prefix
  String phoneNumberPrefix;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetPhoneNumberInfo.fromJson(Map<String, dynamic> json) {
    return GetPhoneNumberInfo(
      phoneNumberPrefix: json['phone_number_prefix'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "phone_number_prefix": this.phoneNumberPrefix,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getPhoneNumberInfo';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetPhoneNumberInfoSync extends TdFunction {
  /// Returns information about a phone number by its prefix synchronously. getCountries must be called at least once after changing localization to the specified language if properly localized country information is expected. Can be called synchronously
  GetPhoneNumberInfoSync(
      {required this.languageCode,
      required this.phoneNumberPrefix,
      this.extra});

  /// [languageCode] A two-letter ISO 639-1 country code for country information localization
  String languageCode;

  /// [phoneNumberPrefix] The phone number prefix
  String phoneNumberPrefix;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetPhoneNumberInfoSync.fromJson(Map<String, dynamic> json) {
    return GetPhoneNumberInfoSync(
      languageCode: json['language_code'] ?? "",
      phoneNumberPrefix: json['phone_number_prefix'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "language_code": this.languageCode,
      "phone_number_prefix": this.phoneNumberPrefix,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getPhoneNumberInfoSync';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetApplicationDownloadLink extends TdFunction {
  /// Returns the link for downloading official Telegram application to be used when the current user invites friends to Telegram
  GetApplicationDownloadLink(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetApplicationDownloadLink.fromJson(Map<String, dynamic> json) {
    return GetApplicationDownloadLink(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getApplicationDownloadLink';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetDeepLinkInfo extends TdFunction {
  /// Returns information about a tg:// deep link. Use "tg://need_update_for_some_feature" or "tg:some_unsupported_feature" for testing. Returns a 404 error for unknown links. Can be called before authorization
  GetDeepLinkInfo(
      {required this.link,
      this.extra});

  /// [link] The link
  String link;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetDeepLinkInfo.fromJson(Map<String, dynamic> json) {
    return GetDeepLinkInfo(
      link: json['link'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "link": this.link,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getDeepLinkInfo';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetApplicationConfig extends TdFunction {
  /// Returns application config, provided by the server. Can be called before authorization
  GetApplicationConfig(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetApplicationConfig.fromJson(Map<String, dynamic> json) {
    return GetApplicationConfig(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getApplicationConfig';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SaveApplicationLogEvent extends TdFunction {
  /// Saves application log event on the server. Can be called before authorization
  SaveApplicationLogEvent(
      {required this.type,
      required this.chatId,
      required this.data,
      this.extra});

  /// [type] Event type
  String type;

  /// [chatId] Optional chat identifier, associated with the event
  int chatId;

  /// [data] The log event data
  JsonValue data;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SaveApplicationLogEvent.fromJson(Map<String, dynamic> json) {
    return SaveApplicationLogEvent(
      type: json['type'] ?? "",
      chatId: json['chat_id'] ?? 0,
      data: JsonValue.fromJson(json['data'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "type": this.type,
      "chat_id": this.chatId,
      "data": this.data.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'saveApplicationLogEvent';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class AddProxy extends TdFunction {
  /// Adds a proxy server for network requests. Can be called before authorization
  AddProxy(
      {required this.server,
      required this.port,
      required this.enable,
      required this.type,
      this.extra});

  /// [server] Proxy server IP address
  String server;

  /// [port] Proxy server port
  int port;

  /// [enable] True, if the proxy needs to be enabled
  bool enable;

  /// [type] Proxy type
  ProxyType type;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory AddProxy.fromJson(Map<String, dynamic> json) {
    return AddProxy(
      server: json['server'] ?? "",
      port: json['port'] ?? 0,
      enable: json['enable'] ?? false,
      type: ProxyType.fromJson(json['type'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "server": this.server,
      "port": this.port,
      "enable": this.enable,
      "type": this.type.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'addProxy';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class EditProxy extends TdFunction {
  /// Edits an existing proxy server for network requests. Can be called before authorization
  EditProxy(
      {required this.proxyId,
      required this.server,
      required this.port,
      required this.enable,
      required this.type,
      this.extra});

  /// [proxyId] Proxy identifier
  int proxyId;

  /// [server] Proxy server IP address
  String server;

  /// [port] Proxy server port
  int port;

  /// [enable] True, if the proxy needs to be enabled
  bool enable;

  /// [type] Proxy type
  ProxyType type;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory EditProxy.fromJson(Map<String, dynamic> json) {
    return EditProxy(
      proxyId: json['proxy_id'] ?? 0,
      server: json['server'] ?? "",
      port: json['port'] ?? 0,
      enable: json['enable'] ?? false,
      type: ProxyType.fromJson(json['type'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "proxy_id": this.proxyId,
      "server": this.server,
      "port": this.port,
      "enable": this.enable,
      "type": this.type.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'editProxy';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class EnableProxy extends TdFunction {
  /// Enables a proxy. Only one proxy can be enabled at a time. Can be called before authorization
  EnableProxy(
      {required this.proxyId,
      this.extra});

  /// [proxyId] Proxy identifier
  int proxyId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory EnableProxy.fromJson(Map<String, dynamic> json) {
    return EnableProxy(
      proxyId: json['proxy_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "proxy_id": this.proxyId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'enableProxy';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class DisableProxy extends TdFunction {
  /// Disables the currently enabled proxy. Can be called before authorization
  DisableProxy(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory DisableProxy.fromJson(Map<String, dynamic> json) {
    return DisableProxy(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'disableProxy';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class RemoveProxy extends TdFunction {
  /// Removes a proxy server. Can be called before authorization
  RemoveProxy(
      {required this.proxyId,
      this.extra});

  /// [proxyId] Proxy identifier
  int proxyId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory RemoveProxy.fromJson(Map<String, dynamic> json) {
    return RemoveProxy(
      proxyId: json['proxy_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "proxy_id": this.proxyId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'removeProxy';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetProxies extends TdFunction {
  /// Returns list of proxies that are currently set up. Can be called before authorization
  GetProxies(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetProxies.fromJson(Map<String, dynamic> json) {
    return GetProxies(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getProxies';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetProxyLink extends TdFunction {
  /// Returns an HTTPS link, which can be used to add a proxy. Available only for SOCKS5 and MTProto proxies. Can be called before authorization
  GetProxyLink(
      {required this.proxyId,
      this.extra});

  /// [proxyId] Proxy identifier
  int proxyId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetProxyLink.fromJson(Map<String, dynamic> json) {
    return GetProxyLink(
      proxyId: json['proxy_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "proxy_id": this.proxyId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getProxyLink';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class PingProxy extends TdFunction {
  /// Computes time needed to receive a response from a Telegram server through a proxy. Can be called before authorization
  PingProxy(
      {required this.proxyId,
      this.extra});

  /// [proxyId] Proxy identifier. Use 0 to ping a Telegram server without a proxy
  int proxyId;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory PingProxy.fromJson(Map<String, dynamic> json) {
    return PingProxy(
      proxyId: json['proxy_id'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "proxy_id": this.proxyId,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'pingProxy';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetLogStream extends TdFunction {
  /// Sets new log stream for internal logging of TDLib. Can be called synchronously
  SetLogStream(
      {required this.logStream,
      this.extra});

  /// [logStream] New log stream
  LogStream logStream;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetLogStream.fromJson(Map<String, dynamic> json) {
    return SetLogStream(
      logStream: LogStream.fromJson(json['log_stream'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "log_stream": this.logStream.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setLogStream';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetLogStream extends TdFunction {
  /// Returns information about currently used log stream for internal logging of TDLib. Can be called synchronously
  GetLogStream(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetLogStream.fromJson(Map<String, dynamic> json) {
    return GetLogStream(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getLogStream';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetLogVerbosityLevel extends TdFunction {
  /// Sets the verbosity level of the internal logging of TDLib. Can be called synchronously
  SetLogVerbosityLevel(
      {required this.newVerbosityLevel,
      this.extra});

  /// [newVerbosityLevel] New value of the verbosity level for logging. Value 0 corresponds to fatal errors, value 1 corresponds to errors, value 2 corresponds to warnings and debug warnings, value 3 corresponds to informational, value 4 corresponds to debug, value 5 corresponds to verbose debug, value greater than 5 and up to 1023 can be used to enable even more logging
  int newVerbosityLevel;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetLogVerbosityLevel.fromJson(Map<String, dynamic> json) {
    return SetLogVerbosityLevel(
      newVerbosityLevel: json['new_verbosity_level'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "new_verbosity_level": this.newVerbosityLevel,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setLogVerbosityLevel';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetLogVerbosityLevel extends TdFunction {
  /// Returns current verbosity level of the internal logging of TDLib. Can be called synchronously
  GetLogVerbosityLevel(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetLogVerbosityLevel.fromJson(Map<String, dynamic> json) {
    return GetLogVerbosityLevel(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getLogVerbosityLevel';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetLogTags extends TdFunction {
  /// Returns list of available TDLib internal log tags, for example,
  GetLogTags(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetLogTags.fromJson(Map<String, dynamic> json) {
    return GetLogTags(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getLogTags';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class SetLogTagVerbosityLevel extends TdFunction {
  /// Sets the verbosity level for a specified TDLib internal log tag. Can be called synchronously
  SetLogTagVerbosityLevel(
      {required this.tag,
      required this.newVerbosityLevel,
      this.extra});

  /// [tag] Logging tag to change verbosity level
  String tag;

  /// [newVerbosityLevel] New verbosity level; 1-1024
  int newVerbosityLevel;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory SetLogTagVerbosityLevel.fromJson(Map<String, dynamic> json) {
    return SetLogTagVerbosityLevel(
      tag: json['tag'] ?? "",
      newVerbosityLevel: json['new_verbosity_level'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "tag": this.tag,
      "new_verbosity_level": this.newVerbosityLevel,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setLogTagVerbosityLevel';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class GetLogTagVerbosityLevel extends TdFunction {
  /// Returns current verbosity level for a specified TDLib internal log tag. Can be called synchronously
  GetLogTagVerbosityLevel(
      {required this.tag,
      this.extra});

  /// [tag] Logging tag to change verbosity level
  String tag;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory GetLogTagVerbosityLevel.fromJson(Map<String, dynamic> json) {
    return GetLogTagVerbosityLevel(
      tag: json['tag'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "tag": this.tag,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getLogTagVerbosityLevel';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class AddLogMessage extends TdFunction {
  /// Adds a message to TDLib internal log. Can be called synchronously
  AddLogMessage(
      {required this.verbosityLevel,
      required this.text,
      this.extra});

  /// [verbosityLevel] The minimum verbosity level needed for the message to be logged; 0-1023
  int verbosityLevel;

  /// [text] Text of a message to log
  String text;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory AddLogMessage.fromJson(Map<String, dynamic> json) {
    return AddLogMessage(
      verbosityLevel: json['verbosity_level'] ?? 0,
      text: json['text'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "verbosity_level": this.verbosityLevel,
      "text": this.text,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'addLogMessage';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class TestCallEmpty extends TdFunction {
  /// Does nothing; for testing only. This is an offline method. Can be called before authorization
  TestCallEmpty(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory TestCallEmpty.fromJson(Map<String, dynamic> json) {
    return TestCallEmpty(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'testCallEmpty';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class TestCallString extends TdFunction {
  /// Returns the received string; for testing only. This is an offline method. Can be called before authorization
  TestCallString(
      {required this.x,
      this.extra});

  /// [x] String to return
  String x;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory TestCallString.fromJson(Map<String, dynamic> json) {
    return TestCallString(
      x: json['x'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "x": this.x,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'testCallString';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class TestCallBytes extends TdFunction {
  /// Returns the received bytes; for testing only. This is an offline method. Can be called before authorization
  TestCallBytes(
      {required this.x,
      this.extra});

  /// [x] Bytes to return
  String x;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory TestCallBytes.fromJson(Map<String, dynamic> json) {
    return TestCallBytes(
      x: json['x'] ?? "",
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "x": this.x,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'testCallBytes';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class TestCallVectorInt extends TdFunction {
  /// Returns the received vector of numbers; for testing only. This is an offline method. Can be called before authorization
  TestCallVectorInt(
      {required this.x,
      this.extra});

  /// [x] Vector of numbers to return
  List<int> x;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory TestCallVectorInt.fromJson(Map<String, dynamic> json) {
    return TestCallVectorInt(
      x: List<int>.from((json['x'] ?? []).map((item) => item ?? 0).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "x": this.x.map((i) => i).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'testCallVectorInt';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class TestCallVectorIntObject extends TdFunction {
  /// Returns the received vector of objects containing a number; for testing only. This is an offline method. Can be called before authorization
  TestCallVectorIntObject(
      {required this.x,
      this.extra});

  /// [x] Vector of objects to return
  List<TestInt> x;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory TestCallVectorIntObject.fromJson(Map<String, dynamic> json) {
    return TestCallVectorIntObject(
      x: List<TestInt>.from((json['x'] ?? []).map((item) => TestInt.fromJson(item ?? <String, dynamic>{})).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "x": this.x.map((i) => i.toJson()).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'testCallVectorIntObject';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class TestCallVectorString extends TdFunction {
  /// Returns the received vector of strings; for testing only. This is an offline method. Can be called before authorization
  TestCallVectorString(
      {required this.x,
      this.extra});

  /// [x] Vector of strings to return
  List<String> x;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory TestCallVectorString.fromJson(Map<String, dynamic> json) {
    return TestCallVectorString(
      x: List<String>.from((json['x'] ?? []).map((item) => item ?? "").toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "x": this.x.map((i) => i).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'testCallVectorString';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class TestCallVectorStringObject extends TdFunction {
  /// Returns the received vector of objects containing a string; for testing only. This is an offline method. Can be called before authorization
  TestCallVectorStringObject(
      {required this.x,
      this.extra});

  /// [x] Vector of objects to return
  List<TestString> x;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory TestCallVectorStringObject.fromJson(Map<String, dynamic> json) {
    return TestCallVectorStringObject(
      x: List<TestString>.from((json['x'] ?? []).map((item) => TestString.fromJson(item ?? <String, dynamic>{})).toList()),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "x": this.x.map((i) => i.toJson()).toList(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'testCallVectorStringObject';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class TestSquareInt extends TdFunction {
  /// Returns the squared received number; for testing only. This is an offline method. Can be called before authorization
  TestSquareInt(
      {required this.x,
      this.extra});

  /// [x] Number to square
  int x;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory TestSquareInt.fromJson(Map<String, dynamic> json) {
    return TestSquareInt(
      x: json['x'] ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "x": this.x,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'testSquareInt';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class TestNetwork extends TdFunction {
  /// Sends a simple network request to the Telegram servers; for testing only. Can be called before authorization
  TestNetwork(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory TestNetwork.fromJson(Map<String, dynamic> json) {
    return TestNetwork(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'testNetwork';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class TestProxy extends TdFunction {
  /// Sends a simple network request to the Telegram servers via proxy; for testing only. Can be called before authorization
  TestProxy(
      {required this.server,
      required this.port,
      required this.type,
      required this.dcId,
      required this.timeout,
      this.extra});

  /// [server] Proxy server IP address
  String server;

  /// [port] Proxy server port
  int port;

  /// [type] Proxy type
  ProxyType type;

  /// [dcId] Identifier of a datacenter, with which to test connection
  int dcId;

  /// [timeout] The maximum overall timeout for the request
  double timeout;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory TestProxy.fromJson(Map<String, dynamic> json) {
    return TestProxy(
      server: json['server'] ?? "",
      port: json['port'] ?? 0,
      type: ProxyType.fromJson(json['type'] ?? <String, dynamic>{}),
      dcId: json['dc_id'] ?? 0,
      timeout: double.tryParse(json['timeout'].toString()) ?? 0,
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "server": this.server,
      "port": this.port,
      "type": this.type.toJson(),
      "dc_id": this.dcId,
      "timeout": this.timeout,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'testProxy';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class TestGetDifference extends TdFunction {
  /// Forces an updates.getDifference call to the Telegram servers; for testing only
  TestGetDifference(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory TestGetDifference.fromJson(Map<String, dynamic> json) {
    return TestGetDifference(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'testGetDifference';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class TestUseUpdate extends TdFunction {
  /// Does nothing and ensures that the Update object is used; for testing only. This is an offline method. Can be called before authorization
  TestUseUpdate(
      {this.extra});

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory TestUseUpdate.fromJson(Map<String, dynamic> json) {
    return TestUseUpdate(
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'testUseUpdate';

  @override
  String getConstructor() => CONSTRUCTOR;
}

class TestReturnError extends TdFunction {
  /// Returns the specified error and ensures that the Error object is used; for testing only. Can be called synchronously
  TestReturnError(
      {required this.error,
      this.extra});

  /// [error] The error to be returned
  TdError error;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  factory TestReturnError.fromJson(Map<String, dynamic> json) {
    return TestReturnError(
      error: TdError.fromJson(json['error'] ?? <String, dynamic>{}),
      extra: json['@extra'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "error": this.error.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'testReturnError';

  @override
  String getConstructor() => CONSTRUCTOR;
}

