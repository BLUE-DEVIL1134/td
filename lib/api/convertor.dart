import 'dart:convert';

import 'package:td/api/td_api.dart';
import 'package:td/api/td_object.dart';

TdObject convertToTdObject(String? query) {
  if (query == null) {
    return TdObject();
  }
  final newJson = json.decode(query);
  final fromJson = allObjects[newJson['@type']];
  if (fromJson == null) {
    return TdObject();
  }
  return fromJson(newJson);
}
