import 'dart:convert';
import 'dart:ffi';
import 'dart:io';

import 'package:ffi/ffi.dart';

typedef Pointer<Void> td_json_client_create();
typedef Pointer<Void> JsonClientCreate();

typedef Void td_json_client_send(Pointer<Void> client, Pointer<Utf8> request);
typedef void JsonClientSend(Pointer<Void> client, Pointer<Utf8> request);

typedef Pointer<Utf8> td_json_client_receive(
    Pointer<Void> client, Double timeout);
typedef Pointer<Utf8> JsonClientReceive(Pointer<Void> client, double timeout);

typedef Pointer<Utf8> td_json_client_execute(
    Pointer<Void> client, Pointer<Utf8> request);
typedef Pointer<Utf8> JsonClientExecute(
    Pointer<Void> client, Pointer<Utf8> request);

typedef Void td_json_client_destroy(Pointer<Void> client);
typedef void JsonClientDestroy(Pointer<Void> client);

class RawJsonBindings {
  late Pointer<Void> _client;
  late JsonClientCreate _jsonClientCreate;
  late JsonClientSend _jsonClientSend;
  late JsonClientReceive _jsonClientReceive;
  late JsonClientExecute _jsonClientExecute;
  late JsonClientDestroy _jsonClientDestroy;

  RawJsonBindings([bool shouldInit = true]) {
    DynamicLibrary? lib;
    if (Platform.isAndroid)
      lib = DynamicLibrary.open("libtdjsonandroid.so");
    else if (Platform.isIOS)
      lib = DynamicLibrary.executable();
    else
      lib = DynamicLibrary.process();

    var createName = "td_json_client_create";
    var sendName = "td_json_client_send";
    var receiveName = "td_json_client_receive";
    var destroyName = "td_json_client_destroy";
    var executeName = "td_json_client_execute";

    if (Platform.isAndroid) {
      createName = "_" + createName;
      sendName = "_" + sendName;
      receiveName = "_" + receiveName;
      destroyName = "_" + destroyName;
      executeName = "_" + executeName;
    }

    _jsonClientCreate =
        lib.lookupFunction<td_json_client_create, JsonClientCreate>(createName);
    _jsonClientSend =
        lib.lookupFunction<td_json_client_send, JsonClientSend>(sendName);
    _jsonClientReceive = lib
        .lookupFunction<td_json_client_receive, JsonClientReceive>(receiveName);
    _jsonClientDestroy = lib
        .lookupFunction<td_json_client_destroy, JsonClientDestroy>(destroyName);
    _jsonClientExecute = lib
        .lookupFunction<td_json_client_execute, JsonClientExecute>(executeName);
  }

  void initClient() => _client = _jsonClientCreate();
  Pointer<Void> get clientAddr => _client;

  void send(Map<String, dynamic> req) {
    var reqJson = json.encode(req);
    var ptr = reqJson.toNativeUtf8();
    var orig = ptr.toDartString();
    _jsonClientSend(_client, ptr);
  }

  void destroy([Pointer<Void>? client]) =>
      _jsonClientDestroy(client != null ? client : _client);

  Map<String, dynamic> execute(Map<String, dynamic> req) {
    var reqJson = json.encode(req);
    final res = _jsonClientExecute(_client, reqJson.toNativeUtf8());
    final resJson = res.toDartString();
    return json.decode(resJson);
  }

  String? receive({double timeout = 10.0}) {
    final res = _jsonClientReceive(_client, timeout);
    if (res.address != 0) {
      final resJson = res.toDartString();
      return resJson;
    } else
      return null;
  }
}
