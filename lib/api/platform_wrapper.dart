import 'dart:async';
import 'package:flutter/services.dart';
import 'package:td/api/convertor.dart';
import 'dart:convert' show json;

import 'package:td/api/td_function.dart';
import 'package:td/api/td_object.dart';

class TdlibPlatformWrapper {
  static const MethodChannel _channel = const MethodChannel('td');
  static const EventChannel _eventChannel = EventChannel('td/events');

  /// Creates a new instance of TDLib.
  /// Returns client id for the created instance of TDLib.
  /// 0 mean No client instance.
  static Future<int> createClient([shouldCleanClients = false]) async {
    int result;
    try {
      result = await _channel.invokeMethod(
          'clientCreate', {'shouldCleanClients': shouldCleanClients});
    } on PlatformException {
      result = 0;
    }

    return result;
  }

  /// Destroys the TDLib client instance. After this is called the client instance shouldn't be used anymore.
  static Future<void> destroyClient(int clientId) async =>
      await _channel.invokeMethod('clientDestroy', clientId);

  /// Sends request to the TDLib client. May be called from any thread.
  static Future<void> clientSend(int clientId, TdFunction event) async =>
      await _channel.invokeMethod('clientSend',
          {'query': json.encode(event.toJson()), 'clientId': clientId});

  /// Events from the incoming updates and request responses from the TDLib client by [clientId] identifier.
  /// Must be call once per client.
  static Stream<String> clientEvents() {
    return _eventChannel
        .receiveBroadcastStream()
        .map((event) => event.toString());
  }

  /// Synchronously executes TDLib request. May be called from any thread.
  /// Only a few requests can be executed synchronously.
  /// Returned pointer will be deallocated by TDLib during next call to clientReceive or clientExecute in the same thread, so it can't be used after that.
  static Future<TdObject> clientExecute(int clientId, TdFunction event) async =>
      convertToTdObject(await _channel.invokeMethod('clientExecute',
          {'query': json.encode(event.toJson()), 'clientId': clientId}));

  /// Sets the verbosity [level] of the internal logging of TDLib.
  /// By default the TDLib uses a log verbosity [level] of 5.
  /// Value 0 corresponds to fatal errors.
  /// value 1 corresponds to errors.
  /// value 2 corresponds to warnings and debug warnings.
  /// value 3 corresponds to informational.
  /// value 4 corresponds to debug.
  /// value 5 corresponds to verbose debug.
  /// value greater than 5 and up to 1024 can be used to enable even more logging.
  /// static Future<void> setLogVerbosityLevel(int level) async => await _platform
  ///    .invokeMethod('logLevel', <String, dynamic>{'level': level});

  /// Sets the path to the file where the internal TDLib log will be written.
  /// By default TDLib writes logs to stderr or an OS specific log.
  /// Use this method to write the log to a file instead.
  /// Null-terminated [filePath] to a file where the internal TDLib log will be written. Use an empty path to switch back to the default logging behaviour.
  // static Future<void> setLogFilePath(String filePath) async => await _platform
  //    .invokeMethod('logPath', <String, dynamic>{'path': filePath});

  /// Sets maximum size of the file to where the internal TDLib log is written before the file will be auto-rotated.
  /// Unused if log is not written to a file. Defaults to 10 MB.
  /// Maximum [size] of the file to where the internal TDLib log is written before the file will be auto-rotated. Should be positive.
  // static Future<void> setLogMaxFileSize(int size) async =>
  //    await _platform.invokeMethod('logSize', <String, dynamic>{'size': size});
}
