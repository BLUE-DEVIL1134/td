import 'dart:ffi';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:td/api/raw_json_client.dart';

const ISOLATE_CLIENTS_NAME = 'isolate_clients';

class IsolateClientManager {
  late SharedPreferences _prefs;
  late RawJsonBindings _bindings;

  bool isInitialized = false;

  Future<void> init() async {
    _prefs = await SharedPreferences.getInstance();
    _bindings = RawJsonBindings();
  }

  Future<void> checkClients() async {
    if (!isInitialized) await init();
    List<String>? clients = _prefs.getStringList(ISOLATE_CLIENTS_NAME);
    if (clients != null) {
      for (var clientAddrRaw in clients) {
        _bindings.destroy(Pointer.fromAddress(int.tryParse(clientAddrRaw)!));
        _prefs.setStringList(ISOLATE_CLIENTS_NAME, []);
      }
    }
  }

  void saveClient(int clientAddr) {
    List<String>? clients = _prefs.getStringList(ISOLATE_CLIENTS_NAME);
    if (clients == null) clients = [];
    clients.add(clientAddr.toString());
    _prefs.setStringList(ISOLATE_CLIENTS_NAME, clients);
  }
}
