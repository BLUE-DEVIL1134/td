import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rxdart/rxdart.dart';
import 'package:td/api/convertor.dart';
import 'package:td/api/ffi_wrapper.dart';
import 'package:td/api/platform_wrapper.dart';
import 'package:td/api/td_function.dart';
import 'package:td/api/td_object.dart';

int _random() => Random().nextInt(10000000);

enum Backend { PLATFORM_CHANNELS, FFI }

// TODO: Changing current client

/// Helper class which can be used for Flutter applications
class TelegramService extends ChangeNotifier {
  /// FFI Manager
  TdlibFFIWrapper? ffiManager;

  /// Tdlib events stream, can be listened multiple times
  BehaviorSubject<TdObject> currentClientEvents = BehaviorSubject();
  int? currentClientId;

  Map<int, BehaviorSubject<TdObject>> events = {};
  Stream<String>? _clientEvents;
  late StreamSubscription _clientsEventsSubs;

  Map results = <int, Completer>{};
  Map callbackResults = <int, Future<void>>{};

  Backend? selectedBackend;

  Future<void> initClient([Backend? backend]) async {
    if (selectedBackend != null && backend != selectedBackend)
      throw PlatformException(
          code: "TDLib",
          message:
              "Please use same backend across all clients."); // Maybe allow

    int clientId;

    if (backend == null) {
      if (Platform.isAndroid)
        backend = Backend.PLATFORM_CHANNELS;
      else
        backend = Backend.FFI;
    } else
      selectedBackend = backend;

    if (selectedBackend == Backend.PLATFORM_CHANNELS) {
      clientId =
          await TdlibPlatformWrapper.createClient(currentClientId == null);
      if (_clientEvents == null)
        _clientEvents = TdlibPlatformWrapper.clientEvents();
      if (clientId == 0)
        throw PlatformException(
            code: "TDLib", message: "Error while launching");
    } else {
      if (ffiManager == null) ffiManager = TdlibFFIWrapper();
      clientId = await ffiManager!.initClient();
      _clientEvents = ffiManager!.updates.stream;
    }

    if (currentClientId == null) currentClientId = clientId;
    _clientsEventsSubs = _clientEvents!.listen(_eventsResolver);
    events[clientId] = BehaviorSubject();
  }

  void _eventsResolver(String newEvent) async {
    Map<dynamic, dynamic> eventDecoded = json.decode(newEvent);
    int clientId = eventDecoded['clientId'];
    eventDecoded.remove('clientId');

    TdObject tdObject = convertToTdObject(json.encode(eventDecoded));
    if (clientId == currentClientId) currentClientEvents.add(tdObject);
    events[clientId]!.add(tdObject);
    await _resolveEvent(tdObject);
  }

  Future _resolveEvent(TdObject event) async {
    if (event.extra != null) {
      final int? extraId = int.tryParse(event.extra!);
      if (results.containsKey(extraId)) {
        results.remove(extraId).complete(event);
      } else if (callbackResults.containsKey(extraId)) {
        await callbackResults.remove(extraId);
      }
    }
  }

  void destroyClient(int clientId) async {
    if (selectedBackend == Backend.PLATFORM_CHANNELS) {
      // Platform channels impl
      await TdlibPlatformWrapper.destroyClient(clientId);
    } else {
      // FFI impl
      await ffiManager!.close(clientId);
    }
  }

  /// Sends request to the TDLib client. May be called from any thread.
  Future<TdObject?> send(event, {Future<void>? callback, int? clientId}) async {
    if (clientId == null) clientId = currentClientId!;

    final rndId = _random();
    event.extra = rndId.toString();
    if (callback != null) {
      callbackResults[rndId] = callback;
      try {
        if (selectedBackend == Backend.PLATFORM_CHANNELS)
          await TdlibPlatformWrapper.clientSend(clientId, event);
        else
          await ffiManager!.send(clientId, event);
      } catch (e) {
        print(e);
      }
    } else {
      final completer = Completer<TdObject>();
      results[rndId] = completer;
      if (selectedBackend == Backend.PLATFORM_CHANNELS)
        await TdlibPlatformWrapper.clientSend(clientId, event);
      else
        await ffiManager!.send(clientId, event);
      return completer.future;
    }

    return null;
  }

  /// Synchronously executes TDLib request. May be called from any thread.
  /// Only a few requests can be executed synchronously.
  /// Returned pointer will be deallocated by TDLib during next call to clientReceive or clientExecute in the same thread, so it can't be used after that.
  Future<TdObject> execute(TdFunction event, {int? clientId}) async {
    if (clientId == null) clientId = currentClientId!;

    if (selectedBackend == Backend.PLATFORM_CHANNELS)
      return await TdlibPlatformWrapper.clientExecute(clientId, event);
    else
      return await ffiManager!.execute(clientId, event);
  }

  @override
  void dispose() {
    _clientsEventsSubs.cancel();
    currentClientEvents.close();
    events.forEach((clientId, value) {
      value.close();
      if (selectedBackend == Backend.FFI)
        ffiManager!.close(clientId);
      else
        TdlibPlatformWrapper.destroyClient(clientId);
    });
    super.dispose();
  }
}
